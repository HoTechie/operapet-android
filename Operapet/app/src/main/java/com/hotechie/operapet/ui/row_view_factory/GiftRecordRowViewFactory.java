package com.hotechie.operapet.ui.row_view_factory;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.hotechie.operapet.Constant;
import com.hotechie.operapet.MyApplication;
import com.hotechie.operapet.R;
import com.hotechie.operapet.Util;
import com.hotechie.operapet.service.ServiceManager;
import com.hotechie.operapet.service.model.Gift;
import com.squareup.picasso.Picasso;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

/**
 * Created by duncan on 19/10/2016.
 */

public class GiftRecordRowViewFactory extends RowViewFactory {
    public static interface GiftRecordRowViewFactoryCallback {
        public void onClick(Gift gift);
    }

    protected List<Gift> mGifts= new ArrayList<>();
    protected GiftRecordRowViewFactory.GiftRecordRowViewFactoryCallback mCallback = null;

    protected SimpleDateFormat originalTimeFormat;
    protected SimpleDateFormat targetTimeFormat;


    public GiftRecordRowViewFactory(List<Gift> gifts, final GiftRecordRowViewFactory.GiftRecordRowViewFactoryCallback callback){
        mGifts = new ArrayList<>(gifts);
        mCallback = callback;

        originalTimeFormat = new SimpleDateFormat("yyyy-MM-dd HH:mm");
        targetTimeFormat = new SimpleDateFormat(Util.getString(R.string.post_datetime_format));
    }

    @Override
    public int getCount() {
        return mGifts.size();
    }

    @Override
    public Object getItem(int index) {
        return mGifts.get(index);
    }

    @Override
    public View getView(final Object object) {
        final Gift gift = (Gift)object;

        LayoutInflater inflater = (LayoutInflater) MyApplication.getAppContext().getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        View rowView = inflater.inflate(R.layout.row_gift_record, null, false);

        ImageView imgLeft = (ImageView)rowView.findViewById(R.id.img_left);
        TextView txtLbNeededMark = (TextView)rowView.findViewById(R.id.txt_lb_needed_mark);
        TextView txtNeededMark = (TextView)rowView.findViewById(R.id.txt_needed_mark);
        TextView txtName = (TextView)rowView.findViewById(R.id.txt_name);
        TextView txtDate = (TextView)rowView.findViewById(R.id.txt_date);
        ImageView img = (ImageView)rowView.findViewById(R.id.img);
        TextView txtIndicator = (TextView)rowView.findViewById(R.id.txt_indicator);

        txtNeededMark.setText(String.valueOf(gift.credit));
        txtName.setText(gift.name);
        if (gift.images != null && gift.images.size() > 0){
            Picasso.with(MyApplication.getAppContext()).load(ServiceManager.sharedInstance().getImageUrl(ServiceManager.IMAGE_URL_GIFT, gift.images.get(0))).into(img);
        }
        else{
            img.setImageDrawable(null);
        }

        String createTimeStr = "";
        try {
            Date createTimeDate = originalTimeFormat.parse(gift.create_time);
            createTimeStr = targetTimeFormat.format(createTimeDate);
        } catch (Exception e) {
            e.printStackTrace();
        }
        txtDate.setText(createTimeStr);

        if (gift.status == Constant.GIFT_STATUS_COLLECTED){
            imgLeft.setImageResource(R.drawable.gift_record_left_gery);
            txtLbNeededMark.setTextColor(Util.getColor(MyApplication.getAppContext(), R.color.main_text_color_invert));
            txtNeededMark.setTextColor(Util.getColor(MyApplication.getAppContext(), R.color.main_text_color_invert));
            txtName.setTextColor(Util.getColor(MyApplication.getAppContext(), R.color.main_text_color));
            txtIndicator.setText(Util.getString(R.string.profile_gift_collected));
        }
        else if (gift.status == Constant.GIFT_STATUS_REDEEMED){
            imgLeft.setImageResource(R.drawable.gift_record_left);
            txtLbNeededMark.setTextColor(Util.getColor(MyApplication.getAppContext(), R.color.theme_color_darker));
            txtNeededMark.setTextColor(Util.getColor(MyApplication.getAppContext(), R.color.theme_color_darker));
            txtName.setTextColor(Util.getColor(MyApplication.getAppContext(), R.color.theme_color));
            txtIndicator.setText(Util.getString(R.string.profile_gift_redeemed));
        }
        else if (gift.status == Constant.GIFT_STATUS_CANCELED){
            imgLeft.setImageResource(R.drawable.gift_record_left_gery);
            txtLbNeededMark.setTextColor(Util.getColor(MyApplication.getAppContext(), R.color.main_text_color_invert));
            txtNeededMark.setTextColor(Util.getColor(MyApplication.getAppContext(), R.color.main_text_color_invert));
            txtName.setTextColor(Util.getColor(MyApplication.getAppContext(), R.color.main_text_color));
            txtIndicator.setText(Util.getString(R.string.profile_gift_canceled));
        }

        if (mCallback != null && gift.status == Constant.GIFT_STATUS_REDEEMED){
            rowView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    mCallback.onClick(gift);
                }
            });
        }

        return rowView;
    }

    @Override
    public boolean modifyViewLayout(View contentView, Object object, int index) {
        return false;
    }

}
