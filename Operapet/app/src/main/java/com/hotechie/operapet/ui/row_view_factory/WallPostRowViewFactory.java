package com.hotechie.operapet.ui.row_view_factory;

import android.content.res.Resources;
import android.view.View;
import android.view.ViewGroup;
import android.widget.RelativeLayout;

import com.hotechie.operapet.MyApplication;
import com.hotechie.operapet.service.model.Post;

import java.util.List;

import retrofit2.http.PATCH;

/**
 * Created by duncan on 30/9/2016.
 */

public class WallPostRowViewFactory extends PostRowViewFactory {

    protected View mProfileView = null;

    public WallPostRowViewFactory(List<Post> posts, final PostRowViewFactoryCallback callback, View pofileView){
        super(posts, callback);
        mProfileView = pofileView;
    }

    @Override
    public int getCount() {
        return super.getCount() + 1;
    }

    @Override
    public Object getItem(int index) {
        if (index == 0)
            return null;
        return super.getItem(index - 1);
    }

    @Override
    public View getView(Object object) {
        if (object == null){
            ViewGroup parent = (ViewGroup)mProfileView.getParent();
            if (parent != null){
                parent.removeView(mProfileView);
            }
            return mProfileView;
        }


        View rowView = super.getView(object);
        return rowView;
    }

    @Override
    public boolean modifyViewLayout(View contentView, Object object, int index) {
        if (index == 0) {
            RelativeLayout.LayoutParams params = (RelativeLayout.LayoutParams) contentView.getLayoutParams();
            params.setMargins(0, 0, 0, 0);
            return false;
        }

        return super.modifyViewLayout(contentView, object, index);
    }
}
