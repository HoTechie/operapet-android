package com.hotechie.operapet.ui.row_view_factory;

import android.content.Context;
import android.content.res.Resources;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.hotechie.operapet.MyApplication;
import com.hotechie.operapet.R;
import com.hotechie.operapet.Util;
import com.hotechie.operapet.service.ServiceManager;
import com.hotechie.operapet.service.model.Comment;
import com.squareup.picasso.Picasso;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

/**
 * Created by duncan on 30/9/2016.
 */

public class PostCommentRowViewFactory extends RowViewFactory {
    public static interface  PostCommentRowViewFactoryCallback{
        public void onClickLike(Comment comment);
    }

    protected List<Comment> mComments= new ArrayList<>();
    protected PostCommentRowViewFactoryCallback mCallback = null;

    protected View mPostView = null;

    protected SimpleDateFormat originalTimeFormat;
    protected SimpleDateFormat targetTimeFormat;

    public PostCommentRowViewFactory(List<Comment> comments, View postView, final PostCommentRowViewFactoryCallback callback){
        mComments = new ArrayList<>(comments);
        mCallback = callback;

        mPostView = postView;

        originalTimeFormat = new SimpleDateFormat("yyyy-MM-dd HH:mm");
        targetTimeFormat = new SimpleDateFormat(Util.getString(R.string.post_datetime_format));
    }

    @Override
    public int getCount() {
        return mComments.size() + (mPostView == null ? 0 : 1);
    }

    @Override
    public Object getItem(int index) {
        if (mPostView != null && index == 0)
            return null;

        return mComments.get(index - (mPostView == null ? 0 : 1));
    }

    @Override
    public View getView(final Object object) {
        if (object == null){
            ViewGroup parent = (ViewGroup)mPostView.getParent();
            if (parent != null){
                parent.removeView(mPostView);
            }
            return mPostView;
        }

        final Comment comment = (Comment)object;
        LayoutInflater inflater = (LayoutInflater) MyApplication.getAppContext().getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        View rowView = inflater.inflate(R.layout.row_post_detail_comment, null, false);

        ImageView imgProfile = (ImageView)rowView.findViewById(R.id.img_profile);
        TextView txtName = (TextView)rowView.findViewById(R.id.txt_name);
        ImageView imgLike = (ImageView) rowView.findViewById(R.id.img_like);
        TextView txtDate = (TextView)rowView.findViewById(R.id.txt_date);
        TextView txtLikeCount = (TextView)rowView.findViewById(R.id.txt_like_count);
        TextView txtComment = (TextView)rowView.findViewById(R.id.txt_comment);

        String createTimeStr = "";
        try {
            Date createTimeDate = originalTimeFormat.parse(comment.create_time);
            createTimeStr = targetTimeFormat.format(createTimeDate);
        } catch (ParseException e) {
            e.printStackTrace();
        }
        txtDate.setText(createTimeStr);
        txtName.setText(comment.profile.user_name);
        txtLikeCount.setText(String.valueOf(comment.no_of_like));

        Picasso.with(MyApplication.getAppContext()).load(ServiceManager.sharedInstance().getImageUrl(ServiceManager.IMAGE_URL_PROFILE, comment.profile.image_id)).into(imgProfile);

        if (comment.is_like){
            imgLike.setColorFilter(Util.getColor(MyApplication.getAppContext(), R.color.theme_color));
        }
        else{
            imgLike.setColorFilter(Util.getColor(MyApplication.getAppContext(), R.color.main_text_color_lighter));
        }

        txtComment.setText(comment.description);

        if (mCallback != null){
            imgLike.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    mCallback.onClickLike(comment);
                }
            });
        }

        return rowView;
    }

    @Override
    public boolean modifyViewLayout(View contentView, Object object, int index) {
        Resources resources = MyApplication.getAppContext().getResources();
        RelativeLayout.LayoutParams params = (RelativeLayout.LayoutParams) contentView.getLayoutParams();
        if (mPostView != null && index == 0) {
            params.setMargins(0, 0, 0, 0);
        }
        else{
            params.setMargins(resources.getDimensionPixelSize(R.dimen.list_row_margin_horizontal), resources.getDimensionPixelSize(R.dimen.list_row_post_margin_top), resources.getDimensionPixelSize(R.dimen.list_row_margin_horizontal), resources.getDimensionPixelSize(R.dimen.list_row_post_margin_bottom));
        }
        return false;
    }
}
