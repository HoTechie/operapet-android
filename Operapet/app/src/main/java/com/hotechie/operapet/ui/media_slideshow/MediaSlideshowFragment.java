package com.hotechie.operapet.ui.media_slideshow;

import android.content.Context;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.view.PagerAdapter;
import android.support.v4.view.ViewPager;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;

import com.hotechie.operapet.R;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by duncan on 24/7/2016.
 */
public class MediaSlideshowFragment extends Fragment {
    private static String TAG = "MediaSlideshowFragment";

    public static MediaSlideshowFragment getInstance(Bundle b, List<MediaSlideshowViewModel> viewModels){
        MediaSlideshowFragment fragment = new MediaSlideshowFragment();
        fragment.setArguments(b);
        fragment.mViewModels = new ArrayList<>(viewModels);
        return fragment;
    }

    public static String KEY_IS_SHOW_INDICATOR = "is_show_indicator";
    public static String KEY_INDICATOR_FILL_RES = "indicator_fill_res";
    public static String KEY_INDICATOR_UNFILL_RES = "indicator_unfill_res";

    protected ViewPager mPager = null;
    protected List<MediaSlideshowViewModel> mViewModels = new ArrayList<>();

    protected boolean mIsShowIndicator = false;
    protected int mIndicatorFillRes = -1, mIndicatorUnfillRes = -1;
    protected LinearLayout mLayoutIndicator = null;

    @Override
    public void onActivityCreated(Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);

        if (getArguments() != null){
            mIsShowIndicator = getArguments().getBoolean(KEY_IS_SHOW_INDICATOR, mIsShowIndicator);
            mIndicatorFillRes = getArguments().getInt(KEY_INDICATOR_FILL_RES, mIndicatorFillRes);
            mIndicatorUnfillRes = getArguments().getInt(KEY_INDICATOR_UNFILL_RES, mIndicatorUnfillRes);
        }

        PagerAdapter adapter = getAdapter();
        mPager.setAdapter(adapter);


        mLayoutIndicator.setVisibility(mIsShowIndicator ? View.VISIBLE : View.GONE);
        if (mIsShowIndicator && mIndicatorFillRes > -1 && mIndicatorUnfillRes > -1){
            final List<ImageView> indicators = new ArrayList<>();
            for (int i = 0; i < adapter.getCount(); i ++){
                ImageView imgIndicator = new ImageView(mLayoutIndicator.getContext());
                imgIndicator.setScaleType(ImageView.ScaleType.CENTER_INSIDE);
                imgIndicator.setImageResource(i == 0 ? mIndicatorFillRes : mIndicatorUnfillRes);
                imgIndicator.setContentDescription("indicator");
                LinearLayout.LayoutParams params = new LinearLayout.LayoutParams(mLayoutIndicator.getLayoutParams().height, ViewGroup.LayoutParams.MATCH_PARENT);
                int halfHeight = mLayoutIndicator.getLayoutParams().height / 2;
                params.setMargins(halfHeight, 0, halfHeight, 0);
                mLayoutIndicator.addView(imgIndicator, params);
                indicators.add(imgIndicator);
            }

            mPager.addOnPageChangeListener(new ViewPager.OnPageChangeListener() {
                @Override
                public void onPageScrolled(int position, float positionOffset, int positionOffsetPixels) {

                }

                @Override
                public void onPageSelected(int position) {
                    for (int i = 0; i < indicators.size(); i ++){
                        ImageView imgView = indicators.get(i);
                        imgView.setImageResource(i == position ? mIndicatorFillRes : mIndicatorUnfillRes);
                    }
                }

                @Override
                public void onPageScrollStateChanged(int state) {

                }
            });
        }
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_media_slideshow, container, false);

        mPager = (ViewPager)view.findViewById(R.id.pager);

        mLayoutIndicator = (LinearLayout)view.findViewById(R.id.layout_indicator);

        return view;
    }

    protected MyPageAdapter getAdapter(){
        return new MyPageAdapter(getContext(), mViewModels);
    }


    protected class MyPageAdapter extends PagerAdapter{

        protected Context mContext = null;
        protected List<MediaSlideshowViewModel> mViewModels = new ArrayList<>();

        public MyPageAdapter(Context context, List<MediaSlideshowViewModel> viewModels){
            mContext = context;
            mViewModels = new ArrayList<>(viewModels);
        }

        @Override
        public int getCount() {
            return mViewModels.size();
        }

        @Override
        public boolean isViewFromObject(View view, Object object) {
            return view == object;
        }

        @Override
        public Object instantiateItem(ViewGroup collection, int position) {
            RelativeLayout container = new RelativeLayout(collection.getContext());

            MediaSlideshowViewModel model = mViewModels.get(position);
            model.fillView(container);
//
//            ImageView imageView = new ImageView(collection.getContext());
//            imageView.setScaleType(ImageView.ScaleType.CENTER_CROP);
//            //imageView.setImageResource();
//            RelativeLayout.LayoutParams imageViewLayoutParams = new RelativeLayout.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.MATCH_PARENT);
//            container.addView(imageView, imageViewLayoutParams);

            ((ViewPager)collection).addView(container, 0);
            return container;
        }

        @Override
        public void destroyItem(ViewGroup collection, int position, Object view) {
            ((ViewPager) collection).removeView((View) view);
        }
    }
}
