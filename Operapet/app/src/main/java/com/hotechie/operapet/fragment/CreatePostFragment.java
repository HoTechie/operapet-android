package com.hotechie.operapet.fragment;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.Point;
import android.net.Uri;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.Display;
import android.view.LayoutInflater;
import android.view.View;
import android.view.WindowManager;
import android.widget.ArrayAdapter;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.Spinner;
import android.widget.TextView;

import com.hotechie.operapet.Constant;
import com.hotechie.operapet.MyApplication;
import com.hotechie.operapet.R;
import com.hotechie.operapet.SessionManager;
import com.hotechie.operapet.Util;
import com.hotechie.operapet.service.PostsService;
import com.hotechie.operapet.service.ResponseHandler;
import com.hotechie.operapet.service.ServiceManager;
import com.hotechie.operapet.service.model.Post;
import com.hotechie.operapet.service.model.Profile;
import com.hotechie.operapet.service.model.ResponseWrapper;
import com.hotechie.operapet.service.model.Tag;
import com.squareup.picasso.Picasso;

import org.w3c.dom.Text;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import okhttp3.MediaType;
import okhttp3.RequestBody;
import retrofit2.Call;
import retrofit2.Response;

/**
 * Created by duncan on 11/10/2016.
 */

public class CreatePostFragment extends BaseFragment {
    private static String TAG = "CreatePostFragment";

    public static CreatePostFragment getInstance(){
        CreatePostFragment frag = new CreatePostFragment();
        return frag;
    }


    protected Post mPost = null;

    protected String mTagStr = "";
    protected List<String> mSelectedPhotos = new ArrayList<>();

    protected TextView mTxtDescription, mTxtProfileName;
    protected ImageView mImgProfile;
    protected EditText mEditTags;
    protected View mImgPhoto, mTxtSend;
    protected LinearLayout mLayoutPostImg;
    protected EditText mEditDescription;

    protected TextView mTxtCategory = null;
    protected Spinner mSpinnerCategory = null;

    @Override
    public String getTitle() {
        return Util.getString(R.string.title_create_post);
    }

    @Override
    public int layoutRes() {
        return R.layout.fragment_create_post;
    }

    @Override
    public void configUI() {
        super.configUI();

        mPost = new Post();


        mTxtDescription = (TextView)getView().findViewById(R.id.txt_description);
        mTxtProfileName = (TextView)getView().findViewById(R.id.txt_profile_name);

        mImgProfile = (ImageView)getView().findViewById(R.id.img_profile);
        mTxtSend = getView().findViewById(R.id.txt_send);
        mLayoutPostImg = (LinearLayout)getView().findViewById(R.id.layout_post_img);
        mEditDescription = (EditText)getView().findViewById(R.id.edit_description);

        mTxtCategory = (TextView)getView().findViewById(R.id.txt_category);
        mSpinnerCategory = (Spinner)getView().findViewById(R.id.spinner_category);
        List<String> options = new ArrayList<>();
        for (Constant.PostCategory cat : Constant.getPostCategoriesForUser()){
            options.add(cat.name);
        }
        ArrayAdapter<String> spinnerDataAdapter = new ArrayAdapter<String>(MyApplication.getAppContext(), R.layout.row_spinner_post_category, options);
        spinnerDataAdapter.setDropDownViewResource(R.layout.row_spinner_post_category);
        mSpinnerCategory.setAdapter(spinnerDataAdapter);


        Profile profile = SessionManager.sharedInstance().getProfile();
        mTxtProfileName.setText(profile.user_name);
        Picasso.with(MyApplication.getAppContext()).load(ServiceManager.sharedInstance().getImageUrl(ServiceManager.IMAGE_URL_PROFILE, profile.image_id)).into(mImgProfile);


        mEditTags = (EditText)getView().findViewById(R.id.edit_tags);
        mEditTags.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {
                String rawText = mEditTags.getText().toString();
                if (!rawText.equals(mTagStr)) {
                    if (rawText.contains(" ")) {
                        mTagStr = rawText.replace(" ", ",");
                        mEditTags.setText(mTagStr);
                        mEditTags.setSelection(mEditTags.getText().length());
                    }
                    else{
                        mTagStr = rawText;
                    }
                }
            }

            @Override
            public void afterTextChanged(Editable editable) {

            }
        });


        mImgPhoto = getView().findViewById(R.id.img_photo);
        mImgPhoto.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent();
                intent.setType("image/*");
                intent.setAction(Intent.ACTION_GET_CONTENT);
                startActivityForResult(Intent.createChooser(intent, getTitle()), 11);
            }
        });


        mTxtSend.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                // TODO create post

                String description = mEditDescription.getText().toString();
                if (description.length() == 0
                        && mSelectedPhotos.size() == 0){
                    return ;
                }

                showLoadingScreen(true, null, Util.getString(R.string.msg_loading));

                RequestBody rbDescription = RequestBody.create(MediaType.parse("text/plain"), description);
                RequestBody rbCategory = RequestBody.create(MediaType.parse("text/plain"), String.valueOf(Constant.getPostCategoriesForUser().get(mSpinnerCategory.getSelectedItemPosition()).id));
                RequestBody rbTags = mTagStr.length() == 0 ? null : RequestBody.create(MediaType.parse("text/plain"), mTagStr);

                List<RequestBody> rbPetImages = new ArrayList<RequestBody>();
                if (mSelectedPhotos != null) {
                    List<String> uriStrs = new ArrayList<String>(mSelectedPhotos);
                    for (int i = 0; i < uriStrs.size(); i ++) {
                        String uriStr = uriStrs.get(i);
                        Bitmap bitmap = getBitmap(uriStr, 1000);
                        File file = new File(MyApplication.getAppContext().getCacheDir(), String.valueOf(i) + ".jpg");
                        FileOutputStream outStream = null;
                        try {
                            outStream = new FileOutputStream(file);
                            bitmap.compress(Bitmap.CompressFormat.JPEG, 100, outStream);
                            outStream.flush();
                            outStream.close();
                            Log.i(TAG, file.getAbsolutePath());
                        } catch (FileNotFoundException e) {
                            e.printStackTrace();
                        } catch (IOException e) {
                            e.printStackTrace();
                        }

                        rbPetImages.add(RequestBody.create(MediaType.parse("image/jpeg"), file));
                    }
                }


                PostsService service = ServiceManager.sharedInstance().getServiceHandler().create(PostsService.class);
                Call<ResponseWrapper<Post>> call = service.postCreatePost(rbDescription,
                        rbCategory,
                        rbTags,
                        rbPetImages.size() >= 1 ? rbPetImages.get(0) : null,
                        rbPetImages.size() >= 2 ? rbPetImages.get(1) : null,
                        rbPetImages.size() >= 3 ? rbPetImages.get(2) : null,
                        rbPetImages.size() >= 4 ? rbPetImages.get(3) : null,
                        rbPetImages.size() >= 5 ? rbPetImages.get(4) : null);
                call.enqueue(new ResponseHandler<ResponseWrapper<Post>>() {
                    @Override
                    public void onResponseParsed(Call<ResponseWrapper<Post>> call, Response<ResponseWrapper<Post>> response) {

                        showLoadingScreen(false, null, null);

                        if (response.body() != null){
                            onShowMessage(Util.getString(R.string.post_create_success), Util.getString(R.string.msg_ok), null);
                            Activity activity = getActivity();
                            if (activity != null && !activity.isFinishing()){
                                Util.backToActivity(activity, true);
                            }
                        }
                        else{
                            onShowMessage(Util.getString(R.string.msg_error), Util.getString(R.string.msg_ok), null);
                        }
                    }

                    @Override
                    public void onFailure(Call<ResponseWrapper<Post>> call, Throwable t) {
                        showLoadingScreen(false, null, null);
                        onShowMessage(Util.getString(R.string.msg_error), Util.getString(R.string.msg_ok), null);
                    }
                });
            }
        });
    }

    @Override
    protected void updateUIRunnable() {
        super.updateUIRunnable();
        if (mSelectedPhotos != null) {
            Display display = ((WindowManager)MyApplication.getAppContext().getSystemService(Context.WINDOW_SERVICE)).getDefaultDisplay();
            Point size = new Point();
            display.getSize(size);
            final int width = (int)(size.x / 5);

            mLayoutPostImg.removeAllViews();
            for (final String uriStr : mSelectedPhotos) {
                Bitmap bitmap = getBitmap(uriStr, width);
                ImageView imageView = new ImageView(mLayoutPostImg.getContext());
                imageView.setImageBitmap(bitmap);
                imageView.setScaleType(ImageView.ScaleType.CENTER_CROP);
                imageView.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        mSelectedPhotos.remove(uriStr);
                        updateUI();
                    }
                });
                mLayoutPostImg.addView(imageView, width, width);
            }
        }
    }




    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        if (resultCode == Activity.RESULT_OK){
            Uri selectedImageUri = data.getData();
            if (selectedImageUri != null) {
                Log.i(TAG, "selected image: " + selectedImageUri.toString());
                mSelectedPhotos.add(selectedImageUri.toString());
                updateUI();
            }
            else{
                Log.e(TAG, "failed to select image");
            }

        }
    }
}
