package com.hotechie.operapet;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.content.res.Resources;
import android.graphics.drawable.Drawable;
import android.os.Build;
import android.os.Looper;

/**
 * Created by duncan on 17/7/2016.
 */
public class Util extends Object {

    /* activity transition */
    public static final String INTENT_EXPECT_RESULT_FLAG = "expect_result";
    public static void moveToActivity(final Activity fromActivity, final Intent intent, final boolean isAnimated){
        if (Looper.getMainLooper() != Looper.myLooper()){
            fromActivity.runOnUiThread(new Runnable() {
                @Override
                public void run() {
                    moveToActivity(fromActivity, intent, isAnimated);
                }
            });
        }
        else {
            intent.putExtra(INTENT_EXPECT_RESULT_FLAG, false);
            intent.addFlags(Intent.FLAG_ACTIVITY_NO_ANIMATION);
            fromActivity.startActivity(intent);
            if (isAnimated)
                fromActivity.overridePendingTransition(R.anim.right_slide_in, R.anim.left_slide_out);
            else
                fromActivity.overridePendingTransition(0, 0);
        }
    }
    public static void backToActivity(final Activity fromActivity, final boolean isAnimated){
        if (Looper.getMainLooper() != Looper.myLooper()){
            fromActivity.runOnUiThread(new Runnable() {
                @Override
                public void run() {
                    backToActivity(fromActivity, isAnimated);
                }
            });
        }
        else {
            fromActivity.finish();
            if (isAnimated)
                fromActivity.overridePendingTransition(R.anim.left_slide_in, R.anim.right_slide_out);
            else
                fromActivity.overridePendingTransition(0, 0);
        }
    }

    public static void moveToActivityForResult(final Activity fromActivity, final Intent intent, final int code, final boolean isAnimated) {
        if (Looper.getMainLooper() != Looper.myLooper()){
            fromActivity.runOnUiThread(new Runnable() {
                @Override
                public void run() {
                    moveToActivityForResult(fromActivity, intent, code, isAnimated);
                }
            });
        }
        else {

            intent.putExtra(INTENT_EXPECT_RESULT_FLAG, true);
            intent.addFlags(Intent.FLAG_ACTIVITY_NO_ANIMATION);
            fromActivity.startActivityForResult(intent, code);
            if (isAnimated)
                fromActivity.overridePendingTransition(R.anim.right_slide_in, R.anim.left_slide_out);
            else
                fromActivity.overridePendingTransition(0, 0);
        }
    }


    public static Drawable getDrawable(Context context, int id) {
        Resources resources = context.getResources();
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            return resources.getDrawable(id, context.getTheme());
        } else {
            return resources.getDrawable(id);
        }
    }


    public static int getColor(Context context, int id) {
        Resources resources = context.getResources();
        if (Build.VERSION.SDK_INT >= 23) {
            return resources.getColor(id, context.getTheme());
        } else {
            return resources.getColor(id);
        }
    }

    public static String getString(int idRes){
        return MyApplication.getAppContext().getString(idRes);
    }

}
