package com.hotechie.operapet.fragment;

import android.content.DialogInterface;
import android.os.Bundle;

import com.hotechie.operapet.R;
import com.hotechie.operapet.Util;
import com.hotechie.operapet.service.PostsService;
import com.hotechie.operapet.service.ResponseHandler;
import com.hotechie.operapet.service.ServiceManager;
import com.hotechie.operapet.service.model.Post;
import com.hotechie.operapet.service.model.ResponseListWrapper;
import com.hotechie.operapet.service.model.Tag;
import com.hotechie.operapet.ui.row_view_factory.PostRowViewFactory;
import com.hotechie.util.MessageDialogFragment;

import java.util.ArrayList;

import retrofit2.Call;
import retrofit2.Response;

/**
 * Created by duncan on 13/10/2016.
 */

public class PostByTagListFragment extends PostListFragment {
    private static String TAG = "PostsListFragment";

    public static PostByTagListFragment getInstance(Tag tag){
        PostByTagListFragment frag = new PostByTagListFragment();
        frag.mTag = tag;
        return frag;
    }

    protected Tag mTag = null;

    @Override
    public int layoutRes() {
        return R.layout.fragment_base_list;
    }

    @Override
    public String getTitle() {
        return "#" + mTag.description;
    }

    @Override
    public void updateData() {
        PostsService service = ServiceManager.sharedInstance().getServiceHandler().create(PostsService.class);
        Call<ResponseListWrapper<Post>> call = service.getPostByTagId(mTag.id > 0 ? mTag.id : mTag.post_tag_id);
        call.enqueue(new ResponseHandler<ResponseListWrapper<Post>>() {

            @Override
            public void onResponseParsed(Call<ResponseListWrapper<Post>> call, Response<ResponseListWrapper<Post>> response) {
                // FIXME pagination
                if (response.body() != null && response.body().data != null) {
                    mPosts = new ArrayList<Post>(response.body().data);
                    mFactory = new PostRowViewFactory(mPosts, mFactoryCallback);
                    updateUI();
                }
            }

            @Override
            public void onFailure(Call<ResponseListWrapper<Post>> call, Throwable t) {
                onHandleError(Util.getString(R.string.msg_error), null, null, new MessageDialogFragment.MessageDialogFragmentCallback() {

                    @Override
                    public void onClickPositiveButton(MessageDialogFragment fragment, DialogInterface dialog, int id) {
                        updateData();
                    }

                    @Override
                    public void onClickNegativeButton(MessageDialogFragment fragment, DialogInterface dialog, int id) {

                    }
                });
            }
        });

    }
}
