package com.hotechie.operapet.service.model;

/**
 * Created by duncan on 21/10/2016.
 */

public class ActionHistory extends BaseModel {
    /*
    {
        "id": 29,
        "type": 501,
        "credit": 1,
        "description": "你剛剛在自己的Wall出post",
        "create_time": "2016-10-19T18:41:08.423"
    }
     */

    public int id = 0;
    public int type;
    public int credit;
    public String description;
    public String create_time;
}
