package com.hotechie.operapet.fragment;

import android.app.Activity;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.View;
import android.widget.EditText;
import android.widget.TextView;

import com.hotechie.operapet.MyApplication;
import com.hotechie.operapet.R;
import com.hotechie.operapet.SessionManager;
import com.hotechie.operapet.Util;
import com.hotechie.operapet.service.model.PinCard;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;

/**
 * Created by duncan on 9/10/2016.
 */

public class PetDetailPinCardFragment extends BaseFragment {
    private static String TAG = "PetDetailPinCardFrag";

    public static interface PetDetailPinCardFragmentCallback{
        public void onSave(PinCard pinCard);
        public void onBackPressed(PinCard pinCard);
    }

    public static PetDetailPinCardFragment getInstance(PinCard pinCard, PetDetailPinCardFragmentCallback callback){
        PetDetailPinCardFragment frag = new PetDetailPinCardFragment();
        frag.mPinCard = pinCard; // cannot be null
        frag.mCallback = callback;
        return frag;
    }


    protected SimpleDateFormat originalTimeFormat;

    protected PetDetailPinCardFragmentCallback mCallback = null;

    protected PinCard mPinCard = null;
    protected EditText mTxtName, mTxtPinCardLastYear, mTxtPinCardLastMonth, mTxtPinCardLastDay, mTxtPinCardNextYear, mTxtPinCardNextMonth, mTxtPinCardNextDay, mTxtPinCardDescription;
    protected View mTxtSave;

    @Override
    public int layoutRes() {
        return R.layout.fragment_pet_detail_pin_card;
    }

    @Override
    public String getTitle() {
        return Util.getString(R.string.pet_lb_pin_card);
    }

    @Override
    public void configUI() {
        super.configUI();

        if (mPinCard == null){
            mPinCard = new PinCard();
            Log.e(TAG, "mPinCard should not be null");
        }

        originalTimeFormat = new SimpleDateFormat("yyyy-MM-dd HH:mm");

        mTxtName = (EditText)getView().findViewById(R.id.txt_name);

        mTxtPinCardLastYear = (EditText)getView().findViewById(R.id.txt_pin_card_last_year);
        mTxtPinCardLastMonth = (EditText)getView().findViewById(R.id.txt_pin_card_last_month);
        mTxtPinCardLastDay = (EditText)getView().findViewById(R.id.txt_pin_card_last_day);
        mTxtPinCardNextYear = (EditText)getView().findViewById(R.id.txt_pin_card_next_year);
        mTxtPinCardNextMonth = (EditText)getView().findViewById(R.id.txt_pin_card_next_month);
        mTxtPinCardNextDay = (EditText)getView().findViewById(R.id.txt_pin_card_next_day);
        mTxtPinCardDescription = (EditText)getView().findViewById(R.id.txt_pin_card_description);

        TextWatcher textWatcher = new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {
                try{
                    final Integer lastMonth = mTxtPinCardLastMonth.getText().length() > 0 ? Integer.parseInt(mTxtPinCardLastMonth.getText().toString()) : null;
                    final Integer lastDay = mTxtPinCardLastDay.getText().length() > 0 ? Integer.parseInt(mTxtPinCardLastDay.getText().toString()) : null;
                    final Integer nextMonth = mTxtPinCardNextMonth.getText().length() > 0 ? Integer.parseInt(mTxtPinCardNextMonth.getText().toString()) : null;
                    final Integer nextDay = mTxtPinCardNextDay.getText().length() > 0 ? Integer.parseInt(mTxtPinCardNextDay.getText().toString()) : null;

                    mHandler.post(new Runnable() {
                        @Override
                        public void run() {
                            if (lastMonth != null) {
                                if (lastMonth < 1) mTxtPinCardLastMonth.setText("1");
                                else if (lastMonth > 12) mTxtPinCardLastMonth.setText("12");
                            }
                            if (nextMonth != null) {
                                if (nextMonth < 1) mTxtPinCardNextMonth.setText("1");
                                else if (nextMonth > 12) mTxtPinCardNextMonth.setText("12");
                            }
                            if (lastDay != null) {
                                if (lastDay < 1) mTxtPinCardLastDay.setText("1");
                                else if (lastDay > 31) mTxtPinCardLastDay.setText("31");
                            }
                            if (nextDay != null) {
                                if (nextDay < 1) mTxtPinCardNextDay.setText("1");
                                else if (nextDay > 31) mTxtPinCardNextDay.setText("12");
                            }
                        }
                    });
                }
                catch (Exception e){

                }
            }

            @Override
            public void afterTextChanged(Editable editable) {
            }
        };
        mTxtPinCardLastMonth.addTextChangedListener(textWatcher);
        mTxtPinCardLastDay.addTextChangedListener(textWatcher);
        mTxtPinCardNextMonth.addTextChangedListener(textWatcher);
        mTxtPinCardNextDay.addTextChangedListener(textWatcher);


        mTxtSave = getView().findViewById(R.id.txt_save);
        mTxtSave.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                DateFormat df = new SimpleDateFormat("yyyy-MM-dd 00:00");
                String defaultDateStr = df.format(new Date());

                String lastYear = mTxtPinCardLastYear.getText().toString();
                String lastMonth = mTxtPinCardLastMonth.getText().toString();
                String lastDay = mTxtPinCardLastDay.getText().toString();
                mPinCard.recent_pin_record_date = defaultDateStr;
                if (lastYear.length() != 0 && lastMonth.length() != 0 && lastDay.length() != 0) {
                    String date = "";
                    try{
                        String str = String.format("%d-%d-%d 00:00", Integer.parseInt(lastYear), Integer.parseInt(lastMonth), Integer.parseInt(lastDay));
                        originalTimeFormat.parse(str);
                        date = str;
                    }
                    catch (Exception e){
                        e.printStackTrace();
                    }
                    mPinCard.recent_pin_record_date = date;
                }

                String nextYear = mTxtPinCardNextYear.getText().toString();
                String nextMonth = mTxtPinCardNextMonth.getText().toString();
                String nextDay = mTxtPinCardNextDay.getText().toString();
                mPinCard.next_pin_record_date = defaultDateStr;
                if (nextYear.length() != 0 && nextMonth.length() != 0 && nextDay.length() != 0) {
                    String date = "";
                    try{
                        String str = String.format("%d-%d-%d 00:00", Integer.parseInt(nextYear), Integer.parseInt(nextMonth), Integer.parseInt(nextDay));
                        originalTimeFormat.parse(str);
                        date = str;
                    }
                    catch (Exception e){
                        e.printStackTrace();
                    }
                    mPinCard.next_pin_record_date = date;
                }

                mPinCard.name = mTxtName.getText().toString();
                mPinCard.description = mTxtPinCardDescription.getText().toString();

                if (mPinCard.name.length() == 0){
                    onShowMessage(Util.getString(R.string.msg_invalid_info), Util.getString(R.string.msg_ok), null);
                }
                else{
                    // TODO save
                    if (mCallback != null){
                        mCallback.onSave(mPinCard);
                    }
                    else {
                        SessionManager.sharedInstance().putIn(SessionManager.VAULT_PIN_CARD_EDIT, mPinCard);

                        Activity activity = getActivity();
                        if (activity != null && !activity.isFinishing()) {
                            activity.onBackPressed();
                        }
                    }
                }
            }
        });


        updateUI();
    }

    @Override
    protected void updateUIRunnable() {
        super.updateUIRunnable();

        PinCard pinCard = mPinCard;

        Calendar lastCalendar = Calendar.getInstance();
        Calendar nextCalendar = Calendar.getInstance();
        try {
            Date lastDate = originalTimeFormat.parse(pinCard.recent_pin_record_date);
            lastCalendar.setTime(lastDate);

            mTxtPinCardLastYear.setText(String.valueOf(lastCalendar.get(Calendar.YEAR)));
            mTxtPinCardLastMonth.setText(String.valueOf(lastCalendar.get(Calendar.MONTH) + 1));
            mTxtPinCardLastDay.setText(String.valueOf(lastCalendar.get(Calendar.DAY_OF_MONTH)));
        } catch (Exception e) {
            e.printStackTrace();
        }

        try {
            Date nextDate = originalTimeFormat.parse(pinCard.next_pin_record_date);
            nextCalendar.setTime(nextDate);

            mTxtPinCardNextYear.setText(String.valueOf(nextCalendar.get(Calendar.YEAR)));
            mTxtPinCardNextMonth.setText(String.valueOf(nextCalendar.get(Calendar.MONTH) + 1));
            mTxtPinCardNextDay.setText(String.valueOf(nextCalendar.get(Calendar.DAY_OF_MONTH)));
        } catch (Exception e) {
            e.printStackTrace();
        }

        mTxtName.setText(pinCard.name);
        mTxtPinCardDescription.setText(pinCard.description);
    }

}
