package com.hotechie.operapet.ui.row_view_factory;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.TextView;

import com.hotechie.operapet.MyApplication;
import com.hotechie.operapet.R;
import com.hotechie.operapet.Util;
import com.hotechie.operapet.service.model.Profile;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by duncan on 15/10/2016.
 */

public class MyFriendRowViewFactory extends UserRowViewFactory {
    public static interface MyFriendRowViewFactoryCallback{
        public void onClick(Profile profile);
        public void onClickFriendRequestEntrance(Profile profile);
    }


    protected List<Profile> mFriendRequests = new ArrayList<>();

    protected MyFriendRowViewFactoryCallback mCallback = null;

    public MyFriendRowViewFactory(List<Profile> users, List<Profile> friendRequests, MyFriendRowViewFactoryCallback callback) {
        super(users, false);

        mRowViewRes = R.layout.row_friend_request_entrance;
        mCallback = callback;

        if (friendRequests != null){
            mFriendRequests = new ArrayList<>(friendRequests);
        }
    }

    @Override
    public int getCount() {
        return super.getCount() + 1 + (mFriendRequests.size() > 0 ? 2 : 0);
    }

    @Override
    public Object getItem(int index) {
        if (mFriendRequests.size() > 0){
            if (index == 0) {
                Profile profile = new Profile();
                profile.id = 0;
                profile.user_name = Util.getString(R.string.friend_title_friend_request);
                return profile;
            }
            else if (index == 1){
                return mFriendRequests.get(0);
            }
            else if (index == 2){
                Profile profile = new Profile();
                profile.id = 0;
                profile.user_name = Util.getString(R.string.friend_title_my_friend);
                return profile;
            }
        }
        else if (index == 0){
            Profile profile = new Profile();
            profile.id = 0;
            profile.user_name = Util.getString(R.string.friend_title_my_friend);
            return profile;
        }

        return super.getItem(index - 1 - (mFriendRequests.size() > 0 ? 2 : 0));
    }

    @Override
    public View getView(Object object) {
        final Profile profile = (Profile)object;
        View rowView = null;

        if (profile.id == 0){
            LayoutInflater inflater = (LayoutInflater) MyApplication.getAppContext().getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            rowView = inflater.inflate(R.layout.row_info, null, false);
            ((TextView)rowView.findViewById(R.id.txt_title)).setText(profile.user_name);
        }
        else{
            rowView = super.getView(object);
            if (mFriendRequests.size() > 0 && object == mFriendRequests.get(0)){
                rowView.findViewById(R.id.layout_count).setVisibility(View.VISIBLE);
                ((TextView)rowView.findViewById(R.id.txt_count)).setText(mFriendRequests.size() > 10 ? "+10" : String.valueOf(mFriendRequests.size()));
                if (mCallback != null){
                    rowView.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View view) {
                            mCallback.onClickFriendRequestEntrance(profile);
                        }
                    });
                }
            }
            else{
                if (mCallback != null){
                    rowView.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View view) {
                            mCallback.onClick(profile);
                        }
                    });
                }
            }
        }

        return rowView;
    }

    @Override
    public boolean modifyViewLayout(View contentView, Object object, int index) {
        return false;
    }
}
