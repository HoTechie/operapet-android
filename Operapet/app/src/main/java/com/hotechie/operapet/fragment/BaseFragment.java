package com.hotechie.operapet.fragment;


import android.app.ProgressDialog;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.net.Uri;
import android.os.Bundle;
import android.os.Handler;
import android.os.Looper;
import android.support.v4.app.DialogFragment;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.hotechie.operapet.BaseFragmentActivity;
import com.hotechie.operapet.MyApplication;
import com.hotechie.operapet.R;
import com.hotechie.operapet.Util;
import com.hotechie.operapet.ui.MyMessageDialogFragment;
import com.hotechie.util.ListDialogFragment;
import com.hotechie.util.MessageDialogFragment;

import java.io.FileNotFoundException;
import java.io.InputStream;
import java.util.List;

/**
 * A simple {@link Fragment} subclass.
 */
public class BaseFragment extends Fragment {
    private static String TAG = "BaseFragment";


    protected Handler mHandler = new Handler();

    public static BaseFragment getInstance(Bundle b){
        BaseFragment frag = new BaseFragment();
        frag.setArguments(b);
        return frag;
    }



    public BaseFragment() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        return inflater.inflate(layoutRes(), container, false);
    }

    public int layoutRes(){
        return R.layout.fragment_base;
    }

    /**
     * Intented to be called by parent activity
     * @param view image view
     * @return is handled
     */
    public boolean onClickActionBarItem(View view){
        return false;
    }


    public String getTitle(){
        return "";
    }

    /**
     * Call once after creating the view. getActivity() and getView() will work correctly
     */
    public void configUI(){

    }

    /**
     * Call REPEATEDLY
     */
    public void updateUI(){
        if (Looper.getMainLooper() != Looper.myLooper()) {
            if (getView() != null) {
                getView().post(new Runnable() {
                    @Override
                    public void run() {
                        updateUIRunnable();
                    }
                });
            }
        }
        else{
            updateUIRunnable();
        }
    }

    /**
     * Call REPEATEDLY, override this method to change the updateUI behaviours
     */
    protected void updateUIRunnable(){

    }

    /**
     * Call to update data from data source
     */
    public void updateData(){

    }

    @Override
    public void onActivityCreated(Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        configUI();
    }


    private static DialogFragment mCurrentDialogFragment = null;

    View errorView = null;
    protected void onHandleError(final String msg, final String btnPositive, final String btnNegative, final MessageDialogFragment.MessageDialogFragmentCallback callback){
        Bundle b = new Bundle();
//        b.putString(MessageDialogFragment.KEY_TITLE,Util.getString(R.string.msg_error));
        if (msg != null)
            b.putString(MessageDialogFragment.KEY_MESSAGE, msg);
        else
            b.putString(MessageDialogFragment.KEY_MESSAGE, Util.getString(R.string.msg_error));

        if (btnPositive != null)
            b.putString(MessageDialogFragment.KEY_POSITIVE_BTN, btnPositive);
        else
            b.putString(MessageDialogFragment.KEY_POSITIVE_BTN,Util.getString(R.string.msg_reload));

        if (btnNegative != null)
            b.putString(MessageDialogFragment.KEY_NEGATIVE_BTN, btnNegative);
        else
            b.putString(MessageDialogFragment.KEY_NEGATIVE_BTN,Util.getString(R.string.msg_cancel));


        MyMessageDialogFragment frag = MyMessageDialogFragment.getInstance(b, callback);
        frag.setCancelable(false);
//        FragmentManager fm = getFragmentManager();
//        FragmentTransaction transaction = fm.beginTransaction();
//        transaction.show(frag);
//        transaction.commitAllowingStateLoss();
//        frag.show(getFragmentManager(), "error_message");

        try {
            frag.show(getFragmentManager(), "error_message");
        }
        catch (Exception e){
            e.printStackTrace();
        }

    }


    protected void onHandleSelection(String title, String[] itemList, ListDialogFragment.ListDialogFragmentCallback callback){
        Bundle b = new Bundle();
        b.putString(ListDialogFragment.KEY_TITLE, title);
        b.putStringArray(ListDialogFragment.KEY_ITEM_LIST, itemList);
        b.putBoolean(ListDialogFragment.KEY_IS_SINGLE_CHOICE, true);

        ListDialogFragment frag = ListDialogFragment.getInstance(b, callback);
//        frag.setCancelable(false);
//        frag.show(getFragmentManager(), "selection_list");
        try {
            frag.show(getFragmentManager(), "selection_list");
        }
        catch (Exception e){
            e.printStackTrace();
        }
//        FragmentManager fm = getFragmentManager();
//        FragmentTransaction transaction = fm.beginTransaction();
//        transaction.show(frag);
//        transaction.commitAllowingStateLoss();
    }

    protected void onHandleSelection(String title, List<String> itemList, int selection, ListDialogFragment.ListDialogFragmentCallback callback){

        String[] arr = new String[itemList.size()];
        Bundle b = new Bundle();
        b.putString(ListDialogFragment.KEY_TITLE, title);
        b.putStringArray(ListDialogFragment.KEY_ITEM_LIST, itemList.toArray(arr));
        b.putBoolean(ListDialogFragment.KEY_IS_SINGLE_CHOICE, true);
        b.putInt(ListDialogFragment.KEY_SINGLE_CHOICE_SELECTED, selection);

        final ListDialogFragment frag = ListDialogFragment.getInstance(b, callback);
        frag.setCancelable(false);
        try {
            frag.show(getFragmentManager(), "selection_list");
        }
        catch (Exception e){
            e.printStackTrace();
        }
//        mHandler.post(new Runnable() {
//            @Override
//            public void run() {
//                FragmentManager fm = getFragmentManager();
//                FragmentTransaction transaction = fm.beginTransaction();
//                transaction.show(frag);
//                transaction.commitAllowingStateLoss();
//            }
//        });
    }


    protected void onHandleSelection(String title, List<String> itemList, ListDialogFragment.ListDialogFragmentCallback callback){

        String[] arr = new String[itemList.size()];
        onHandleSelection(title, itemList.toArray(arr), callback);
    }

    protected void onShowMessage(String msg, String btnPositive, MessageDialogFragment.MessageDialogFragmentCallback callback){
        Bundle b = new Bundle();
//        b.putString(MessageDialogFragment.KEY_TITLE,Util.getString(R.string.msg_error));
        if (msg != null)
            b.putString(MessageDialogFragment.KEY_MESSAGE, msg);
        else
            b.putString(MessageDialogFragment.KEY_MESSAGE,Util.getString(R.string.msg_error));

        if (btnPositive != null)
            b.putString(MessageDialogFragment.KEY_POSITIVE_BTN, btnPositive);
        else
            b.putString(MessageDialogFragment.KEY_POSITIVE_BTN,Util.getString(R.string.msg_ok));


        MyMessageDialogFragment frag = MyMessageDialogFragment.getInstance(b, callback);
        frag.setCancelable(false);
        try {

            frag.show(getFragmentManager(), "error_message");
        }
        catch (Exception e){
            e.printStackTrace();
        }
//        FragmentManager fm = getFragmentManager();
//        FragmentTransaction transaction = fm.beginTransaction();
//        transaction.show(frag);
//        transaction.commitAllowingStateLoss();
    }


    protected void onShowMessage(String msg, String btnPositive, String btnNegative, MessageDialogFragment.MessageDialogFragmentCallback callback){
        onHandleError(msg, btnPositive == null ? Util.getString(R.string.msg_ok) : btnPositive, btnNegative, callback);
    }


    protected void replaceContentFragment(int layoutRes, Fragment frag){
        if (getActivity() != null && !getActivity().isFinishing()) {
            FragmentManager fragmentManager = getFragmentManager();
            FragmentTransaction fragmentTransaction = fragmentManager.beginTransaction();
            fragmentTransaction.replace(layoutRes, frag).commitAllowingStateLoss();
        }
    }

    public void showLoadingScreen(final boolean isShow, final String title, final String msg){
        BaseFragmentActivity activity = (BaseFragmentActivity)getActivity();
        if (activity != null){
            activity.showLoadingScreen(isShow, title, msg);
        }
    }



    protected Bitmap getBitmap(String uriStr, int maxSize){

        Bitmap resultBitmap = null;
        Uri uri = Uri.parse(uriStr);


        InputStream is = null;
        try {
            is = MyApplication.getAppContext().getContentResolver().openInputStream(uri);
            resultBitmap = BitmapFactory.decodeStream(is);
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        }

        if (resultBitmap == null){
            return null;
        }
        else {
            resultBitmap = getResizedBitmap(resultBitmap, maxSize);

            return resultBitmap;
        }
    }

    /**
     * reduces the size of the image
     * @param image
     * @param maxSize
     * @return
     */
    public Bitmap getResizedBitmap(Bitmap image, int maxSize) {
        int width = image.getWidth();
        int height = image.getHeight();

        float bitmapRatio = (float)width / (float) height;
        if (bitmapRatio > 0) {
            width = maxSize;
            height = (int) (width / bitmapRatio);
        } else {
            height = maxSize;
            width = (int) (height * bitmapRatio);
        }

        return Bitmap.createScaledBitmap(image, width, height, true);
    }
}
