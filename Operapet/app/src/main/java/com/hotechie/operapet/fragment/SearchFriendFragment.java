package com.hotechie.operapet.fragment;

import android.app.Activity;
import android.content.Intent;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.View;
import android.widget.EditText;

import com.hotechie.operapet.Constant;
import com.hotechie.operapet.InnerPageFragmentActivity;
import com.hotechie.operapet.R;
import com.hotechie.operapet.SessionManager;
import com.hotechie.operapet.Util;
import com.hotechie.operapet.service.MemberService;
import com.hotechie.operapet.service.PostsService;
import com.hotechie.operapet.service.ResponseHandler;
import com.hotechie.operapet.service.ServiceManager;
import com.hotechie.operapet.service.model.BaseModel;
import com.hotechie.operapet.service.model.Profile;
import com.hotechie.operapet.service.model.ResponseListWrapper;
import com.hotechie.operapet.service.model.ResponseWrapper;
import com.hotechie.operapet.service.model.Tag;
import com.hotechie.operapet.ui.row_view_factory.MyFriendRowViewFactory;
import com.hotechie.operapet.ui.row_view_factory.RowViewFactoryListAdapter;
import com.hotechie.operapet.ui.row_view_factory.SearchFriendRowViewFactory;
import com.hotechie.operapet.ui.row_view_factory.TagRowViewFactory;
import com.hotechie.operapet.ui.row_view_factory.UserRowViewFactory;

import java.lang.reflect.Member;
import java.util.ArrayList;
import java.util.List;

import okhttp3.MediaType;
import okhttp3.RequestBody;
import retrofit2.Call;
import retrofit2.Response;

/**
 * Created by duncan on 15/10/2016.
 */

public class SearchFriendFragment extends BaseListFragment {
    private static String TAG = "SearchFriendFrag";

    public static SearchFriendFragment getInstance (){
        SearchFriendFragment frag = new SearchFriendFragment();
        return frag;
    }

    protected String mSearch = "";
    protected Call<ResponseListWrapper<Profile>> mSearchCall = null;

    protected List<Profile> mMyFriends = new ArrayList<>();
    protected List<Profile> mFriendRequests = new ArrayList<>();

    protected MyFriendRowViewFactory mFactory = null;
    protected MyFriendRowViewFactory.MyFriendRowViewFactoryCallback mFactoryCallback = null;


    protected List<Profile> mSearchUsers = new ArrayList<>();

    protected SearchFriendRowViewFactory mSearchFactory = null;
    protected SearchFriendRowViewFactory.SearchFriendRowViewFactoryCallback mSearchFactoryCallback = null;

    protected View mImgNoResult = null;

    protected EditText mEditSearch = null;
    protected View mImgSearchBtnClear = null;

    @Override
    public void onResume() {
        super.onResume();

        updateData();
    }

    @Override
    public String getTitle() {
        return Util.getString(R.string.title_search_friend);
    }

    @Override
    public int layoutRes() {
        return R.layout.fragment_search_friend;
    }

    @Override
    public void configUI() {
        super.configUI();

        mFactoryCallback = new MyFriendRowViewFactory.MyFriendRowViewFactoryCallback() {
            @Override
            public void onClick(Profile profile) {

                WallFragment frag = WallFragment.getInstance(profile.id);
                MenuFragment.setNextContentFragment(frag);
                Activity activity = getActivity();
                if (activity != null && !activity.isFinishing()) {
                    Intent intent = new Intent(activity, InnerPageFragmentActivity.class);
                    Util.moveToActivity(activity, intent, true);
                }
            }

            @Override
            public void onClickFriendRequestEntrance(Profile profile) {
                // TODO show friend request page
                FriendRequestListFragment frag = FriendRequestListFragment.getInstance(profile.id);
                MenuFragment.setNextContentFragment(frag);
                Activity activity = getActivity();
                if (activity != null && !activity.isFinishing()) {
                    Intent intent = new Intent(activity, InnerPageFragmentActivity.class);
                    Util.moveToActivity(activity, intent, true);
                }
            }
        };
        mFactory = new MyFriendRowViewFactory(mMyFriends, mFriendRequests, mFactoryCallback);
        mAdapter = new RowViewFactoryListAdapter(mFactory);
        mListView.setAdapter(mAdapter);


        mSearchFactoryCallback = new SearchFriendRowViewFactory.SearchFriendRowViewFactoryCallback() {
            @Override
            public void onClick(Profile profile) {

                WallFragment frag = WallFragment.getInstance(profile.id);
                MenuFragment.setNextContentFragment(frag);
                Activity activity = getActivity();
                if (activity != null && !activity.isFinishing()) {
                    Intent intent = new Intent(activity, InnerPageFragmentActivity.class);
                    Util.moveToActivity(activity, intent, true);
                }
            }

            @Override
            public void onClickFriendStatus(Profile profile, Integer status) {
                if (status == null || status == Constant.FRIENDSHIP_STATUS_NO_FRIEND){
                    // TODO add friend
                    RequestBody rbTargetMemberId = RequestBody.create(MediaType.parse("text/plain"), String.valueOf(profile.id));

                    MemberService service = ServiceManager.sharedInstance().getServiceHandler().create(MemberService.class);
                    Call<ResponseWrapper<BaseModel>> call = service.postRequestFriend(rbTargetMemberId);
                    call.enqueue(new ResponseHandler<ResponseWrapper<BaseModel>>() {
                        @Override
                        public void onResponseParsed(Call<ResponseWrapper<BaseModel>> call, Response<ResponseWrapper<BaseModel>> response) {
                            updateData();
                        }

                        @Override
                        public void onFailure(Call<ResponseWrapper<BaseModel>> call, Throwable t) {
                            onShowMessage(Util.getString(R.string.msg_error), Util.getString(R.string.msg_ok), null);
                            updateData();
                        }
                    });
                }
            }
        };
        mSearchFactory = new SearchFriendRowViewFactory(mSearchUsers, mSearchFactoryCallback);


        mImgNoResult = getView().findViewById(R.id.img_no_result);

        mEditSearch = (EditText) getView().findViewById(R.id.edit_search);
        mImgSearchBtnClear = getView().findViewById(R.id.img_btn_search_clear);


        mEditSearch.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {
                final String rawText = mEditSearch.getText().toString();
                if (mSearch.length() == 0 || rawText.length() == 0){
                    updateUI();
                }

                mSearch = rawText;
                mHandler.postDelayed(new Runnable() {
                    @Override
                    public void run() {
                        String currentText = mEditSearch.getText().toString();
                        if (currentText.equals(rawText) && currentText.length() > 0){
                            Log.i(TAG, "Search");
                            updateUI();
                            updateData();
                        }
                        else {
                            mSearchUsers = new ArrayList<Profile>();
                            updateUI();
                        }
                    }
                }, 1000);
            }

            @Override
            public void afterTextChanged(Editable editable) {

            }
        });
        mImgSearchBtnClear.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                mEditSearch.setText("");
                mSearch = "";
            }
        });
    }

    @Override
    protected void updateUIRunnable() {
        super.updateUIRunnable();

        if (mSearch.length() == 0){
            mImgNoResult.setVisibility(View.GONE);

            mAdapter.setFactory(mFactory);
        }
        else{
            mImgNoResult.setVisibility(mSearchUsers.size() == 0 ? View.VISIBLE : View.GONE);

            mAdapter.setFactory(mSearchFactory);
        }


        mAdapter.notifyDataSetChanged();
    }


    @Override
    public void updateData() {
        super.updateData();

        MemberService service = ServiceManager.sharedInstance().getServiceHandler().create(MemberService.class);
        if (mSearch.length() == 0) {
            Call<ResponseListWrapper<Profile>> friendListCall = service.getFriendList(SessionManager.sharedInstance().getMemberId());
            friendListCall.enqueue(new ResponseHandler<ResponseListWrapper<Profile>>() {
                @Override
                public void onResponseParsed(Call<ResponseListWrapper<Profile>> call, Response<ResponseListWrapper<Profile>> response) {
                    if (response.body() != null && response.body().data != null) {
                        mMyFriends = new ArrayList<Profile>(response.body().data);

                        mFactory = new MyFriendRowViewFactory(mMyFriends, mFriendRequests, mFactoryCallback);
                        updateUI();
                    } else {
                        onShowMessage(Util.getString(R.string.msg_error), Util.getString(R.string.msg_ok), null);
                    }
                }

                @Override
                public void onFailure(Call<ResponseListWrapper<Profile>> call, Throwable t) {

                    onShowMessage(Util.getString(R.string.msg_error), Util.getString(R.string.msg_ok), null);
                }
            });

            Call<ResponseListWrapper<Profile>> friendRequestListCall = service.getFriendRequestList();
            friendRequestListCall.enqueue(new ResponseHandler<ResponseListWrapper<Profile>>() {
                @Override
                public void onResponseParsed(Call<ResponseListWrapper<Profile>> call, Response<ResponseListWrapper<Profile>> response) {
                    if (response.body() != null && response.body().data != null) {
                        mFriendRequests = new ArrayList<Profile>(response.body().data);
                        mFactory = new MyFriendRowViewFactory(mMyFriends, mFriendRequests, mFactoryCallback);
                        updateUI();
                    } else {
                        onShowMessage(Util.getString(R.string.msg_error), Util.getString(R.string.msg_ok), null);
                    }
                }

                @Override
                public void onFailure(Call<ResponseListWrapper<Profile>> call, Throwable t) {

                    onShowMessage(Util.getString(R.string.msg_error), Util.getString(R.string.msg_ok), null);
                }
            });
        }
        else{
            String search = mSearch;
            RequestBody rbSearchKey = RequestBody.create(MediaType.parse("text/plain"), search);

            mSearchCall = service.postGetSearchFriendResult(rbSearchKey);
            mSearchCall.enqueue(new ResponseHandler<ResponseListWrapper<Profile>>() {
                @Override
                public void onResponseParsed(Call<ResponseListWrapper<Profile>> call, Response<ResponseListWrapper<Profile>> response) {
                    if (response.body() != null && response.body().data != null){
                        List<Profile> users = new ArrayList<Profile>(response.body().data);
                        for (Profile user : users){
                            if (user.id == SessionManager.sharedInstance().getMemberId()){
                                users.remove(user);
                                break;
                            }
                        }
                        mSearchUsers = users;
                        mSearchFactory = new SearchFriendRowViewFactory(mSearchUsers, mSearchFactoryCallback);
                        updateUI();
                    }
                    else{
                        onShowMessage(Util.getString(R.string.msg_error), Util.getString(R.string.msg_ok), null);
                    }
                }

                @Override
                public void onFailure(Call<ResponseListWrapper<Profile>> call, Throwable t) {
                    onShowMessage(Util.getString(R.string.msg_error), Util.getString(R.string.msg_ok), null);
//                    updateUI();
                }
            });
        }

    }
}
