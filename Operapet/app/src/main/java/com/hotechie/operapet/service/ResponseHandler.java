package com.hotechie.operapet.service;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

/**
 * Created by duncan on 19/7/2016.
 */
public abstract class ResponseHandler<T> implements Callback<T> {
    @Override
    public void onResponse(Call<T> call, Response<T> response){
//        if (response.errorBody())

        onResponseParsed(call, response);
    }

    public abstract void onResponseParsed(Call<T> call, Response<T> response);

    @Override
    public abstract void onFailure(Call<T> call, Throwable t);
}
