package com.hotechie.operapet;

import android.app.Activity;
import android.app.AlarmManager;
import android.app.IntentService;
import android.app.Notification;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.BroadcastReceiver;
import android.content.ComponentName;
import android.content.Context;
import android.content.Intent;
import android.media.RingtoneManager;
import android.net.Uri;
import android.os.Bundle;
import android.os.SystemClock;
import android.support.v4.app.NotificationCompat;
import android.support.v4.content.WakefulBroadcastReceiver;
import android.util.Log;
import android.widget.Toast;

import com.google.android.gms.gcm.GcmListenerService;
import com.google.android.gms.gcm.GcmReceiver;
import com.google.android.gms.gcm.GoogleCloudMessaging;
import com.google.android.gms.iid.InstanceID;
import com.google.android.gms.iid.InstanceIDListenerService;
import com.hotechie.operapet.fragment.MenuFragment;
import com.hotechie.operapet.fragment.NotificationListFragment;
import com.hotechie.operapet.service.model.Profile;

/**
 * Created by Gary.KK on 8/9/2016.
 */
public class GcmManager {

    public String gcm_token = "";
    public String sender_ID = "22245104018"; // hotechie sender ID

    private static GcmManager mInstance = null;
    public static GcmManager sharedInstance(){
        if (mInstance == null)
            mInstance = new GcmManager();
        return mInstance;
    }

    /*
    Service for Registering GCM Token
     */
    public static class RegistrationIntentService extends IntentService {
        private static final String TAG = "RegIntentService";

        public RegistrationIntentService() {
            super(TAG);
            Log.e(TAG, "STARTED");
        }

        protected void onHandleIntent(Intent intent) {
            try {
                synchronized (TAG) {
                    InstanceID instanceID = InstanceID.getInstance(this);
                    String token = instanceID.getToken(GcmManager.sharedInstance().sender_ID, GoogleCloudMessaging.INSTANCE_ID_SCOPE, null);
                    GcmManager.sharedInstance().gcm_token = token;
                }
            }catch (Exception e) {
                Log.d(TAG, "Failed to refresh GCM token", e);
            }
        }
    }

    /*
    Service for Renewing GCM Token
    */
    public static class AppInstanceIDListenerService extends InstanceIDListenerService {
        private static final String TAG = "AppInstanceIDLS";

        @Override
        public void onTokenRefresh() {
            Intent intent = new Intent(this, RegistrationIntentService.class);
            startService(intent);
        }
    }

     /*
    Service for Receiving and Displaying GCM Message
     */
    public static class AppGcmListenerService extends GcmListenerService {
        private static final String TAG = "AppGcmListenerService";

         @Override
        public void onMessageReceived(String  from, Bundle data) {
            String message = data.getString("message");

//            Intent intent = new Intent(this, BaseTabFragmentActivity.class);
//            intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);

            Profile profile = SessionManager.sharedInstance().getProfile();
             Intent intent = null;
             if (profile != null) {
                 NotificationListFragment frag = NotificationListFragment.getInstance();
                 MenuFragment.setNextContentFragment(frag);
                 intent = new Intent(this, BaseFragmentActivity.class);
                 intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
             }
             else {
                 intent = new Intent(this, LoginActivity.class);
                 intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
             }

            PendingIntent pendingIntent = PendingIntent.getActivity(this, 0, intent, PendingIntent.FLAG_ONE_SHOT);
            Uri defaultSoundUri = RingtoneManager.getDefaultUri(RingtoneManager.TYPE_NOTIFICATION);
            NotificationCompat.Builder notificationBuilder = new NotificationCompat.Builder(this)
                    .setSmallIcon(R.mipmap.ic_launcher)
                    .setContentTitle(Util.getString(R.string.app_name))
                    .setContentText(message)
                    .setAutoCancel(true)
                    .setSound(defaultSoundUri)
                    .setContentIntent(pendingIntent)
                    .setPriority(Notification.PRIORITY_MAX)
                    .setVisibility(Notification.VISIBILITY_PUBLIC)
                    .setStyle(new NotificationCompat.BigTextStyle()
                            .bigText(message));

            NotificationManager notificationManager = (NotificationManager) getSystemService(Context.NOTIFICATION_SERVICE);
            notificationManager.notify((int)System.currentTimeMillis(), notificationBuilder.build());
        }
    }

    public static class GcmBroadcastReceiver extends GcmReceiver {

        @Override
        public void onReceive(Context context, Intent intent) {
            super.onReceive(context,intent);

            GoogleCloudMessaging gcm = GoogleCloudMessaging.getInstance(context);
            String messageType = gcm.getMessageType(intent);
            if (GoogleCloudMessaging.MESSAGE_TYPE_SEND_ERROR.equals(messageType)) {
                sendNotification(context,"Send error: " + intent.getExtras().toString());
            } else if (GoogleCloudMessaging.MESSAGE_TYPE_DELETED.equals(messageType)) {
                sendNotification(context,"Deleted messages on server: " +
                        intent.getExtras().toString());
            } else {
                sendNotification(context,"Received: " + intent.getExtras().toString());
            }
            setResultCode(Activity.RESULT_OK);
        }

        public void sendNotification(Context context, String message) {
//            Intent intent = new Intent(context, BaseTabFragmentActivity.class);
//            intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
            Profile profile = SessionManager.sharedInstance().getProfile();
            Intent intent = null;
            if (profile != null) {
                NotificationListFragment frag = NotificationListFragment.getInstance();
                MenuFragment.setNextContentFragment(frag);
                intent = new Intent(context, BaseFragmentActivity.class);
                intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
            }
            else {
                intent = new Intent(context, LoginActivity.class);
                intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
            }

            PendingIntent pendingIntent = PendingIntent.getActivity(context, 0, intent, PendingIntent.FLAG_ONE_SHOT);
            Uri defaultSoundUri = RingtoneManager.getDefaultUri(RingtoneManager.TYPE_NOTIFICATION);
            NotificationCompat.Builder notificationBuilder = new NotificationCompat.Builder(context)
                    .setSmallIcon(R.mipmap.ic_launcher)
                    .setContentTitle(Util.getString(R.string.app_name))
                    .setContentText(message)
                    .setAutoCancel(true)
                    .setSound(defaultSoundUri)
                    .setContentIntent(pendingIntent)
                    .setPriority(Notification.PRIORITY_MAX)
                    .setVisibility(Notification.VISIBILITY_PUBLIC)
                    .setStyle(new NotificationCompat.BigTextStyle()
                            .bigText(message));

            NotificationManager notificationManager = (NotificationManager) context.getSystemService(Context.NOTIFICATION_SERVICE);
            notificationManager.notify((int)System.currentTimeMillis(), notificationBuilder.build());
        }
    }
}
