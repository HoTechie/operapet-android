package com.hotechie.operapet.fragment;

import android.app.Activity;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.net.Uri;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import com.hotechie.operapet.Constant;
import com.hotechie.operapet.InnerPageFragmentActivity;
import com.hotechie.operapet.MyApplication;
import com.hotechie.operapet.R;
import com.hotechie.operapet.SessionManager;
import com.hotechie.operapet.Util;
import com.hotechie.operapet.service.AdvService;
import com.hotechie.operapet.service.GiftService;
import com.hotechie.operapet.service.ProfileService;
import com.hotechie.operapet.service.ResponseHandler;
import com.hotechie.operapet.service.ServiceManager;
import com.hotechie.operapet.service.model.Adv;
import com.hotechie.operapet.service.model.Gift;
import com.hotechie.operapet.service.model.Profile;
import com.hotechie.operapet.service.model.ResponseListWrapper;
import com.hotechie.operapet.service.model.ResponseWrapper;
import com.hotechie.operapet.ui.row_view_factory.GiftRowViewFactory;
import com.hotechie.operapet.ui.row_view_factory.PostRowViewFactory;
import com.hotechie.operapet.ui.row_view_factory.RowViewFactoryListAdapter;
import com.hotechie.util.MessageDialogFragment;
import com.squareup.picasso.Picasso;

import java.util.ArrayList;
import java.util.List;

import retrofit2.Call;
import retrofit2.Response;

/**
 * Created by duncan on 5/10/2016.
 */

public class GiftListFragment extends BaseListFragment {
    private static String TAG = "GiftListFragment";

    public static GiftListFragment getInstance(){
        GiftListFragment frag = new GiftListFragment();
        return frag;
    }

    protected View mInfoView = null;
    protected TextView mTxtCredit = null;
    protected ImageView mImgAdv = null;
    protected Adv mAdv = null;

    protected List<Gift> mGifts = new ArrayList<>();
    protected GiftRowViewFactory mFactory = null;
    protected GiftRowViewFactory.GiftRowViewFactoryCallback mFactoryCallback = null;


    @Override
    public void onResume() {
        super.onResume();
        updateData();
    }

    @Override
    public String getTitle() {
        return Util.getString(R.string.title_gift_list);
    }

    @Override
    public void configUI() {
        super.configUI();

        final Profile profile = SessionManager.sharedInstance().getProfile();

        LayoutInflater inflater = (LayoutInflater) MyApplication.getAppContext().getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        mInfoView = inflater.inflate(R.layout.view_gift_info, null, false);
        mTxtCredit = (TextView)mInfoView.findViewById(R.id.txt_credit);
        mTxtCredit.setText(String.valueOf(profile != null ? profile.remain_credit : 0));
        mImgAdv = (ImageView)mInfoView.findViewById(R.id.img_adv);

        mFactoryCallback = new GiftRowViewFactory.GiftRowViewFactoryCallback() {
            @Override
            public void onClick(Gift gift) {
                if (profile.credit_total >= gift.credit){
                    GiftDetailFragment frag = GiftDetailFragment.getInstance(gift);
                    MenuFragment.setNextContentFragment(frag);
                    Activity activity = getActivity();
                    if (activity != null && !activity.isFinishing()){
                        Intent intent = new Intent(activity, InnerPageFragmentActivity.class);
                        Util.moveToActivity(activity, intent, true);
                    }
                }
            }
        };
        mFactory = new GiftRowViewFactory(mGifts, mInfoView, (profile != null ? profile.credit_total : 0), mFactoryCallback);
        mAdapter = new RowViewFactoryListAdapter(mFactory);
        mListView.setAdapter(mAdapter);

    }

    @Override
    protected void updateUIRunnable() {
        super.updateUIRunnable();

        Profile profile = SessionManager.sharedInstance().getProfile();
        mTxtCredit.setText(String.valueOf(profile != null ? profile.remain_credit : 0));
        if (mAdv != null) {
            Picasso.with(MyApplication.getAppContext()).load(ServiceManager.sharedInstance().getImageUrl(ServiceManager.IMAGE_URL_ADV, mAdv.image_id)).into(mImgAdv);
            mImgAdv.setScaleType(ImageView.ScaleType.FIT_XY);
            mImgAdv.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    Intent i = new Intent(Intent.ACTION_VIEW);
                    i.setData(Uri.parse(mAdv.link));
                    i.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                    MyApplication.getAppContext().startActivity(i);
                }
            });
        }

        mAdapter.setFactory(mFactory);
        mAdapter.notifyDataSetChanged();
    }

    @Override
    public void updateData() {
        super.updateData();

        showLoadingScreen(true, "", Util.getString(R.string.msg_loading));

        GiftService service = ServiceManager.sharedInstance().getServiceHandler().create(GiftService.class);
        Call<ResponseListWrapper<Gift>> call = service.getGifts();
        call.enqueue(new ResponseHandler<ResponseListWrapper<Gift>>() {
            @Override
            public void onResponseParsed(Call<ResponseListWrapper<Gift>> call, Response<ResponseListWrapper<Gift>> response) {
                showLoadingScreen(false, "", Util.getString(R.string.msg_loading));
                if (response.body() != null && response.body().data != null){
                    Profile profile = SessionManager.sharedInstance().getProfile();
                    mGifts = new ArrayList<Gift>(response.body().data);
                    mFactory = new GiftRowViewFactory(mGifts, mInfoView, (profile != null ? profile.credit_total : 0), mFactoryCallback);
                    updateUI();
                }
            }

            @Override
            public void onFailure(Call<ResponseListWrapper<Gift>> call, Throwable t) {
                showLoadingScreen(false, "", Util.getString(R.string.msg_loading));
                onHandleError(Util.getString(R.string.msg_error), null, null, new MessageDialogFragment.MessageDialogFragmentCallback() {

                    @Override
                    public void onClickPositiveButton(MessageDialogFragment fragment, DialogInterface dialog, int id) {
                        updateData();
                    }

                    @Override
                    public void onClickNegativeButton(MessageDialogFragment fragment, DialogInterface dialog, int id) {

                    }
                });
            }
        });


        AdvService serviceAdv = ServiceManager.sharedInstance().getServiceHandler().create(AdvService.class);
        Call<ResponseListWrapper<Adv>> callAdv = serviceAdv.getAdvListByType(Constant.ADV_TYPE_GIFT);
        callAdv.enqueue(new ResponseHandler<ResponseListWrapper<Adv>>() {
            @Override
            public void onResponseParsed(Call<ResponseListWrapper<Adv>> call, Response<ResponseListWrapper<Adv>> response) {
                if (response.body() != null && response.body().data != null && response.body().data.size() > 0){
//                    Profile profile = SessionManager.sharedInstance().getProfile();
                    mAdv = response.body().data.get(0);
//                    mFactory = new GiftRowViewFactory(mGifts, mInfoView, (profile != null ? profile.credit_total : 0), mFactoryCallback);
                    updateUI();

//                    Picasso.with(MyApplication.getAppContext()).load(ServiceManager.sharedInstance().getImageUrl(ServiceManager.IMAGE_URL_ADV, adv.image_id)).into(imgAdv);
//                    imgAdv.setOnClickListener(new View.OnClickListener() {
//                        @Override
//                        public void onClick(View view) {
//                            Intent i = new Intent(Intent.ACTION_VIEW);
//                            i.setData(Uri.parse(adv.link));
//                            startActivity(i);
//                        }
//                    });
                }
            }

            @Override
            public void onFailure(Call<ResponseListWrapper<Adv>> call, Throwable t) {

            }
        });


        ProfileService serviceP = ServiceManager.sharedInstance().getServiceHandler().create(ProfileService.class);
        Call<ResponseWrapper<Profile>> callP = serviceP.getProfile();
        callP.enqueue(new ResponseHandler<ResponseWrapper<Profile>>() {

            @Override
            public void onResponseParsed(Call<ResponseWrapper<Profile>> call, Response<ResponseWrapper<Profile>> response) {

                showLoadingScreen(false, "", Util.getString(R.string.msg_loading));

                if (response.body() != null && response.body().data != null) {
                    SessionManager.sharedInstance().setProfile(response.body().data);
                    Profile profile = SessionManager.sharedInstance().getProfile();
                    mFactory = new GiftRowViewFactory(mGifts, mInfoView, (profile != null ? profile.credit_total : 0), mFactoryCallback);
                    updateUI();
                }
            }

            @Override
            public void onFailure(Call<ResponseWrapper<Profile>> call, Throwable t) {
                showLoadingScreen(false, "", Util.getString(R.string.msg_loading));
                onHandleError(Util.getString(R.string.msg_error), null, null, new MessageDialogFragment.MessageDialogFragmentCallback() {

                    @Override
                    public void onClickPositiveButton(MessageDialogFragment fragment, DialogInterface dialog, int id) {
                        updateData();
                    }

                    @Override
                    public void onClickNegativeButton(MessageDialogFragment fragment, DialogInterface dialog, int id) {

                    }
                });
            }
        });
    }
}
