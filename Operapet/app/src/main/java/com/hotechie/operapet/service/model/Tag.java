package com.hotechie.operapet.service.model;

/**
 * Created by duncan on 17/8/2016.
 */
public class Tag extends BaseModel {
    public int id = 0;
    public String description;

    public int image_id = 0; // profile image id when calling GetSearchTagResultApi

    public int post_tag_id = 0;
}
