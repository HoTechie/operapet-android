package com.hotechie.operapet.ui.row_view_factory;

import android.content.Context;
import android.graphics.drawable.Drawable;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.TextView;

import com.hotechie.operapet.MyApplication;
import com.hotechie.operapet.R;
import com.hotechie.operapet.service.model.BaseModel;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

/**
 * Created by duncan on 10/7/2016.
 */
public class BaseModelRowViewFactory extends RowViewFactory {
    public static interface BaseModelRowViewFactoryCallback{
        public void onClick(BaseModel model);
    }

    protected List<BaseModel> mModels = new ArrayList<>();
    protected BaseModelRowViewFactoryCallback mCallback = null;
    protected List<String> mRoomSwitchOns = new ArrayList<>();

    public BaseModelRowViewFactory(List<BaseModel> models, final BaseModelRowViewFactoryCallback callback){
        mModels = new ArrayList<>(models);
        mCallback = callback;
    }

    @Override
    public int getCount() {
        return mModels.size();
    }

    @Override
    public Object getItem(int index) {
        return mModels.get(index);
    }

    @Override
    public View getView(Object object) {
//        LayoutInflater inflater = (LayoutInflater) MyApplication.getAppContext().getSystemService(Context.LAYOUT_INFLATER_SERVICE);
//        View rowView = inflater.inflate(R.layout.row_room, null, false);
//
//        final BaseModel obj = (BaseModel)object;
//        ((TextView)rowView.findViewById(R.id.txt_title)).setText(obj.name);
//        Drawable icon = IconManager.sharedInstance().roomIconWithID(MyApplication.getAppContext(), Integer.parseInt(obj.imgId, 10), false);
//        ((ImageView) rowView.findViewById(R.id.img_icon)).setImageDrawable(icon);
//
//
//        if (mFactoryCallback != null) {
//            rowView.setOnClickListener(new View.OnClickListener() {
//                @Override
//                public void onClick(View view) {
//                    mFactoryCallback.onClick(obj);
//                }
//            });
//        }
//
//        return rowView;



        return null;
    }
}
