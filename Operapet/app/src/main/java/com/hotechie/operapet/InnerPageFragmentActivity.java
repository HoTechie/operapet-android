package com.hotechie.operapet;

import android.content.DialogInterface;
import android.graphics.drawable.Drawable;
import android.os.Looper;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.TextView;

import com.hotechie.util.MessageDialogFragment;

/**
 * Created by duncan on 30/9/2016.
 */

public class InnerPageFragmentActivity extends BaseFragmentActivity {
    @Override
    public void configUI() {
        super.configUI();


        setupActionBar(mContentFragment.getTitle(), null, Util.getDrawable(this, R.drawable.btn_back), null, null);

    }

    @Override
    public boolean onClickActionBarItem(View view) {
        boolean isHandled = false;

        if (mContentFragment != null)
            isHandled = mContentFragment.onClickActionBarItem(view);

        if (!isHandled) {
            int id = view.getId();
            switch (id) {
                case R.id.btn_left:
                    onBackPressed();
                    isHandled = true;

                    break;
            }
        }

        return isHandled;

    }

    @Override
    public void onBackPressed() {
        Util.backToActivity(InnerPageFragmentActivity.this, true);

    }


    public void setupActionBar(final String titleText, final Drawable titleIcon, final Drawable leftIcon, final Drawable rightIcon, final Drawable rightIcon2){
        runOnUiThread(new Runnable() {
            @Override
            public void run() {
                if (mLayoutActionBar != null) {
                    LayoutInflater inflator = LayoutInflater.from(InnerPageFragmentActivity.this);
                    View v = inflator.inflate(R.layout.action_bar_content_view, null);
                    ViewGroup.LayoutParams layoutParams = new ViewGroup.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.MATCH_PARENT);
                    mLayoutActionBar.addView(v, layoutParams);
                    TextView txtTitle = ((TextView) v.findViewById(R.id.title));
                    if (txtTitle != null)
                        txtTitle.setText(titleText);
                    ((ImageView) v.findViewById(R.id.icon)).setImageDrawable(titleIcon);
                    ImageButton btnLeft = ((ImageButton) v.findViewById(R.id.btn_left));
                    btnLeft.setColorFilter(Util.getColor(MyApplication.getAppContext(), R.color.action_bar_back_btn));
                    if (leftIcon == null)
                        btnLeft.setVisibility(View.GONE);
                    else {
                        btnLeft.setImageDrawable(leftIcon);
                        btnLeft.setVisibility(View.VISIBLE);
                    }
                    ImageButton btnRight = ((ImageButton) v.findViewById(R.id.btn_right));
                    if (rightIcon == null)
                        btnRight.setVisibility(View.INVISIBLE);
                    else {
                        btnRight.setImageDrawable(rightIcon);
                        btnRight.setVisibility(View.VISIBLE);
                    }

                    ImageButton btnRight2 = ((ImageButton) v.findViewById(R.id.btn_right_2));
                    if (rightIcon2 == null)
                        btnRight2.setVisibility(View.GONE);
                    else {
                        btnRight2.setImageDrawable(rightIcon2);
                        btnRight2.setVisibility(View.VISIBLE);
                    }

                    View.OnClickListener onClickListener = new View.OnClickListener() {
                        @Override
                        public void onClick(View view) {
                            onClickActionBarItem(view);
                        }
                    };
                    btnLeft.setOnClickListener(onClickListener);
                    btnRight.setOnClickListener(onClickListener);
                    btnRight2.setOnClickListener(onClickListener);

                }
            }
        });

    }
}
