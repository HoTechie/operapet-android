package com.hotechie.operapet.fragment;

import android.app.Activity;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.util.TypedValue;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.hotechie.operapet.Constant;
import com.hotechie.operapet.InnerPageFragmentActivity;
import com.hotechie.operapet.MyApplication;
import com.hotechie.operapet.R;
import com.hotechie.operapet.SessionManager;
import com.hotechie.operapet.Util;
import com.hotechie.operapet.service.MemberService;
import com.hotechie.operapet.service.PostsService;
import com.hotechie.operapet.service.ProfileService;
import com.hotechie.operapet.service.ResponseHandler;
import com.hotechie.operapet.service.ServiceManager;
import com.hotechie.operapet.service.model.BaseModel;
import com.hotechie.operapet.service.model.Post;
import com.hotechie.operapet.service.model.Profile;
import com.hotechie.operapet.service.model.ResponseListWrapper;
import com.hotechie.operapet.service.model.ResponseWrapper;
import com.hotechie.operapet.service.model.Tag;
import com.hotechie.operapet.ui.row_view_factory.PostRowViewFactory.PostRowViewFactoryCallback;
import com.hotechie.operapet.ui.row_view_factory.RowViewFactoryListAdapter;
import com.hotechie.operapet.ui.row_view_factory.WallPostRowViewFactory;
import com.hotechie.util.MessageDialogFragment;
import com.squareup.picasso.Picasso;

import java.util.ArrayList;
import java.util.List;

import okhttp3.MediaType;
import okhttp3.RequestBody;
import retrofit2.Call;
import retrofit2.Response;

/**
 * Created by duncan on 30/9/2016.
 */

public class WallFragment extends BaseListFragment {
    private static String TAG = "WallFragment";

    public static WallFragment getInstance(int memberId){
        WallFragment frag = new WallFragment();
        frag.mMemberId = memberId;
        return frag;
    }

    protected int mMemberId = 0;
    protected List<Post> mPosts = new ArrayList<>();
    protected Profile mProfile = null;

    protected WallPostRowViewFactory mFactory = null;
    protected PostRowViewFactoryCallback mFactoryCallback = null;

    protected View mProfileView = null;
    protected TextView mTxtName, mTxtShareCount, mTxtPostCount;
    protected ImageView mImgProfile = null;

    protected View mLayoutPet, mLayoutFriend, mLayoutAddFriend;
    protected ImageView mImgAddFriend;
    protected TextView mTxtAddFriend;

    @Override
    public void onStart() {
        super.onStart();
        updateData();
    }

    @Override
    public void onResume() {
        super.onResume();
        updateUI();
    }

    @Override
    public String getTitle() {
        int myMemberId = SessionManager.sharedInstance().getMemberId();
        if (myMemberId > 0 && myMemberId == mMemberId){
            return Util.getString(R.string.title_my_wall);
        }
        return "";
    }


    @Override
    public void configUI() {
        super.configUI();

        LayoutInflater inflater = (LayoutInflater) MyApplication.getAppContext().getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        mProfileView = inflater.inflate(R.layout.view_wall_profile, null, false);
        mTxtName = (TextView)mProfileView.findViewById(R.id.txt_name);
        mTxtShareCount = (TextView)mProfileView.findViewById(R.id.txt_share_count);
        mTxtPostCount = (TextView)mProfileView.findViewById(R.id.txt_post_count);
        mImgProfile = (ImageView)mProfileView.findViewById(R.id.img_profile);
        mLayoutAddFriend = mProfileView.findViewById(R.id.layout_add_friend);
        mLayoutFriend = mProfileView.findViewById(R.id.layout_friend);
        mLayoutPet = mProfileView.findViewById(R.id.layout_pet);
        mImgAddFriend = (ImageView)mProfileView.findViewById(R.id.img_add_friend);
        mTxtAddFriend = (TextView)mProfileView.findViewById(R.id.txt_add_friend);

        if (mMemberId == SessionManager.sharedInstance().getMemberId()){
            mProfileView.findViewById(R.id.layout_add_friend_container).setVisibility(View.GONE);
            ((LinearLayout)mProfileView.findViewById(R.id.layout_user_action)).setWeightSum(2);
        }
        else{
            mProfileView.findViewById(R.id.layout_add_friend_container).setVisibility(View.VISIBLE);
            ((LinearLayout)mProfileView.findViewById(R.id.layout_user_action)).setWeightSum(3);
        }

        mLayoutPet.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                // TODO move to pet list page
                PetListFragment frag = PetListFragment.getInstance(mMemberId, mMemberId == SessionManager.sharedInstance().getMemberId());
                MenuFragment.setNextContentFragment(frag);
                Activity activity = getActivity();
                if (activity != null && !activity.isFinishing()) {
                    Intent newIntent = new Intent(activity, InnerPageFragmentActivity.class);
                    Util.moveToActivity(activity, newIntent, true);
                }
            }
        });

        mLayoutAddFriend.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (mProfile.friend_relation != null){
                    int status = mProfile.friend_relation.status;
                    if (status == Constant.FRIENDSHIP_STATUS_NO_FRIEND ||
                            (status == Constant.FRIENDSHIP_STATUS_PENDING && mProfile.friend_relation.destination_id == mMemberId)){
                        RequestBody rbTargetMemberId = RequestBody.create(MediaType.parse("text/plain"), String.valueOf(mMemberId));

                        MemberService service = ServiceManager.sharedInstance().getServiceHandler().create(MemberService.class);
                        Call<ResponseWrapper<BaseModel>> call = service.postRequestFriend(rbTargetMemberId);
                        call.enqueue(new ResponseHandler<ResponseWrapper<BaseModel>>() {
                            @Override
                            public void onResponseParsed(Call<ResponseWrapper<BaseModel>> call, Response<ResponseWrapper<BaseModel>> response) {
                                updateData();
                            }

                            @Override
                            public void onFailure(Call<ResponseWrapper<BaseModel>> call, Throwable t) {
                                onShowMessage(Util.getString(R.string.msg_error), Util.getString(R.string.msg_ok), null);
                                updateData();
                            }
                        });
                    }
                    else if (status == Constant.FRIENDSHIP_STATUS_PENDING && mProfile.friend_relation.destination_id == SessionManager.sharedInstance().getMemberId()){

                        onShowMessage(Util.getString(R.string.wall_confirm_friend_q), Util.getString(R.string.wall_confirm_friend_q_yes), Util.getString(R.string.wall_confirm_friend_q_no), new MessageDialogFragment.MessageDialogFragmentCallback() {
                            @Override
                            public void onClickPositiveButton(MessageDialogFragment fragment, DialogInterface dialog, int id) {
                                RequestBody rbTargetMemberId = RequestBody.create(MediaType.parse("text/plain"), String.valueOf(mMemberId));

                                MemberService service = ServiceManager.sharedInstance().getServiceHandler().create(MemberService.class);
                                Call<ResponseWrapper<BaseModel>> call = service.postAcceptFriend(rbTargetMemberId);
                                call.enqueue(new ResponseHandler<ResponseWrapper<BaseModel>>() {
                                    @Override
                                    public void onResponseParsed(Call<ResponseWrapper<BaseModel>> call, Response<ResponseWrapper<BaseModel>> response) {
                                        updateData();
                                    }

                                    @Override
                                    public void onFailure(Call<ResponseWrapper<BaseModel>> call, Throwable t) {
                                        onShowMessage(Util.getString(R.string.msg_error), Util.getString(R.string.msg_ok), null);
                                        updateData();
                                    }
                                });
                            }

                            @Override
                            public void onClickNegativeButton(MessageDialogFragment fragment, DialogInterface dialog, int id) {
                                RequestBody rbTargetMemberId = RequestBody.create(MediaType.parse("text/plain"), String.valueOf(mProfile.friend_relation.friend_relationship_id));

                                MemberService service = ServiceManager.sharedInstance().getServiceHandler().create(MemberService.class);
                                Call<ResponseWrapper<BaseModel>> call = service.postDeleteFriend(rbTargetMemberId);
                                call.enqueue(new ResponseHandler<ResponseWrapper<BaseModel>>() {
                                    @Override
                                    public void onResponseParsed(Call<ResponseWrapper<BaseModel>> call, Response<ResponseWrapper<BaseModel>> response) {
                                        updateData();
                                    }

                                    @Override
                                    public void onFailure(Call<ResponseWrapper<BaseModel>> call, Throwable t) {
                                        onShowMessage(Util.getString(R.string.msg_error), Util.getString(R.string.msg_ok), null);
                                        updateData();
                                    }
                                });
                            }
                        });
                    }
                    else if (status == Constant.FRIENDSHIP_STATUS_CONFIRMED){

                        onShowMessage(Util.getString(R.string.wall_unfriend_q), Util.getString(R.string.wall_unfriend_q_yes), Util.getString(R.string.msg_cancel), new MessageDialogFragment.MessageDialogFragmentCallback() {
                            @Override
                            public void onClickPositiveButton(MessageDialogFragment fragment, DialogInterface dialog, int id) {
                                RequestBody rbTargetMemberId = RequestBody.create(MediaType.parse("text/plain"), String.valueOf(mProfile.friend_relation.friend_relationship_id));

                                MemberService service = ServiceManager.sharedInstance().getServiceHandler().create(MemberService.class);
                                Call<ResponseWrapper<BaseModel>> call = service.postDeleteFriend(rbTargetMemberId);
                                call.enqueue(new ResponseHandler<ResponseWrapper<BaseModel>>() {
                                    @Override
                                    public void onResponseParsed(Call<ResponseWrapper<BaseModel>> call, Response<ResponseWrapper<BaseModel>> response) {
                                        updateData();
                                    }

                                    @Override
                                    public void onFailure(Call<ResponseWrapper<BaseModel>> call, Throwable t) {
                                        onShowMessage(Util.getString(R.string.msg_error), Util.getString(R.string.msg_ok), null);
                                        updateData();
                                    }
                                });
                            }

                            @Override
                            public void onClickNegativeButton(MessageDialogFragment fragment, DialogInterface dialog, int id) {

                            }
                        });
                    }
                }
            }
        });

        mLayoutFriend.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                WallFriendListFragment frag = WallFriendListFragment.getInstance(mMemberId);
                MenuFragment.setNextContentFragment(frag);
                Activity activity = getActivity();
                if (activity != null && !activity.isFinishing()) {
                    Intent newIntent = new Intent(activity, InnerPageFragmentActivity.class);
                    Util.moveToActivity(activity, newIntent, true);
                }
            }
        });



        mFactoryCallback = new PostRowViewFactoryCallback(){

            @Override
            public void onClick(Post post) {
                PostDetailFragment frag = PostDetailFragment.getInstance(post);
                MenuFragment.setNextContentFragment(frag);
                Intent newIntent = new Intent(getActivity(), InnerPageFragmentActivity.class);
                Util.moveToActivity(getActivity(), newIntent, true);
            }

            @Override
            public void onClickLike(Post post) {

                RequestBody rbPostId = RequestBody.create(MediaType.parse("text/plain"), String.valueOf(post.id));

                PostsService service = ServiceManager.sharedInstance().getServiceHandler().create(PostsService.class);
                Call<ResponseWrapper<BaseModel>> call = service.postCreateLike(rbPostId);
                call.enqueue(new ResponseHandler<ResponseWrapper<BaseModel>>() {
                    @Override
                    public void onResponseParsed(Call<ResponseWrapper<BaseModel>> call, Response<ResponseWrapper<BaseModel>> response) {
                        updateData();
                    }

                    @Override
                    public void onFailure(Call<ResponseWrapper<BaseModel>> call, Throwable t) {
                        updateData();
                    }
                });
            }

            @Override
            public void onClickShare(Post post) {

                RequestBody rbPostId = RequestBody.create(MediaType.parse("text/plain"), String.valueOf(post.id));

                PostsService service = ServiceManager.sharedInstance().getServiceHandler().create(PostsService.class);
                Call<ResponseWrapper<BaseModel>> call = service.postCreateShare(rbPostId);
                call.enqueue(new ResponseHandler<ResponseWrapper<BaseModel>>() {
                    @Override
                    public void onResponseParsed(Call<ResponseWrapper<BaseModel>> call, Response<ResponseWrapper<BaseModel>> response) {
                        onShowMessage(Util.getString(R.string.post_share_success), Util.getString(R.string.msg_ok), null);

                        updateData();
                    }

                    @Override
                    public void onFailure(Call<ResponseWrapper<BaseModel>> call, Throwable t) {
                        updateData();
                    }
                });
            }

            @Override
            public void onClickTag(Tag tag) {
                PostByTagListFragment frag = PostByTagListFragment.getInstance(tag);
                MenuFragment.setNextContentFragment(frag);
                Activity activity = getActivity();
                if (activity != null && !activity.isFinishing()) {
                    Intent intent = new Intent(activity, InnerPageFragmentActivity.class);
                    Util.moveToActivity(activity, intent, true);
                }
            }

            @Override
            public void onClickProfile(Profile profile) {
                if (profile.id == mMemberId)
                    return;

                WallFragment frag = WallFragment.getInstance(profile.id);
                MenuFragment.setNextContentFragment(frag);
                Activity activity = getActivity();
                if (activity != null && !activity.isFinishing()) {
                    Intent intent = new Intent(activity, InnerPageFragmentActivity.class);
                    Util.moveToActivity(activity, intent, true);
                }
            }
        };
        mFactory = new WallPostRowViewFactory(mPosts, mFactoryCallback, mProfileView);
        mAdapter = new RowViewFactoryListAdapter(mFactory);
        mListView.setAdapter(mAdapter);
        int listPaddingTop = (int)TypedValue.applyDimension(TypedValue.COMPLEX_UNIT_DIP, 20, getResources().getDisplayMetrics());
        mListView.setPadding(0, listPaddingTop, 0, 0);

    }

    @Override
    protected void updateUIRunnable() {
        super.updateUIRunnable();

        if (mProfile != null){
            mTxtName.setText(mProfile.user_name);
            mTxtPostCount.setText(String.valueOf(mProfile.no_of_post));
            mTxtShareCount.setText(String.valueOf(mProfile.no_of_share));
            Picasso.with(MyApplication.getAppContext()).load(ServiceManager.sharedInstance().getImageUrl(ServiceManager.IMAGE_URL_PROFILE, mProfile.image_id)).into(mImgProfile);

            if (mProfile.friend_relation != null){
                if (mProfile.friend_relation.status == Constant.FRIENDSHIP_STATUS_PENDING){
                    mImgAddFriend.setColorFilter(Util.getColor(MyApplication.getAppContext(), R.color.theme_color_darker));
                    mImgAddFriend.setImageResource(R.drawable.icon_tab_friend);
                    if (mProfile.friend_relation.source_id == SessionManager.sharedInstance().getMemberId())
                        mTxtAddFriend.setText(Util.getString(R.string.wall_add_friend_pending));
                    else
                        mTxtAddFriend.setText(Util.getString(R.string.wall_add_friend_pending_my_response));
                }
                else if (mProfile.friend_relation.status == Constant.FRIENDSHIP_STATUS_CONFIRMED){
                    mImgAddFriend.setColorFilter(null);
                    mImgAddFriend.setImageResource(R.drawable.icon_is_friend);
                    mTxtAddFriend.setText(Util.getString(R.string.wall_add_friend_confirmed));
                }
            }
        }
        else{
            mTxtName.setText("");
            mTxtPostCount.setText(String.valueOf(0));
            mTxtShareCount.setText(String.valueOf(0));
        }

        mAdapter.notifyDataSetChanged();
    }

    @Override
    public void updateData() {
        super.updateData();

        updateUI();
        showLoadingScreen(true, "", Util.getString(R.string.msg_loading));
        ProfileService service = ServiceManager.sharedInstance().getServiceHandler().create(ProfileService.class);
        Call<ResponseWrapper<Profile>> call = service.getProfile();
        if (mMemberId == SessionManager.sharedInstance().getMemberId()){
            call = service.getProfile();
        }
        else{
            call = service.getProfile(mMemberId);
        }
        call.enqueue(new ResponseHandler<ResponseWrapper<Profile>>() {

            @Override
            public void onResponseParsed(Call<ResponseWrapper<Profile>> call, Response<ResponseWrapper<Profile>> response) {

                showLoadingScreen(false, "", Util.getString(R.string.msg_loading));

                if (response.body() != null && response.body().data != null) {
                    mProfile = response.body().data;
                    updateUI();
                }
            }

            @Override
            public void onFailure(Call<ResponseWrapper<Profile>> call, Throwable t) {
                showLoadingScreen(false, "", Util.getString(R.string.msg_loading));
                onHandleError(Util.getString(R.string.msg_error), null, null, new MessageDialogFragment.MessageDialogFragmentCallback() {

                    @Override
                    public void onClickPositiveButton(MessageDialogFragment fragment, DialogInterface dialog, int id) {
                        updateData();
                    }

                    @Override
                    public void onClickNegativeButton(MessageDialogFragment fragment, DialogInterface dialog, int id) {

                    }
                });
            }
        });


        PostsService postService = ServiceManager.sharedInstance().getServiceHandler().create(PostsService.class);
        Call<ResponseListWrapper<Post>> postCall = postService.getPostsByMember(mMemberId);
        postCall.enqueue(new ResponseHandler<ResponseListWrapper<Post>>() {

            @Override
            public void onResponseParsed(Call<ResponseListWrapper<Post>> call, Response<ResponseListWrapper<Post>> response) {
                // FIXME pagination
                if (response.body() != null && response.body().data != null) {
                    mPosts = new ArrayList<Post>(response.body().data);
                    mFactory = new WallPostRowViewFactory(mPosts, mFactoryCallback, mProfileView);
                    mAdapter.setFactory(mFactory);
                    updateUI();
                }
            }

            @Override
            public void onFailure(Call<ResponseListWrapper<Post>> call, Throwable t) {
//                onHandleError(Util.getString(R.string.msg_error), null, null, new MessageDialogFragment.MessageDialogFragmentCallback() {
//
//                    @Override
//                    public void onClickPositiveButton(MessageDialogFragment fragment, DialogInterface dialog, int id) {
//                        updateData();
//                    }
//
//                    @Override
//                    public void onClickNegativeButton(MessageDialogFragment fragment, DialogInterface dialog, int id) {
//
//                    }
//                });
            }
        });
    }
}
