package com.hotechie.operapet.ui.row_view_factory;

import android.content.Context;
import android.text.Html;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import com.hotechie.operapet.MyApplication;
import com.hotechie.operapet.R;
import com.hotechie.operapet.Util;
import com.hotechie.operapet.service.ServiceManager;
import com.hotechie.operapet.service.model.Notification;
import com.squareup.picasso.Picasso;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

/**
 * Created by duncan on 13/4/2017.
 */

public class NotificationRowViewFactory extends RowViewFactory {
    public static interface NotificationRowViewFactoryCallback {
        public void onClick(Notification notification);
    }

    protected List<Notification> mNotifications= new ArrayList<>();
    protected NotificationRowViewFactoryCallback mCallback = null;

    protected SimpleDateFormat originalTimeFormat;
    protected SimpleDateFormat targetTimeFormat;


    public NotificationRowViewFactory(List<Notification> notifications, final NotificationRowViewFactoryCallback callback){
        mNotifications = new ArrayList<>(notifications);
        mCallback = callback;

        originalTimeFormat = new SimpleDateFormat("yyyy-MM-dd HH:mm");
        targetTimeFormat = new SimpleDateFormat(Util.getString(R.string.post_datetime_format));
    }

    @Override
    public int getCount() {
        return mNotifications.size();
    }

    @Override
    public Object getItem(int index) {
        return mNotifications.get(index);
    }

    @Override
    public View getView(final Object object) {
        final Notification notification = (Notification)object;

        LayoutInflater inflater = (LayoutInflater) MyApplication.getAppContext().getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        View rowView = inflater.inflate(R.layout.row_notification, null, false);

        ImageView imgProfile = (ImageView)rowView.findViewById(R.id.img_profile);
        TextView txtDescription = (TextView)rowView.findViewById(R.id.txt_description);
        TextView txtDate = (TextView)rowView.findViewById(R.id.txt_date);

        txtDescription.setText(Html.fromHtml(notification.description));

        if (notification.image_id > 0) {
            Picasso.with(MyApplication.getAppContext()).load(ServiceManager.sharedInstance().getImageUrl(ServiceManager.IMAGE_URL_PROFILE, notification.image_id)).into(imgProfile);
        }
//        if (notification. != null && gift.images.size() > 0){
//            Picasso.with(MyApplication.getAppContext()).load(ServiceManager.sharedInstance().getImageUrl(ServiceManager.IMAGE_URL_GIFT, gift.images.get(0))).into(img);
//        }
//        else{
//            img.setImageDrawable(null);
//        }

        String createTimeStr = "";
        try {
            Date createTimeDate = originalTimeFormat.parse(notification.crt_date);
            createTimeStr = targetTimeFormat.format(createTimeDate);
        } catch (ParseException e) {
            e.printStackTrace();
        }
        txtDate.setText(createTimeStr);

        if (mCallback != null){
            rowView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    mCallback.onClick(notification);
                }
            });
        }

        return rowView;
    }

    @Override
    public boolean modifyViewLayout(View contentView, Object object, int index) {
        return false;
    }
}
