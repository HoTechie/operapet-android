package com.hotechie.operapet;

import android.graphics.drawable.Drawable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.TextView;

/**
 * Created by duncan on 10/10/2016.
 */

public class InnerPageEditFragmentActivity extends InnerPageFragmentActivity {


    @Override
    public void setupActionBar(final String titleText, final Drawable titleIcon, final Drawable leftIcon, final Drawable rightIcon, final Drawable rightIcon2){
        runOnUiThread(new Runnable() {
            @Override
            public void run() {
                if (mLayoutActionBar != null) {
                    LayoutInflater inflator = LayoutInflater.from(InnerPageEditFragmentActivity.this);
                    View v = inflator.inflate(R.layout.action_bar_content_view, null);
                    ViewGroup.LayoutParams layoutParams = new ViewGroup.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.MATCH_PARENT);
                    mLayoutActionBar.addView(v, layoutParams);
                    TextView txtTitle = ((TextView) v.findViewById(R.id.title));
                    if (txtTitle != null)
                        txtTitle.setText(titleText);
                    ((ImageView) v.findViewById(R.id.icon)).setImageDrawable(titleIcon);
                    ImageButton btnLeft = ((ImageButton) v.findViewById(R.id.btn_left));
                    btnLeft.setColorFilter(Util.getColor(MyApplication.getAppContext(), R.color.action_bar_back_btn));
                    if (leftIcon == null)
                        btnLeft.setVisibility(View.GONE);
                    else {
                        btnLeft.setImageDrawable(leftIcon);
                        btnLeft.setVisibility(View.VISIBLE);
                    }
//                    ImageButton btnRight = ((ImageButton) v.findViewById(R.id.btn_right));
//                    if (rightIcon == null)
//                        btnRight.setVisibility(View.GONE);
//                    else {
//                        btnRight.setImageDrawable(rightIcon);
//                        btnRight.setVisibility(View.VISIBLE);
//                    }
//
//                    ImageButton btnRight2 = ((ImageButton) v.findViewById(R.id.btn_right_2));
//                    if (rightIcon2 == null)
//                        btnRight2.setVisibility(View.GONE);
//                    else {
//                        btnRight2.setImageDrawable(rightIcon2);
//                        btnRight2.setVisibility(View.VISIBLE);
//                    }

                    TextView txtRight = ((TextView)v.findViewById(R.id.txt_right));
                    txtRight.setText(Util.getString(R.string.msg_save));
                    txtRight.setVisibility(View.VISIBLE);

                    View.OnClickListener onClickListener = new View.OnClickListener() {
                        @Override
                        public void onClick(View view) {
                            onClickActionBarItem(view);
                        }
                    };
                    btnLeft.setOnClickListener(onClickListener);
//                    btnRight.setOnClickListener(onClickListener);
//                    btnRight2.setOnClickListener(onClickListener);
                    txtRight.setOnClickListener(onClickListener);

                }
            }
        });

    }
}
