package com.hotechie.operapet.fragment;

import android.app.Activity;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.hotechie.operapet.Constant;
import com.hotechie.operapet.GcmManager;
import com.hotechie.operapet.InnerPageFragmentActivity;
import com.hotechie.operapet.MyApplication;
import com.hotechie.operapet.R;
import com.hotechie.operapet.Util;
import com.hotechie.operapet.service.AdvService;
import com.hotechie.operapet.service.MemberService;
import com.hotechie.operapet.service.PostsService;
import com.hotechie.operapet.service.ResponseHandler;
import com.hotechie.operapet.service.ServiceManager;
import com.hotechie.operapet.service.model.Adv;
import com.hotechie.operapet.service.model.BaseModel;
import com.hotechie.operapet.service.model.Post;
import com.hotechie.operapet.service.model.Profile;
import com.hotechie.operapet.service.model.ResponseListWrapper;
import com.hotechie.operapet.service.model.ResponseWrapper;
import com.hotechie.operapet.service.model.Tag;
import com.hotechie.operapet.ui.row_view_factory.PostRowViewFactory;
import com.hotechie.operapet.ui.row_view_factory.RowViewFactoryListAdapter;
import com.hotechie.util.MessageDialogFragment;
import com.squareup.picasso.Picasso;

import java.util.ArrayList;
import java.util.List;

import okhttp3.MediaType;
import okhttp3.RequestBody;
import retrofit2.Call;
import retrofit2.Response;

/**
 * Created by duncan on 17/8/2016.
 */
public class PostListFragment extends BaseListFragment {
    private static String TAG = "PostsListFragment";

    public static PostListFragment getInstance(Bundle b){
        PostListFragment frag = new PostListFragment();
        frag.setArguments(b);
        return frag;
    }

    protected int mCategoryId = 0; // all

    protected PostRowViewFactory mFactory = null;
    protected PostRowViewFactory.PostRowViewFactoryCallback mFactoryCallback = null;

    protected List<Post> mPosts = new ArrayList<>();
    protected Adv mAdv = null;


    protected LinearLayout mLayoutCategory = null;

    @Override
    public void onResume() {
        super.onResume();

        updateData();
    }

    @Override
    public void onStart() {
        super.onStart();

//        final String token = GcmManager.sharedInstance().gcm_token;
//        RequestBody rbDeviceId = RequestBody.create(MediaType.parse("text/plain"), token);
//        RequestBody rbDeviceType = RequestBody.create(MediaType.parse("text/plain"), "2");
//        MemberService service = ServiceManager.sharedInstance().getServiceHandler().create(MemberService.class);
//        Call<ResponseWrapper<Object>> call = service.postUpdateDeviceTokenApi(rbDeviceId, rbDeviceType);
//        call.enqueue(new ResponseHandler<ResponseWrapper<Object>>() {
//            @Override
//            public void onResponseParsed(Call<ResponseWrapper<Object>> call, Response<ResponseWrapper<Object>> response) {
//                Log.i(TAG, "UpdateDeviceTokenApi: " + token);
//            }
//
//            @Override
//            public void onFailure(Call<ResponseWrapper<Object>> call, Throwable t) {
//
//            }
//        });
    }

    @Override
    public int layoutRes() {
        return R.layout.fragment_post_list;
    }

    @Override
    public String getTitle(){
        return MyApplication.getAppContext().getString(R.string.title_posts);
    }

    @Override
    public void configUI(){
        super.configUI();

        mFactoryCallback = new PostRowViewFactory.PostRowViewFactoryCallback() {
            @Override
            public void onClick(Post post) {
                PostDetailFragment frag = PostDetailFragment.getInstance(post);
                MenuFragment.setNextContentFragment(frag);
                Intent newIntent = new Intent(getActivity(), InnerPageFragmentActivity.class);
                Util.moveToActivity(getActivity(), newIntent, true);
            }

            @Override
            public void onClickLike(Post post) {

                RequestBody rbPostId = RequestBody.create(MediaType.parse("text/plain"), String.valueOf(post.id));

                PostsService service = ServiceManager.sharedInstance().getServiceHandler().create(PostsService.class);
                Call<ResponseWrapper<BaseModel>> call = service.postCreateLike(rbPostId);
                call.enqueue(new ResponseHandler<ResponseWrapper<BaseModel>>() {
                    @Override
                    public void onResponseParsed(Call<ResponseWrapper<BaseModel>> call, Response<ResponseWrapper<BaseModel>> response) {
                        updateData();
                    }

                    @Override
                    public void onFailure(Call<ResponseWrapper<BaseModel>> call, Throwable t) {
                        updateData();
                    }
                });
            }

            @Override
            public void onClickShare(Post post) {

                RequestBody rbPostId = RequestBody.create(MediaType.parse("text/plain"), String.valueOf(post.id));

                PostsService service = ServiceManager.sharedInstance().getServiceHandler().create(PostsService.class);
                Call<ResponseWrapper<BaseModel>> call = service.postCreateShare(rbPostId);
                call.enqueue(new ResponseHandler<ResponseWrapper<BaseModel>>() {
                    @Override
                    public void onResponseParsed(Call<ResponseWrapper<BaseModel>> call, Response<ResponseWrapper<BaseModel>> response) {
                        onShowMessage(Util.getString(R.string.post_share_success), Util.getString(R.string.msg_ok), null);

                        updateData();
                    }

                    @Override
                    public void onFailure(Call<ResponseWrapper<BaseModel>> call, Throwable t) {
                        updateData();
                    }
                });
            }

            @Override
            public void onClickTag(Tag tag) {
                PostByTagListFragment frag = PostByTagListFragment.getInstance(tag);
                MenuFragment.setNextContentFragment(frag);
                Activity activity = getActivity();
                if (activity != null && !activity.isFinishing()) {
                    Intent intent = new Intent(activity, InnerPageFragmentActivity.class);
                    Util.moveToActivity(activity, intent, true);
                }
            }

            @Override
            public void onClickProfile(Profile profile) {
                WallFragment frag = WallFragment.getInstance(profile.id);
                MenuFragment.setNextContentFragment(frag);
                Activity activity = getActivity();
                if (activity != null && !activity.isFinishing()) {
                    Intent intent = new Intent(activity, InnerPageFragmentActivity.class);
                    Util.moveToActivity(activity, intent, true);
                }
            }
        };
        mFactory = new PostRowViewFactory(new ArrayList<Post>(), mFactoryCallback);
        mAdapter = new RowViewFactoryListAdapter(mFactory);
        mListView.setAdapter(mAdapter);



        mLayoutCategory= (LinearLayout)getView().findViewById(R.id.layout_category);
        if (mLayoutCategory != null) {
            LayoutInflater inflater = (LayoutInflater) MyApplication.getAppContext().getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            for (final Constant.PostCategory category : Constant.getPostCategories(true)) {
                final View cellView = inflater.inflate(R.layout.cell_post_category, mLayoutCategory, false);
                final TextView txtName = ((TextView) cellView.findViewById(R.id.txt_name));
                txtName.setText(category.name);
                if (category.id == 0)
                    cellView.findViewById(R.id.img_icon).setVisibility(View.GONE);
                else
                    ((ImageView) cellView.findViewById(R.id.img_icon)).setImageResource(category.iconRes);
                cellView.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        if (mCategoryId == category.id)
                            return;

                        View currentView = mLayoutCategory.findViewWithTag(mCategoryId);
                        if (currentView != null) {
                            TextView txtCurrentName = ((TextView) currentView.findViewById(R.id.txt_name));
                            txtCurrentName.setTextColor(Util.getColor(MyApplication.getAppContext(), R.color.post_category_text_color));
                            ((RelativeLayout) currentView.findViewById(R.id.layout_txt_wrapper)).setBackground(Util.getDrawable(MyApplication.getAppContext(), R.drawable.post_category_bg));
                            currentView.findViewById(R.id.img_arr).setVisibility(View.INVISIBLE);
                            ((ImageView) currentView.findViewById(R.id.img_icon)).setColorFilter(Util.getColor(MyApplication.getAppContext(), R.color.post_category_text_color));
                        }

                        txtName.setTextColor(Util.getColor(MyApplication.getAppContext(), R.color.main_text_color_invert));
                        ((RelativeLayout) cellView.findViewById(R.id.layout_txt_wrapper)).setBackground(Util.getDrawable(MyApplication.getAppContext(), R.drawable.post_category_bg_highlight));
                        cellView.findViewById(R.id.img_arr).setVisibility(View.VISIBLE);
                        ((ImageView) cellView.findViewById(R.id.img_icon)).setColorFilter(Util.getColor(MyApplication.getAppContext(), R.color.main_text_color_invert));

                        mCategoryId = category.id;
                        updateData();
                    }
                });

                if (category.id == mCategoryId) {
                    txtName.setTextColor(Util.getColor(MyApplication.getAppContext(), R.color.main_text_color_invert));
                    ((RelativeLayout) cellView.findViewById(R.id.layout_txt_wrapper)).setBackground(Util.getDrawable(MyApplication.getAppContext(), R.drawable.post_category_bg_highlight));
                    cellView.findViewById(R.id.img_arr).setVisibility(View.VISIBLE);
                    ((ImageView) cellView.findViewById(R.id.img_icon)).setColorFilter(Util.getColor(MyApplication.getAppContext(), R.color.main_text_color_invert));
                }

                cellView.setTag(category.id);
                mLayoutCategory.addView(cellView, new LinearLayout.LayoutParams(ViewGroup.LayoutParams.WRAP_CONTENT, ViewGroup.LayoutParams.MATCH_PARENT));
            }
        }
    }

    @Override
    public void updateUI(){
        if (getView() != null){
            getView().post(new Runnable() {
                @Override
                public void run() {
                    mAdapter.setFactory(mFactory);
                    mAdapter.notifyDataSetChanged();
                }
            });
        }
    }

    @Override
    public void updateData(){
        PostsService service = ServiceManager.sharedInstance().getServiceHandler().create(PostsService.class);
        Call<ResponseListWrapper<Post>> call = null;
        if (mCategoryId == 0){
            call = service.getPosts();
        }
        else{
            call = service.getPostsByCategory(mCategoryId);
        }
        call.enqueue(new ResponseHandler<ResponseListWrapper<Post>>() {

            @Override
            public void onResponseParsed(Call<ResponseListWrapper<Post>> call, Response<ResponseListWrapper<Post>> response) {
                // FIXME pagination
                if (response.body() != null && response.body().data != null) {
                    mPosts = new ArrayList<Post>(response.body().data);
                    mFactory = new PostRowViewFactory(mPosts, mFactoryCallback, mAdv);
                    updateUI();
                }
            }

            @Override
            public void onFailure(Call<ResponseListWrapper<Post>> call, Throwable t) {
                onHandleError(Util.getString(R.string.msg_error), null, null, new MessageDialogFragment.MessageDialogFragmentCallback() {

                    @Override
                    public void onClickPositiveButton(MessageDialogFragment fragment, DialogInterface dialog, int id) {
                        updateData();
                    }

                    @Override
                    public void onClickNegativeButton(MessageDialogFragment fragment, DialogInterface dialog, int id) {

                    }
                });
            }
        });

        AdvService serviceAdv = ServiceManager.sharedInstance().getServiceHandler().create(AdvService.class);
        Call<ResponseListWrapper<Adv>> callAdv = serviceAdv.getAdvListByType(Constant.ADV_TYPE_POST_LIST);
        callAdv.enqueue(new ResponseHandler<ResponseListWrapper<Adv>>() {
            @Override
            public void onResponseParsed(Call<ResponseListWrapper<Adv>> call, Response<ResponseListWrapper<Adv>> response) {
                if (response.body() != null && response.body().data != null){
                    mAdv = response.body().data.get(0);
                    mFactory = new PostRowViewFactory(mPosts, mFactoryCallback, mAdv);
                    updateUI();

//                    Picasso.with(MyApplication.getAppContext()).load(ServiceManager.sharedInstance().getImageUrl(ServiceManager.IMAGE_URL_ADV, adv.image_id)).into(imgAdv);
//                    imgAdv.setOnClickListener(new View.OnClickListener() {
//                        @Override
//                        public void onClick(View view) {
//                            Intent i = new Intent(Intent.ACTION_VIEW);
//                            i.setData(Uri.parse(adv.link));
//                            startActivity(i);
//                        }
//                    });
                }
            }

            @Override
            public void onFailure(Call<ResponseListWrapper<Adv>> call, Throwable t) {

            }
        });
    }

}
