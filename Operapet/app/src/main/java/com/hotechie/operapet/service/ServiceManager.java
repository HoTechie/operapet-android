package com.hotechie.operapet.service;

import com.hotechie.operapet.SessionManager;

import java.io.IOException;
import java.util.concurrent.TimeUnit;

import okhttp3.Interceptor;
import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.Response;
import okhttp3.logging.HttpLoggingInterceptor;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

/**
 * Created by duncan on 17/7/2016.
 */
public class ServiceManager extends Object {

    private static ServiceManager mInstance = null;
    public static ServiceManager sharedInstance(){
        if (mInstance == null)
            mInstance = new ServiceManager();
        return mInstance;
    }

    public static String IMAGE_URL_POST = "GetPostImageApi";
    public static String IMAGE_URL_PROFILE = "GetProfileImageApi";
    public static String IMAGE_URL_GIFT = "GetGiftImageApi";
    public static String IMAGE_URL_PET = "GetPetImageApi";
    public static String IMAGE_URL_ADV = "GetAdvImageApi";

    public Retrofit getServiceHandler(){
        HttpLoggingInterceptor logging = new HttpLoggingInterceptor();
        logging.setLevel(HttpLoggingInterceptor.Level.BODY);

        OkHttpClient.Builder builder = new OkHttpClient().newBuilder();
        builder.readTimeout(10, TimeUnit.SECONDS);
        builder.connectTimeout(5, TimeUnit.SECONDS);
        builder.addInterceptor(new Interceptor() {
            @Override public Response intercept(Chain chain) throws IOException {
                Request.Builder builder = chain.request().newBuilder();
//                User user = SessionManager.sharedInstance().getUser();
//                if (user != null) {
//                    builder.addHeader("user", String.valueOf(user.id));
//                    builder.addHeader("token", SessionManager.sharedInstance().getApiToken());
//                }
                if (SessionManager.sharedInstance().getMemberToken() != null && SessionManager.sharedInstance().getMemberToken().length() > 0) {
                    builder.addHeader("memberid", String.valueOf(SessionManager.sharedInstance().getMemberId()));
                    builder.addHeader("token", SessionManager.sharedInstance().getMemberToken());
                }
                Request request = builder.build();
                return chain.proceed(request);
            }
        });

        builder.addInterceptor(logging);

        OkHttpClient client = builder.build();

        return new Retrofit.Builder()
                .baseUrl("http://dev13.palmary.hk/operapet/frontend/api/")
//                .baseUrl("http://192.168.11.9:8080/api/")
                .addConverterFactory(GsonConverterFactory.create())
                .client(client)
                .build();
    }

    public String getImageUrl(String urlPart, int i){
        return "http://dev13.palmary.hk/operapet/frontend/api/" + urlPart + "/" + String.valueOf(i);
//        return "http://192.168.11.29:8080/api/" + urlPart + "/" + String.valueOf(i);
    }

}
