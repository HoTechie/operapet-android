package com.hotechie.operapet.service.model;

/**
 * Created by duncan on 13/4/2017.
 */

public class Notification extends BaseModel {
    public int push_notification_id;
    public int member_id;
    public int post_id;
    public int image_id;
    public String description = "";
    public String status;
    public String crt_date = "";
}
