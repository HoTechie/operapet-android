package com.hotechie.operapet.ui.row_view_factory;

import android.content.res.Resources;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AbsListView;
import android.widget.BaseAdapter;
import android.widget.RelativeLayout;

import com.hotechie.operapet.MyApplication;
import com.hotechie.operapet.R;
import com.hotechie.operapet.Util;


/**
 * Created by duncan on 10/7/2016.
 */
public class RowViewFactoryListAdapter extends BaseAdapter {

    protected RowViewFactory mFactory = null;

    public RowViewFactoryListAdapter(RowViewFactory factory){
        mFactory = factory;
    }

    public void setFactory(RowViewFactory factory){
        mFactory = factory;
    }

    @Override
    public int getCount() {
        return mFactory.getCount();
    }

    @Override
    public Object getItem(int i) {
        return mFactory.getItem(i);
    }

    @Override
    public long getItemId(int i) {
        return 0;
    }

    @Override
    public View getView(int i, View view, ViewGroup viewGroup) {
        Object obj = mFactory.getItem(i);
        View contentView = mFactory.getView(obj);
        RelativeLayout layout = new RelativeLayout(MyApplication.getAppContext());
        layout.addView(contentView, new ViewGroup.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.WRAP_CONTENT));


        if(mFactory.modifyViewLayout(contentView, obj, i)){
            Resources resources = MyApplication.getAppContext().getResources();
            RelativeLayout.LayoutParams params = (RelativeLayout.LayoutParams)contentView.getLayoutParams();
            params.setMargins(resources.getDimensionPixelSize(R.dimen.list_row_margin_horizontal), resources.getDimensionPixelSize(R.dimen.list_row_margin_vertical), resources.getDimensionPixelSize(R.dimen.list_row_margin_horizontal), resources.getDimensionPixelSize(R.dimen.list_row_margin_vertical));

//            View line = new View(MyApplication.getAppContext());
//            layout.addView(line);
//            RelativeLayout.LayoutParams lineParams = (RelativeLayout.LayoutParams)line.getLayoutParams();
//            lineParams.addRule(RelativeLayout.ALIGN_PARENT_BOTTOM, RelativeLayout.TRUE);
//            lineParams.height = resources.getDimensionPixelSize(R.dimen.list_row_divider_height);
//            lineParams.width = RelativeLayout.LayoutParams.MATCH_PARENT;
//            lineParams.setMargins(resources.getDimensionPixelSize(R.dimen.list_row_margin_horizontal), 0, resources.getDimensionPixelSize(R.dimen.list_row_margin_horizontal), 0);
//            line.setBackgroundColor(Util.getColor(MyApplication.getAppContext(), R.color.list_view_divider));
        }
        return layout;
    }
}
