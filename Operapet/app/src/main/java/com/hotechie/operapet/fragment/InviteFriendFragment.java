package com.hotechie.operapet.fragment;

import android.view.View;
import android.widget.EditText;
import android.widget.TextView;

import com.hotechie.operapet.R;
import com.hotechie.operapet.Util;
import com.hotechie.operapet.service.MemberService;
import com.hotechie.operapet.service.ResponseHandler;
import com.hotechie.operapet.service.ServiceManager;
import com.hotechie.operapet.service.model.BaseModel;
import com.hotechie.operapet.service.model.ResponseWrapper;

import java.util.ArrayList;

import okhttp3.MediaType;
import okhttp3.RequestBody;
import retrofit2.Call;
import retrofit2.Response;

/**
 * Created by duncan on 19/10/2016.
 */

public class InviteFriendFragment extends BaseFragment {
    private static String TAG = "InviteFriendFragment";

    public static InviteFriendFragment getInstance(){
        InviteFriendFragment frag = new InviteFriendFragment();
        return frag;
    }

    @Override
    public String getTitle() {
        return Util.getString(R.string.title_invite_friend_page);
    }

    @Override
    public int layoutRes() {
        return R.layout.fragment_invite_friend;
    }

    @Override
    public void configUI() {
        super.configUI();

        getView().findViewById(R.id.layout_btn_send).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                String telCsv = "";

                ArrayList<View> editTels = new ArrayList<>();
                getView().findViewsWithText(editTels, "tel", View.FIND_VIEWS_WITH_CONTENT_DESCRIPTION);
                for (View editText : editTels){
                    String rawText = ((EditText)editText).getText().toString();
                    if (rawText.length() == 8){
                        if (telCsv.length() > 0){
                            telCsv += ",";
                        }
                        telCsv += rawText;
                    }
                }

                if (telCsv.length() > 0){
                    RequestBody rbPhoneListCsv = RequestBody.create(MediaType.parse("text/plain"), telCsv);

                    MemberService service = ServiceManager.sharedInstance().getServiceHandler().create(MemberService.class);
                    Call<ResponseWrapper<BaseModel>> call = service.postReferalFriendSendSms(rbPhoneListCsv);
                    call.enqueue(new ResponseHandler<ResponseWrapper<BaseModel>>() {
                        @Override
                        public void onResponseParsed(Call<ResponseWrapper<BaseModel>> call, Response<ResponseWrapper<BaseModel>> response) {
                            if (response.body() != null){
                                onShowMessage(Util.getString(R.string.invite_friend_success), Util.getString(R.string.msg_ok), null);
                                updateUI();
                            }
                            else{
                                onShowMessage(Util.getString(R.string.msg_error), Util.getString(R.string.msg_ok), null);
                            }
                        }

                        @Override
                        public void onFailure(Call<ResponseWrapper<BaseModel>> call, Throwable t) {
                            onShowMessage(Util.getString(R.string.msg_error), Util.getString(R.string.msg_ok), null);
                        }
                    });
                }

            }
        });
    }

    @Override
    protected void updateUIRunnable() {
        super.updateUIRunnable();

        ArrayList<View> editTels = new ArrayList<>();
        getView().findViewsWithText(editTels, "tel", View.FIND_VIEWS_WITH_CONTENT_DESCRIPTION);
        for (View editText : editTels){
            ((EditText)editText).setText("");

        }
    }
}
