package com.hotechie.operapet.service;

import com.hotechie.operapet.service.model.Adv;
import com.hotechie.operapet.service.model.Pet;
import com.hotechie.operapet.service.model.ResponseListWrapper;

import retrofit2.Call;
import retrofit2.http.GET;
import retrofit2.http.Path;

/**
 * Created by duncan on 4/11/2016.
 */

public interface AdvService {

    @GET("GetAdvListByTypeApi/{type_id}")
    Call<ResponseListWrapper<Adv>> getAdvListByType(@Path("type_id")int type_id);
}
