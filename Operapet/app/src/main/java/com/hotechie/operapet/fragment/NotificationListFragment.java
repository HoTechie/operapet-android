package com.hotechie.operapet.fragment;

import android.content.DialogInterface;
import android.content.Intent;

import com.hotechie.operapet.InnerPageEditFragmentActivity;
import com.hotechie.operapet.InnerPageFragmentActivity;
import com.hotechie.operapet.MyApplication;
import com.hotechie.operapet.R;
import com.hotechie.operapet.SessionManager;
import com.hotechie.operapet.Util;
import com.hotechie.operapet.service.MemberService;
import com.hotechie.operapet.service.ProfileService;
import com.hotechie.operapet.service.ResponseHandler;
import com.hotechie.operapet.service.ServiceManager;
import com.hotechie.operapet.service.model.Notification;
import com.hotechie.operapet.service.model.Pet;
import com.hotechie.operapet.service.model.ResponseListWrapper;
import com.hotechie.operapet.ui.row_view_factory.NotificationRowViewFactory;
import com.hotechie.operapet.ui.row_view_factory.PetRowViewFactory;
import com.hotechie.operapet.ui.row_view_factory.RowViewFactoryListAdapter;
import com.hotechie.util.MessageDialogFragment;

import java.util.ArrayList;
import java.util.List;

import retrofit2.Call;
import retrofit2.Response;

/**
 * Created by duncan on 14/4/2017.
 */

public class NotificationListFragment extends BaseListFragment {
    private static String TAG = "NotificationListFragment";

    public static NotificationListFragment getInstance(){
        NotificationListFragment frag = new NotificationListFragment();
        return frag;
    }

    protected NotificationRowViewFactory mFactory = null;
    protected NotificationRowViewFactory.NotificationRowViewFactoryCallback mFactoryCallback = null;

    protected List<Notification> mNotifications = new ArrayList<>();

    @Override
    public void onResume() {
        super.onResume();
        updateData();
    }

    @Override
    public String getTitle(){
        return MyApplication.getAppContext().getString(R.string.title_notification);
    }

    @Override
    public void configUI(){
        super.configUI();

        mFactoryCallback = new NotificationRowViewFactory.NotificationRowViewFactoryCallback() {
            @Override
            public void onClick(Notification notification) {
                PostDetailFragment frag = PostDetailFragment.getInstance(notification.post_id);
                MenuFragment.setNextContentFragment(frag);
                Intent newIntent = new Intent(getActivity(), InnerPageFragmentActivity.class);
                Util.moveToActivity(getActivity(), newIntent, true);
            }
        };
        mFactory = new NotificationRowViewFactory(new ArrayList<Notification>(), mFactoryCallback);
        mAdapter = new RowViewFactoryListAdapter(mFactory);
        mListView.setAdapter(mAdapter);

    }

    @Override
    public void updateUI(){
        if (getView() != null){
            getView().post(new Runnable() {
                @Override
                public void run() {
                    mAdapter.setFactory(mFactory);
                    mAdapter.notifyDataSetChanged();
                }
            });
        }
    }

    @Override
    public void updateData(){
//        mNotifications = new ArrayList<Notification>();
//        mNotifications.add(new Notification());
//        mNotifications.add(new Notification());
//        mNotifications.add(new Notification());
//        mFactory = new NotificationRowViewFactory(mNotifications, mFactoryCallback);
//        updateUI();

        ProfileService service = ServiceManager.sharedInstance().getServiceHandler().create(ProfileService.class);
        Call<ResponseListWrapper<Notification>> call = service.getNotification();
        call.enqueue(new ResponseHandler<ResponseListWrapper<Notification>>() {

            @Override
            public void onResponseParsed(Call<ResponseListWrapper<Notification>> call, Response<ResponseListWrapper<Notification>> response) {
                if (response.body() != null && response.body().data != null) {
                    mNotifications = new ArrayList<Notification>(response.body().data);
                    mFactory = new NotificationRowViewFactory(mNotifications, mFactoryCallback);
                    updateUI();
                }
            }

            @Override
            public void onFailure(Call<ResponseListWrapper<Notification>> call, Throwable t) {
                onHandleError(Util.getString(R.string.msg_error), null, null, new MessageDialogFragment.MessageDialogFragmentCallback() {

                    @Override
                    public void onClickPositiveButton(MessageDialogFragment fragment, DialogInterface dialog, int id) {
                        updateData();
                    }

                    @Override
                    public void onClickNegativeButton(MessageDialogFragment fragment, DialogInterface dialog, int id) {

                    }
                });
            }
        });
    }
}
