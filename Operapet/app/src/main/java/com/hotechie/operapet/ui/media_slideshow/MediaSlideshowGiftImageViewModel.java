package com.hotechie.operapet.ui.media_slideshow;

import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.RelativeLayout;

import com.hotechie.operapet.MyApplication;
import com.squareup.picasso.Picasso;

/**
 * Created by duncan on 18/10/2016.
 */

public class MediaSlideshowGiftImageViewModel implements MediaSlideshowViewModel {

    protected String mImgUrl = null;

    public MediaSlideshowGiftImageViewModel(String imgUrl){
        mImgUrl = imgUrl;
    }

    @Override
    public void fillView(RelativeLayout container) {
        if (mImgUrl == null)
            return;


        ImageView imageView = new ImageView(container.getContext());
        imageView.setScaleType(ImageView.ScaleType.FIT_CENTER);
        Picasso.with(MyApplication.getAppContext()).load(mImgUrl).into(imageView);
        ViewGroup.LayoutParams imageViewLayoutParams = new ViewGroup.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.MATCH_PARENT);
        container.addView(imageView, imageViewLayoutParams);
    }
}
