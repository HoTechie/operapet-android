package com.hotechie.operapet.fragment;

import android.app.Activity;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.TextView;

import com.hotechie.operapet.InnerPageFragmentActivity;
import com.hotechie.operapet.MyApplication;
import com.hotechie.operapet.R;
import com.hotechie.operapet.SessionManager;
import com.hotechie.operapet.Util;
import com.hotechie.operapet.service.GiftService;
import com.hotechie.operapet.service.ProfileService;
import com.hotechie.operapet.service.ResponseHandler;
import com.hotechie.operapet.service.ServiceManager;
import com.hotechie.operapet.service.model.Gift;
import com.hotechie.operapet.service.model.Profile;
import com.hotechie.operapet.service.model.ResponseListWrapper;
import com.hotechie.operapet.service.model.ResponseWrapper;
import com.hotechie.operapet.ui.row_view_factory.GiftRecordRowViewFactory;
import com.hotechie.operapet.ui.row_view_factory.GiftRowViewFactory;
import com.hotechie.operapet.ui.row_view_factory.RowViewFactoryListAdapter;
import com.hotechie.util.MessageDialogFragment;

import java.util.ArrayList;
import java.util.List;

import okhttp3.MediaType;
import okhttp3.RequestBody;
import retrofit2.Call;
import retrofit2.Response;

/**
 * Created by duncan on 19/10/2016.
 */

public class GiftRecordListFragment extends BaseListFragment {
    private static String TAG = "GiftRecordListFrag";

    public static GiftRecordListFragment getInstance(){
        GiftRecordListFragment frag = new GiftRecordListFragment();
        return frag;
    }

    protected List<Gift> mGifts = new ArrayList<>();
    protected GiftRecordRowViewFactory mFactory = null;
    protected GiftRecordRowViewFactory.GiftRecordRowViewFactoryCallback mFactoryCallback = null;


    @Override
    public void onResume() {
        super.onResume();
        updateData();
    }

    @Override
    public String getTitle() {
        return Util.getString(R.string.profile_gift_record);
    }

    @Override
    public void configUI() {
        super.configUI();

        mFactoryCallback = new GiftRecordRowViewFactory.GiftRecordRowViewFactoryCallback() {
            @Override
            public void onClick(final Gift gift) {
                // TODO collect gift
                onShowMessage(Util.getString(R.string.gift_collect_q), Util.getString(R.string.msg_yes), Util.getString(R.string.msg_no), new MessageDialogFragment.MessageDialogFragmentCallback() {
                    @Override
                    public void onClickPositiveButton(MessageDialogFragment fragment, DialogInterface dialog, int id) {
                        RequestBody rbGiftRecord = RequestBody.create(MediaType.parse("text/plain"), String.valueOf(gift.id)); //redemption_id

                        GiftService service = ServiceManager.sharedInstance().getServiceHandler().create(GiftService.class);
                        Call<ResponseWrapper<Gift>> call = service.postCollectGift(rbGiftRecord);
                        call.enqueue(new ResponseHandler<ResponseWrapper<Gift>>() {
                            @Override
                            public void onResponseParsed(Call<ResponseWrapper<Gift>> call, Response<ResponseWrapper<Gift>> response) {
                                if (response.body() != null){
                                    onShowMessage(Util.getString(R.string.gift_collect_success), Util.getString(R.string.msg_ok), null);
                                }

                                updateData();
                            }

                            @Override
                            public void onFailure(Call<ResponseWrapper<Gift>> call, Throwable t) {
                                updateData();
                            }
                        });
                    }

                    @Override
                    public void onClickNegativeButton(MessageDialogFragment fragment, DialogInterface dialog, int id) {

                    }
                });
            }
        };
        mFactory = new GiftRecordRowViewFactory(mGifts, mFactoryCallback);
        mAdapter = new RowViewFactoryListAdapter(mFactory);
        mListView.setAdapter(mAdapter);

    }

    @Override
    protected void updateUIRunnable() {
        super.updateUIRunnable();

        mAdapter.setFactory(mFactory);
        mAdapter.notifyDataSetChanged();
    }

    @Override
    public void updateData() {
        super.updateData();

        showLoadingScreen(true, "", Util.getString(R.string.msg_loading));

        ProfileService service = ServiceManager.sharedInstance().getServiceHandler().create(ProfileService.class);
        Call<ResponseListWrapper<Gift>> call = service.getGiftHistory();
        call.enqueue(new ResponseHandler<ResponseListWrapper<Gift>>() {
            @Override
            public void onResponseParsed(Call<ResponseListWrapper<Gift>> call, Response<ResponseListWrapper<Gift>> response) {
                showLoadingScreen(false, "", Util.getString(R.string.msg_loading));
                if (response.body() != null && response.body().data != null){
                    mGifts = new ArrayList<Gift>(response.body().data);
                    mFactory = new GiftRecordRowViewFactory(mGifts, mFactoryCallback);
                    updateUI();
                }
            }

            @Override
            public void onFailure(Call<ResponseListWrapper<Gift>> call, Throwable t) {
                showLoadingScreen(false, "", Util.getString(R.string.msg_loading));
                onHandleError(Util.getString(R.string.msg_error), null, null, new MessageDialogFragment.MessageDialogFragmentCallback() {

                    @Override
                    public void onClickPositiveButton(MessageDialogFragment fragment, DialogInterface dialog, int id) {
                        updateData();
                    }

                    @Override
                    public void onClickNegativeButton(MessageDialogFragment fragment, DialogInterface dialog, int id) {

                    }
                });
            }
        });
    }
}
