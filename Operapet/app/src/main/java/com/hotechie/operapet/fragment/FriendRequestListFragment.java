package com.hotechie.operapet.fragment;

import android.app.Activity;
import android.content.DialogInterface;
import android.content.Intent;

import com.hotechie.operapet.InnerPageFragmentActivity;
import com.hotechie.operapet.R;
import com.hotechie.operapet.Util;
import com.hotechie.operapet.service.MemberService;
import com.hotechie.operapet.service.PostsService;
import com.hotechie.operapet.service.ResponseHandler;
import com.hotechie.operapet.service.ServiceManager;
import com.hotechie.operapet.service.model.BaseModel;
import com.hotechie.operapet.service.model.Profile;
import com.hotechie.operapet.service.model.ResponseListWrapper;
import com.hotechie.operapet.service.model.ResponseWrapper;
import com.hotechie.operapet.ui.row_view_factory.FriendRequestRowViewFactory;
import com.hotechie.operapet.ui.row_view_factory.RowViewFactoryListAdapter;
import com.hotechie.operapet.ui.row_view_factory.UserRowViewFactory;
import com.hotechie.util.MessageDialogFragment;

import java.util.ArrayList;
import java.util.List;

import okhttp3.MediaType;
import okhttp3.RequestBody;
import retrofit2.Call;
import retrofit2.Response;

/**
 * Created by duncan on 17/10/2016.
 */

public class FriendRequestListFragment extends BaseListFragment {
    private static String TAG = "FriendRequestListFrag";

    public static FriendRequestListFragment getInstance(int postId){
        FriendRequestListFragment frag = new FriendRequestListFragment();
        return frag;
    }

    protected List<Profile> mUsers = new ArrayList<>();
    protected FriendRequestRowViewFactory mFactory = null;
    protected FriendRequestRowViewFactory.FriendRequestRowViewFactoryCallback mFactoryCallback = null;


    @Override
    public String getTitle() {
        return Util.getString(R.string.title_friend_request);
    }

    @Override
    public void onResume() {
        super.onResume();
        updateData();
    }

    @Override
    public void configUI() {
        super.configUI();

        mFactoryCallback = new FriendRequestRowViewFactory.FriendRequestRowViewFactoryCallback() {
            @Override
            public void onClick(Profile profile) {
                WallFragment frag = WallFragment.getInstance(profile.id);
                MenuFragment.setNextContentFragment(frag);
                Activity activity = getActivity();
                if (activity != null && !activity.isFinishing()) {
                    Intent intent = new Intent(activity, InnerPageFragmentActivity.class);
                    Util.moveToActivity(activity, intent, true);
                }
            }

            @Override
            public void onClickResponse(Profile profile, boolean isAccept) {

                if (isAccept){
                    RequestBody rbTargetMemberId = RequestBody.create(MediaType.parse("text/plain"), String.valueOf(profile.id));

                    MemberService service = ServiceManager.sharedInstance().getServiceHandler().create(MemberService.class);
                    Call<ResponseWrapper<BaseModel>> call = service.postAcceptFriend(rbTargetMemberId);
                    call.enqueue(new ResponseHandler<ResponseWrapper<BaseModel>>() {
                        @Override
                        public void onResponseParsed(Call<ResponseWrapper<BaseModel>> call, Response<ResponseWrapper<BaseModel>> response) {
                            updateData();
                        }

                        @Override
                        public void onFailure(Call<ResponseWrapper<BaseModel>> call, Throwable t) {
                            onShowMessage(Util.getString(R.string.msg_error), Util.getString(R.string.msg_ok), null);
                            updateData();
                        }
                    });
                }
                else{
                    RequestBody rbTargetMemberId = RequestBody.create(MediaType.parse("text/plain"), String.valueOf(profile.friend_relation.friend_relationship_id));

                    MemberService service = ServiceManager.sharedInstance().getServiceHandler().create(MemberService.class);
                    Call<ResponseWrapper<BaseModel>> call = service.postDeleteFriend(rbTargetMemberId);
                    call.enqueue(new ResponseHandler<ResponseWrapper<BaseModel>>() {
                        @Override
                        public void onResponseParsed(Call<ResponseWrapper<BaseModel>> call, Response<ResponseWrapper<BaseModel>> response) {
                            updateData();
                        }

                        @Override
                        public void onFailure(Call<ResponseWrapper<BaseModel>> call, Throwable t) {
                            onShowMessage(Util.getString(R.string.msg_error), Util.getString(R.string.msg_ok), null);
                            updateData();
                        }
                    });
                }

            }
        };

        mFactory = new FriendRequestRowViewFactory(mUsers, mFactoryCallback);
        mAdapter = new RowViewFactoryListAdapter(mFactory);
        mListView.setAdapter(mAdapter);
    }

    @Override
    protected void updateUIRunnable() {
        super.updateUIRunnable();

        mAdapter.setFactory(mFactory);
        mAdapter.notifyDataSetChanged();
    }

    @Override
    public void updateData() {
        super.updateData();

        MemberService service = ServiceManager.sharedInstance().getServiceHandler().create(MemberService.class);
        Call<ResponseListWrapper<Profile>> call = service.getFriendRequestList();
        call.enqueue(new ResponseHandler<ResponseListWrapper<Profile>>() {
            @Override
            public void onResponseParsed(Call<ResponseListWrapper<Profile>> call, Response<ResponseListWrapper<Profile>> response) {
                if (response.body() != null && response.body().data != null) {
                    mUsers = new ArrayList<Profile>(response.body().data);
                    mFactory = new FriendRequestRowViewFactory(mUsers, mFactoryCallback);
                    updateUI();
                }
            }

            @Override
            public void onFailure(Call<ResponseListWrapper<Profile>> call, Throwable t) {
                onHandleError(Util.getString(R.string.msg_error), null, null, new MessageDialogFragment.MessageDialogFragmentCallback() {

                    @Override
                    public void onClickPositiveButton(MessageDialogFragment fragment, DialogInterface dialog, int id) {
                        updateData();
                    }

                    @Override
                    public void onClickNegativeButton(MessageDialogFragment fragment, DialogInterface dialog, int id) {

                    }
                });
            }
        });
    }
}
