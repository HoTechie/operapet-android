package com.hotechie.operapet.fragment;

import android.os.Bundle;
import android.widget.ListView;

import com.hotechie.operapet.R;
import com.hotechie.operapet.ui.row_view_factory.RowViewFactoryListAdapter;

/**
 * Created by duncan on 19/7/2016.
 */
public class BaseListFragment extends BaseFragment {
    private static String TAG = "BaseListFragment";

    protected ListView mListView = null;
    protected RowViewFactoryListAdapter mAdapter = null;


    public static BaseListFragment getInstance(Bundle b){
        BaseListFragment frag = new BaseListFragment();
        frag.setArguments(b);
        return frag;
    }

    @Override
    public int layoutRes(){
        return R.layout.fragment_base_list;
    }

    @Override
    public void configUI() {
        super.configUI();

        mListView = (ListView)getView().findViewById(R.id.list_view);
    }
}
