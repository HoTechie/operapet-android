package com.hotechie.operapet.ui.row_view_factory;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import com.hotechie.operapet.Constant;
import com.hotechie.operapet.MyApplication;
import com.hotechie.operapet.R;
import com.hotechie.operapet.Util;
import com.hotechie.operapet.service.model.Profile;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by duncan on 15/10/2016.
 */

public class SearchFriendRowViewFactory extends UserRowViewFactory {
    public static interface SearchFriendRowViewFactoryCallback{
        public void onClick(Profile profile);
        public void onClickFriendStatus(Profile profile, Integer status);
    }

    protected List<Profile> mUsers = new ArrayList<>();
    protected SearchFriendRowViewFactoryCallback mCallback = null;

    public SearchFriendRowViewFactory(List<Profile> users, SearchFriendRowViewFactoryCallback callback) {
        super(users, false);

        mRowViewRes = R.layout.row_friend_search;

        mCallback = callback;

    }


    @Override
    public View getView(Object object) {
        final Profile profile = (Profile)object;
        View rowView = super.getView(object);

        ImageView imgFriend = (ImageView) rowView.findViewById(R.id.img_friend);
        if (profile.friend_relation != null) {
            if (profile.friend_relation.status == Constant.FRIENDSHIP_STATUS_PENDING) {
                imgFriend.setImageResource(R.drawable.icon_tab_friend);
                imgFriend.setColorFilter(Util.getColor(MyApplication.getAppContext(), R.color.theme_color_darker));
            }
            else if (profile.friend_relation.status == Constant.FRIENDSHIP_STATUS_NO_FRIEND){
                imgFriend.setImageResource(R.drawable.icon_add_friend);
            }
            else if (profile.friend_relation.status == Constant.FRIENDSHIP_STATUS_CONFIRMED){
                imgFriend.setImageResource(R.drawable.icon_is_friend);
            }
        }
        else{
            imgFriend.setImageResource(R.drawable.icon_add_friend);
        }

        if (mCallback != null){
            rowView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    mCallback.onClick(profile);
                }
            });
            imgFriend.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    mCallback.onClickFriendStatus(profile, profile.friend_relation == null ? null : profile.friend_relation.status);
                }
            });
        }

        return rowView;
    }

    @Override
    public boolean modifyViewLayout(View contentView, Object object, int index) {
        return false;
    }
}
