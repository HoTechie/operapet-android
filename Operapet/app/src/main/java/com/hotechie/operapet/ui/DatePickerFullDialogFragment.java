package com.hotechie.operapet.ui;

import android.app.DatePickerDialog;
import android.app.Dialog;
import android.content.res.Resources;
import android.os.Bundle;
import android.view.View;
import android.view.ViewGroup;

import com.hotechie.util.DatePickerDialogFragment;

import java.util.Calendar;

/**
 * Created by duncan on 8/10/2016.
 */

public class DatePickerFullDialogFragment extends DatePickerDialogFragment {


    public static final String KEY_YEAR = "year";


    public static DatePickerFullDialogFragment getInstance(Bundle b, DatePickerDialogFragmentCallback callback){
        DatePickerFullDialogFragment f = new DatePickerFullDialogFragment();
        if (b != null)
            f.setArguments(b);
        f.mCallback = callback;
        return f;
    }



    @Override
    public Dialog onCreateDialog(Bundle savedInstanceState) {

        Bundle infoBundle = new Bundle();
        if (getArguments() != null)
            infoBundle = getArguments();
        else if (savedInstanceState != null)
            infoBundle = savedInstanceState;

        Calendar cal = Calendar.getInstance();
        int year = infoBundle.getInt(KEY_YEAR, cal.get(Calendar.YEAR));
        int month = infoBundle.getInt(KEY_MONTH, cal.get(Calendar.MONTH));
        int day = infoBundle.getInt(KEY_DAY, cal.get(Calendar.DAY_OF_MONTH));
        DatePickerDialog dialog = new DatePickerDialog(getContext(), this, year, month, day);

        return dialog;


    }
}
