package com.hotechie.operapet.service.model;

import com.hotechie.operapet.Constant;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by duncan on 2/10/2016.
 */

public class Pet extends BaseModel {
/*
{
    "id": 1,
    "member_id": 1,
    "gender": 1,
    "image_id": 0,
    "pet_type": 5,
    "birth_date": "1989-12-03 00:00",
    "description": "testing pet",
    "medical_record": "testing record",
    "last_body_check_date": "2016-12-30 00:00",
    "next_body_check_date": "2016-12-31 00:00",
    "last_bath_hair_cut_description": "testing",
    "bmi_record_weight_1": 1,
    "bmi_record_length_1": 1,
    "bmi_record_weight_2": 1,
    "bmi_record_length_2": 1,
    "bmi_record_weight_3": 1,
    "bmi_record_length_3": 1,
    "pin_card_list": [
        {
            "id": 1,
            "pet_id": 1,
            "recent_pin_record_date": "2016-10-30 00:00",
            "next_pin_record_date": "2016-10-30 00:00",
            "description": "testing "
        }
    ]
}
 */

    public int id = 0;
    public int member_id = -1;
    public int gender = Constant.GENDER_MALE;
    public int image_id = -1;
    public int pet_type = 1;
//    public String birth_date = "2015-10-3 00:00";
    public String birth_date = "";
    public String description = "";
    public String medical_record = "";
    public String last_body_check_date = "";
    public String next_body_check_date = "";
    public String last_bath_hair_cut_description = "";
    public float bmi_record_weight_1 = 0;
    public float bmi_record_length_1 = 0;
    public float bmi_record_weight_2 = 0;
    public float bmi_record_length_2 = 0;
    public float bmi_record_weight_3 = 0;
    public float bmi_record_length_3 = 0;
    public String bmi_record_date_1 = null;
    public String bmi_record_date_2 = null;
    public String bmi_record_date_3 = null;
    public List<PinCard> pin_card_list = new ArrayList<>();


    public int pet_id = 0; // for safety
}
