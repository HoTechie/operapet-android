package com.hotechie.operapet.fragment;

import android.app.Activity;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Bitmap;
import android.net.Uri;
import android.os.Bundle;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.hotechie.operapet.Constant;
import com.hotechie.operapet.InnerPageFragmentActivity;
import com.hotechie.operapet.MyApplication;
import com.hotechie.operapet.R;
import com.hotechie.operapet.SessionManager;
import com.hotechie.operapet.Util;
import com.hotechie.operapet.service.PetService;
import com.hotechie.operapet.service.ResponseHandler;
import com.hotechie.operapet.service.ServiceManager;
import com.hotechie.operapet.service.model.Pet;
import com.hotechie.operapet.service.model.PinCard;
import com.hotechie.operapet.service.model.ResponseWrapper;
import com.hotechie.operapet.ui.DatePickerFullDialogFragment;
import com.hotechie.util.DatePickerDialogFragment;
import com.hotechie.util.InputDialogFragment;
import com.hotechie.util.ListDialogFragment;
import com.hotechie.util.MessageDialogFragment;
import com.squareup.picasso.Picasso;

import org.json.JSONArray;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Objects;

import okhttp3.MediaType;
import okhttp3.RequestBody;
import retrofit2.Call;
import retrofit2.Response;

/**
 * Created by duncan on 4/10/2016.
 */

public class PetDetailFragment extends BaseFragment {
    private static String TAG = "PetDetailFragment";

    public static PetDetailFragment getInstance(Pet pet, boolean canEdit){
        PetDetailFragment frag = new PetDetailFragment();
        frag.mPet = pet; // null to create
        frag.mCanEdit = canEdit;
        return frag;
    }

    protected Pet mPet = null;
    protected String mSelectedPhotoUri = null;

    protected boolean mCanEdit = true;

    protected SimpleDateFormat originalTimeFormat;
    protected SimpleDateFormat targetTimeFormat;

    protected ImageView mImgProfile = null;
    protected ImageView mImgProfileBorder = null;
    protected TextView mTxtName = null;
    protected View mBtnEditName = null;

    protected ImageView mImgGenderGirl, mImgGenderBoy;

    protected View mLayoutTypeLeft = null;
    protected TextView mTxtTypeLeft = null;
    protected View mLayoutType = null;
    protected TextView mTxtType = null;

    protected TextView mTxtBirthday = null;
    protected View mBtnEditBirthday = null;

    protected LinearLayout mLayoutPinCard = null;
    protected View.OnClickListener mOnClickPinCardCell = null;
    protected PinCard mPinCard = null;
    protected View mLayoutPinCardDetail = null;
    protected View mBtnPinCardDetailEdit = null;
    protected TextView mTxtPinCardLastYear, mTxtPinCardLastMonth, mTxtPinCardLastDay, mTxtPinCardNextYear, mTxtPinCardNextMonth, mTxtPinCardNextDay, mTxtPinCardDescription;

    protected TextView mTxtLastHairCutting = null;
    protected View mBtnEditLastHairCutting = null;

    protected TextView mEditBmiWeight, mEditBmiLength, mTxtBmiResult, mTxtBmiStatusStr;
    protected ImageView mImgBmiStatus;

    protected TextView mTxtBmiYear1, mTxtBmiMonth1, mTxtBmiDay1, mTxtBmiResult1;
    protected TextView mTxtBmiYear2, mTxtBmiMonth2, mTxtBmiDay2, mTxtBmiResult2;
    protected TextView mTxtBmiYear3, mTxtBmiMonth3, mTxtBmiDay3, mTxtBmiResult3;

    protected TextView mTxtMedicalRecord = null;
    protected View mBtnEditMedicalRecord = null;

    protected TextView mTxtLastHealthCheckYear, mTxtLastHealthCheckMonth, mTxtLastHealthCheckDay;
    protected TextView mTxtNextHealthCheckYear, mTxtNextHealthCheckMonth, mTxtNextHealthCheckDay;

    @Override
    public String getTitle() {
        return Util.getString(R.string.title_pet);
    }

    @Override
    public void onResume() {
        super.onResume();

        PinCard pinCard = (PinCard)SessionManager.sharedInstance().getObject(SessionManager.VAULT_PIN_CARD_EDIT);
        if (mPet != null && pinCard != null){
            if (mPinCard == null){
                mPet.pin_card_list.add(pinCard);
            }
            else{
                mPinCard.name = pinCard.name;
                mPinCard.next_pin_record_date = pinCard.next_pin_record_date;
                mPinCard.recent_pin_record_date = pinCard.recent_pin_record_date;
                mPinCard.description = pinCard.description;
            }
            SessionManager.sharedInstance().putIn(SessionManager.VAULT_PIN_CARD_EDIT, null);
        }

        updateUI();
//        updateData();
    }

    @Override
    public int layoutRes() {
        return R.layout.fragment_pet;
    }

    @Override
    public boolean onClickActionBarItem(View view) {
        if (view.getId() == R.id.txt_right){
            // TODO save to server and trigger reload

            if (mPet.description == null || mPet.description.length() == 0){
                onShowMessage(Util.getString(R.string.pet_lb_name_must), Util.getString(R.string.msg_ok), null);
                return true;
            }


            String lastYear = mTxtLastHealthCheckYear.getText().toString();
            String lastMonth = mTxtLastHealthCheckMonth.getText().toString();
            String lastDay = mTxtLastHealthCheckDay.getText().toString();
            if (lastYear.length() != 0 || lastMonth.length() != 0 || lastDay.length() != 0) {
                if (!checkDate(Util.getString(R.string.pet_lb_last_health_check_date), lastYear, lastMonth, lastDay)) return false;
                String date = "";
                try{
                    String str = String.format("%d-%d-%d 00:00", Integer.parseInt(lastYear), Integer.parseInt(lastMonth), Integer.parseInt(lastDay));
                    originalTimeFormat.parse(str);
                    date = str;
                }
                catch (Exception e){
                    e.printStackTrace();
                }
                mPet.last_body_check_date = date;
            }
            else{
                mPet.last_body_check_date = "";
            }

            String nextYear = mTxtNextHealthCheckYear.getText().toString();
            String nextMonth = mTxtNextHealthCheckMonth.getText().toString();
            String nextDay = mTxtNextHealthCheckDay.getText().toString();
            if (nextYear.length() != 0 || nextMonth.length() != 0 || nextDay.length() != 0) {
                if (!checkDate(Util.getString(R.string.pet_lb_next_health_check_date), nextYear, nextMonth, nextDay)) return false;
                String date = "";
                try{
                    String str = String.format("%d-%d-%d 00:00", Integer.parseInt(nextYear), Integer.parseInt(nextMonth), Integer.parseInt(nextDay));
                    originalTimeFormat.parse(str);
                    date = str;
                }
                catch (Exception e){
                    e.printStackTrace();
                }
                mPet.next_body_check_date = date;
            }
            else{
                mPet.next_body_check_date = "";
            }

            RequestBody rbPetId = RequestBody.create(MediaType.parse("text/plain"), String.valueOf(mPet.id));
            RequestBody rbGender = RequestBody.create(MediaType.parse("text/plain"), String.valueOf(mPet.gender));
            RequestBody rbMemberId = RequestBody.create(MediaType.parse("text/plain"), String.valueOf(mPet.member_id));
            RequestBody rbPetType = RequestBody.create(MediaType.parse("text/plain"), String.valueOf(mPet.pet_type));
            RequestBody rbBirthDate = RequestBody.create(MediaType.parse("text/plain"), String.valueOf(mPet.birth_date));
            RequestBody rbDescription = RequestBody.create(MediaType.parse("text/plain"), String.valueOf(mPet.description));
            RequestBody rbMedicalRecord = RequestBody.create(MediaType.parse("text/plain"), String.valueOf(mPet.medical_record));
            RequestBody rbLastBodyCheckDate = RequestBody.create(MediaType.parse("text/plain"), String.valueOf(mPet.last_body_check_date));
            RequestBody rbNextBodyCheckDate = RequestBody.create(MediaType.parse("text/plain"), String.valueOf(mPet.next_body_check_date));
            RequestBody rbLastHairCutDescription = RequestBody.create(MediaType.parse("text/plain"), String.valueOf(mPet.last_bath_hair_cut_description));
            RequestBody rbBmiRecordWeight1 = RequestBody.create(MediaType.parse("text/plain"), String.valueOf(mPet.bmi_record_weight_1));
            RequestBody rbBmiRecordLength1 = RequestBody.create(MediaType.parse("text/plain"), String.valueOf(mPet.bmi_record_length_1));
            RequestBody rbBmiRecordWeight2 = RequestBody.create(MediaType.parse("text/plain"), String.valueOf(mPet.bmi_record_weight_2));
            RequestBody rbBmiRecordLength2 = RequestBody.create(MediaType.parse("text/plain"), String.valueOf(mPet.bmi_record_length_2));
            RequestBody rbBmiRecordWeight3 = RequestBody.create(MediaType.parse("text/plain"), String.valueOf(mPet.bmi_record_weight_3));
            RequestBody rbBmiRecordLength3 = RequestBody.create(MediaType.parse("text/plain"), String.valueOf(mPet.bmi_record_length_3));
            RequestBody rbBmiRecordDate1 = mPet.bmi_record_date_1 == null ? null : RequestBody.create(MediaType.parse("text/plain"), String.valueOf(mPet.bmi_record_date_1));
            RequestBody rbBmiRecordDate2 = mPet.bmi_record_date_2 == null ? null : RequestBody.create(MediaType.parse("text/plain"), String.valueOf(mPet.bmi_record_date_2));
            RequestBody rbBmiRecordDate3 = mPet.bmi_record_date_3 == null ? null : RequestBody.create(MediaType.parse("text/plain"), String.valueOf(mPet.bmi_record_date_3));


            String bmiLength = mEditBmiLength.getText().toString();
            String bmiWeight = mEditBmiWeight.getText().toString();
            if (bmiLength.length() > 0 && bmiWeight.length() > 0){
                try{
                    float length = Float.parseFloat(bmiLength);
                    float weight = Float.parseFloat(bmiWeight);

                    if (length > 0 && weight > 0){
                        rbBmiRecordWeight3 = rbBmiRecordWeight2;
                        rbBmiRecordLength3 = rbBmiRecordLength2;
                        rbBmiRecordDate3 = rbBmiRecordDate2;
                        rbBmiRecordWeight2 = rbBmiRecordWeight1;
                        rbBmiRecordLength2 = rbBmiRecordLength1;
                        rbBmiRecordDate2 = rbBmiRecordDate1;
                        rbBmiRecordWeight1 = RequestBody.create(MediaType.parse("text/plain"), String.valueOf(weight));
                        rbBmiRecordLength1 = RequestBody.create(MediaType.parse("text/plain"), String.valueOf(length));


                        Calendar calendar = Calendar.getInstance();
                        rbBmiRecordDate1 = RequestBody.create(MediaType.parse("text/plain"), String.format("%d-%d-%d 00:00", calendar.get(Calendar.YEAR), calendar.get(Calendar.MONTH) + 1, calendar.get(Calendar.DAY_OF_MONTH)));
                    }
                }
                catch (Exception e){
                    e.printStackTrace();
                }
            }



            JSONArray jsonArray = new JSONArray();
            for (PinCard pinCard : mPet.pin_card_list){
                jsonArray.put(pinCard.toJSONObject());
            }
            RequestBody rbPinCardJSON = RequestBody.create(MediaType.parse("text/plain"), jsonArray.toString());

            RequestBody rbPetImage = null;
            if (mSelectedPhotoUri != null) {
                Bitmap bitmap = getBitmap(mSelectedPhotoUri, 500);
                File file = new File(MyApplication.getAppContext().getCacheDir(), String.valueOf(0) + ".jpg");
                FileOutputStream outStream = null;
                try {
                    outStream = new FileOutputStream(file);
                    bitmap.compress(Bitmap.CompressFormat.JPEG, 100, outStream);
                    outStream.flush();
                    outStream.close();
                    Log.i(TAG, file.getAbsolutePath());
                } catch (FileNotFoundException e) {
                    e.printStackTrace();
                } catch (Exception e) {
                    e.printStackTrace();
                }
                rbPetImage = RequestBody.create(MediaType.parse("image/jpeg"), file);
            }

            PetService service = ServiceManager.sharedInstance().getServiceHandler().create(PetService.class);
            Call<ResponseWrapper<Pet>> call = service.postCreatePet(rbPetId, rbGender, rbMemberId, rbPetType, rbBirthDate, rbDescription, rbPetImage, rbMedicalRecord, rbLastBodyCheckDate, rbNextBodyCheckDate, rbLastHairCutDescription, rbBmiRecordWeight1, rbBmiRecordLength1, rbBmiRecordWeight2, rbBmiRecordLength2, rbBmiRecordWeight3, rbBmiRecordLength3, rbBmiRecordDate1, rbBmiRecordDate2, rbBmiRecordDate3, rbPinCardJSON);
            call.enqueue(new ResponseHandler<ResponseWrapper<Pet>>() {
                @Override
                public void onResponseParsed(Call<ResponseWrapper<Pet>> call, Response<ResponseWrapper<Pet>> response) {
//                    if (response.body() != null && response.body().data != null) {
//                        mPinCard = null;
//                        mPet = response.body().data;
//                        updateUI();
//                    }
                    if (response.body() != null && response.body().data != null){
                        Pet pet = response.body().data;
                        if (pet.id == 0){
                            pet.id = pet.pet_id;
                        }
                        mPet = pet;
                        mSelectedPhotoUri = null;
                        mPinCard = null;
                        updateData();

                        mHandler.post(new Runnable() {
                            @Override
                            public void run() {
                                mEditBmiLength.setText("");
                                mEditBmiWeight.setText("");
                            }
                        });
                    }
                    else{
                        onShowMessage(Util.getString(R.string.msg_error), Util.getString(R.string.msg_ok), null);
//                        try {
//                            onShowMessage(response.errorBody().string(), Util.getString(R.string.msg_ok), null);
//                        } catch (IOException e) {
//                            e.printStackTrace();
//                        }
                    }
                }

                @Override
                public void onFailure(Call<ResponseWrapper<Pet>> call, Throwable t) {
                    onShowMessage(Util.getString(R.string.msg_error), Util.getString(R.string.msg_ok), null);
                }
            });

            return true;
        }

        return false;
    }

    @Override
    public void configUI() {
        super.configUI();

        if (mPet == null){
            // null to create
            mPet = new Pet();
//            PinCard pinCard = new PinCard();
//            pinCard.name = "pin";
//            mPet.pin_card_list.add(pinCard);
        }


        originalTimeFormat = new SimpleDateFormat("yyyy-MM-dd HH:mm");
        targetTimeFormat = new SimpleDateFormat("dd / MM / yyyy");

        if (mCanEdit) {
            getView().findViewById(R.id.txt_delete_pet).setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    if (mPet.id > 0) {
                        onShowMessage(Util.getString(R.string.pet_delete_pet_q), Util.getString(R.string.msg_ok), Util.getString(R.string.msg_cancel), new MessageDialogFragment.MessageDialogFragmentCallback() {
                            @Override
                            public void onClickPositiveButton(MessageDialogFragment fragment, DialogInterface dialog, int id) {
                                showLoadingScreen(true, null, Util.getString(R.string.msg_loading));
                                RequestBody rbPetId = RequestBody.create(MediaType.parse("text/plain"), String.valueOf(mPet.id));
                                PetService service = ServiceManager.sharedInstance().getServiceHandler().create(PetService.class);
                                Call<Object> call = service.postDeletePet(rbPetId);
                                call.enqueue(new ResponseHandler<Object>() {
                                    @Override
                                    public void onResponseParsed(Call<Object> call, Response<Object> response) {

                                        showLoadingScreen(false, null, null);
                                        Activity activity = getActivity();
                                        if (activity != null && !activity.isFinishing()) {
                                            Util.backToActivity(activity, true);
                                        }
                                    }

                                    @Override
                                    public void onFailure(Call<Object> call, Throwable t) {

                                        showLoadingScreen(false, null, null);
                                        onHandleError(Util.getString(R.string.msg_error), null, null, null);
                                    }
                                });
                            }

                            @Override
                            public void onClickNegativeButton(MessageDialogFragment fragment, DialogInterface dialog, int id) {

                            }
                        });
                    } else {
                        Activity activity = getActivity();
                        if (activity != null && !activity.isFinishing()) {
                            Util.backToActivity(activity, true);
                        }
                    }
                }
            });
        }
        else {
            getView().findViewById(R.id.txt_delete_pet).setVisibility(View.GONE);
        }


        mImgProfile = (ImageView)getView().findViewById(R.id.img_profile);
        mImgProfileBorder = (ImageView)getView().findViewById(R.id.img_pet_border);
        if (mCanEdit) {
            mImgProfile.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    Intent intent = new Intent();
                    intent.setType("image/*");
                    intent.setAction(Intent.ACTION_GET_CONTENT);
                    startActivityForResult(Intent.createChooser(intent, getTitle()), 11);
                }
            });
        }

        mTxtName = (TextView)getView().findViewById(R.id.txt_name);
        mBtnEditName = getView().findViewById(R.id.img_edit_name);
        if (mCanEdit) {
            mBtnEditName.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    Bundle b = new Bundle();
                    b.putString(InputDialogFragment.KEY_MESSAGE, mPet.description);
                    b.putString(InputDialogFragment.KEY_MESSAGE_HINT, getString(R.string.pet_lb_name));
                    b.putString(InputDialogFragment.KEY_POSITIVE_BTN, getString(R.string.msg_ok));
                    b.putString(InputDialogFragment.KEY_NEGATIVE_BTN, getString(R.string.msg_cancel));

                    InputDialogFragment frag = InputDialogFragment.getInstance(b, new InputDialogFragment.InputDialogFragmentCallback() {
                        @Override
                        public void onClickPositiveButton(InputDialogFragment fragment, DialogInterface dialog, EditText editText, int id) {
                            String rawText = editText.getText().toString();
                            mPet.description = rawText;
                            updateUI();
                        }

                        @Override
                        public void onClickNegativeButton(InputDialogFragment fragment, DialogInterface dialog, EditText editText, int id) {

                        }
                    });
                    try {
                        frag.show(getFragmentManager(), "input_dialog");
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                }
            });
        }
        else{
            mBtnEditName.setVisibility(View.GONE);
        }

        mImgGenderGirl = (ImageView)getView().findViewById(R.id.img_gender_girl);
        mImgGenderBoy = (ImageView)getView().findViewById(R.id.img_gender_boy);
        if (mCanEdit) {
            View.OnClickListener genderOnClickListener = new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    int viewId = view.getId();
                    if (viewId == R.id.img_gender_boy) {
                        mPet.gender = Constant.GENDER_MALE;
                    } else {
                        mPet.gender = Constant.GENDER_FEMALE;
                    }
                    updateUI();
                }
            };
            mImgGenderBoy.setOnClickListener(genderOnClickListener);
            mImgGenderGirl.setOnClickListener(genderOnClickListener);
        }

        mLayoutTypeLeft = getView().findViewById(R.id.layout_type_left);
        mTxtTypeLeft = (TextView)getView().findViewById(R.id.txt_type_left);
        if (mCanEdit) {
            mLayoutTypeLeft.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {

                    List<String> options = new ArrayList<String>(Constant.getPetTypeCategory());
                    final int selection = options.indexOf(Constant.getPetTypeCategory(mPet.pet_type));
                    onHandleSelection(null, options, selection, new ListDialogFragment.ListDialogFragmentCallback() {
                        @Override
                        public void onClickPositiveButton(ListDialogFragment fragment, DialogInterface dialog, boolean[] selectionList, int id) {
                            if (selection == id)
                                return;

                            if (id == 0) {
                                mPet.pet_type = Constant.getDefaultCat();
                            } else {
                                mPet.pet_type = Constant.getDefaultDog();
                            }
                            mTxtTypeLeft.setText(Constant.getPetTypeCategory(mPet.pet_type));
                            updateUI();
                        }

                        @Override
                        public void onClickNegativeButton(ListDialogFragment fragment, DialogInterface dialog, int id) {

                        }
                    });

                }
            });
        }
        else {
            mLayoutTypeLeft.setVisibility(View.GONE);
        }

        mLayoutType = getView().findViewById(R.id.layout_type);
        mTxtType = (TextView)getView().findViewById(R.id.txt_type);
        if (mCanEdit) {
            mLayoutType.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    List<String> options = new ArrayList<String>();
                    final List<Integer> optionId = new ArrayList<Integer>();
                    final Map<Integer, String> types = new HashMap<Integer, String>(Constant.getPetTypes(mPet.pet_type));

                    int selection = 0;
                    for (int i = Constant.getPetTypeDefaultId(mPet.pet_type); i < types.size() + Constant.getPetTypeDefaultId(mPet.pet_type); i++) {
                        optionId.add(i);
                        options.add(types.get(i));
                        if (i == mPet.pet_type) {
                            selection = i - Constant.getPetTypeDefaultId(mPet.pet_type);
                        }
                    }

                    onHandleSelection(null, options, selection, new ListDialogFragment.ListDialogFragmentCallback() {
                        @Override
                        public void onClickPositiveButton(ListDialogFragment fragment, DialogInterface dialog, boolean[] selectionList, int id) {
                            mPet.pet_type = optionId.get(id);
                            mTxtType.setText(types.get(mPet.pet_type));
                            updateUI();
                        }

                        @Override
                        public void onClickNegativeButton(ListDialogFragment fragment, DialogInterface dialog, int id) {

                        }
                    });

                }
            });
        }


        mTxtBirthday = (TextView)getView().findViewById(R.id.txt_birthday);
        mBtnEditBirthday = getView().findViewById(R.id.img_edit_birthday);
        if (mCanEdit) {
            mBtnEditBirthday.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {

                    Bundle b = new Bundle();
//                b.putInt(DatePickerDialogFragment.KEY_MONTH, );
//                b.putInt(DatePickerDialogFragment.KEY_DAY, );

                    DatePickerFullDialogFragment frag = DatePickerFullDialogFragment.getInstance(b, new DatePickerDialogFragment.DatePickerDialogFragmentCallback() {
                        @Override
                        public void onDateSet(DatePickerDialogFragment fragment, DatePicker datePicker, int month, int dayOfMonth) {
                            String date = String.format("%d-%d-%d 00:00", datePicker.getYear(), datePicker.getMonth() + 1, datePicker.getDayOfMonth());
                            mPet.birth_date = date;
                            updateUI();
                        }
                    });
                    try {
                        frag.show(getFragmentManager(), "date_picker");
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                }
            });
        }
        else{
            mBtnEditBirthday.setVisibility(View.GONE);
        }


        mLayoutPinCard = (LinearLayout)getView().findViewById(R.id.layout_pin_card);
        mOnClickPinCardCell = new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                PinCard pinCard = (PinCard) view.getTag();
                if (mPinCard != null && pinCard == mPinCard){
                    mPinCard = null;
                    updateUI();
                }
                else if (pinCard == null){
                    // TODO create pin card
                    mPinCard = null;
                    updateUI();

                    PinCard newPinCard = new PinCard();
                    newPinCard.name = "";
                    newPinCard.next_pin_record_date = "";
                    newPinCard.recent_pin_record_date = "";
                    newPinCard.description = "";
                    newPinCard.pet_id = mPet.id;

                    PetDetailPinCardFragment frag = PetDetailPinCardFragment.getInstance(newPinCard, null);
                    Activity activity = getActivity();
                    if (activity != null && !activity.isFinishing()) {
                        MenuFragment.setNextContentFragment(frag);
                        Intent intent = new Intent(activity, InnerPageFragmentActivity.class);
                        Util.moveToActivity(activity, intent, true);
                    }

                }
                else{
                    mPinCard = pinCard;
                    updateUI();
                }

            }
        };
        mLayoutPinCardDetail = getView().findViewById(R.id.layout_pin_card_detail);
        mTxtPinCardLastYear = (TextView)getView().findViewById(R.id.txt_pin_card_last_year);
        mTxtPinCardLastMonth = (TextView)getView().findViewById(R.id.txt_pin_card_last_month);
        mTxtPinCardLastDay = (TextView)getView().findViewById(R.id.txt_pin_card_last_day);
        mTxtPinCardNextYear = (TextView)getView().findViewById(R.id.txt_pin_card_next_year);
        mTxtPinCardNextMonth = (TextView)getView().findViewById(R.id.txt_pin_card_next_month);
        mTxtPinCardNextDay = (TextView)getView().findViewById(R.id.txt_pin_card_next_day);
        mTxtPinCardDescription = (TextView)getView().findViewById(R.id.txt_pin_card_description);
        mBtnPinCardDetailEdit = getView().findViewById(R.id.img_edit_pin_card);
        if (mCanEdit) {
            mBtnPinCardDetailEdit.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    if (mPinCard == null)
                        return;

                    PinCard pinCard = new PinCard();
                    pinCard.name = mPinCard.name;
                    pinCard.next_pin_record_date = mPinCard.next_pin_record_date;
                    pinCard.recent_pin_record_date = mPinCard.recent_pin_record_date;
                    pinCard.description = mPinCard.description;
                    pinCard.pet_id = mPet.id;
                    pinCard.id = mPinCard.id;

                    PetDetailPinCardFragment frag = PetDetailPinCardFragment.getInstance(pinCard, null);
                    Activity activity = getActivity();
                    if (activity != null && !activity.isFinishing()) {
                        MenuFragment.setNextContentFragment(frag);
                        Intent intent = new Intent(activity, InnerPageFragmentActivity.class);
                        Util.moveToActivity(activity, intent, true);
                    }
                }
            });
        }
        else{
            mBtnPinCardDetailEdit.setVisibility(View.GONE);
        }

        mTxtLastHairCutting = (TextView) getView().findViewById(R.id.txt_last_hair_cutting);
        mBtnEditLastHairCutting = getView().findViewById(R.id.img_edit_last_hair_cutting);
        if (mCanEdit) {
            mBtnEditLastHairCutting.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    Bundle b = new Bundle();
                    b.putString(InputDialogFragment.KEY_MESSAGE, mPet.last_bath_hair_cut_description);
                    b.putString(InputDialogFragment.KEY_MESSAGE_HINT, getString(R.string.pet_lb_last_hair_cutting));
                    b.putString(InputDialogFragment.KEY_POSITIVE_BTN, getString(R.string.msg_ok));
                    b.putString(InputDialogFragment.KEY_NEGATIVE_BTN, getString(R.string.msg_cancel));

                    InputDialogFragment frag = InputDialogFragment.getInstance(b, new InputDialogFragment.InputDialogFragmentCallback() {
                        @Override
                        public void onClickPositiveButton(InputDialogFragment fragment, DialogInterface dialog, EditText editText, int id) {
                            String rawText = editText.getText().toString();
                            mPet.last_bath_hair_cut_description = rawText;
                            updateUI();
                        }

                        @Override
                        public void onClickNegativeButton(InputDialogFragment fragment, DialogInterface dialog, EditText editText, int id) {

                        }
                    });
                    try {
                        frag.show(getFragmentManager(), "input_dialog");
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                }
            });
        }
        else{
            mBtnEditLastHairCutting.setVisibility(View.GONE);
        }

        mEditBmiWeight = (EditText)getView().findViewById(R.id.edit_bmi_weight);
        mEditBmiLength = (EditText)getView().findViewById(R.id.edit_bmi_length);
        mTxtBmiResult = (TextView)getView().findViewById(R.id.txt_bmi_result);
        mImgBmiStatus = (ImageView)getView().findViewById(R.id.img_bmi_status);
        mTxtBmiStatusStr = (TextView)getView().findViewById(R.id.txt_bmi_status_str);
        if (mCanEdit) {
            TextWatcher watcher = new TextWatcher() {
                @Override
                public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {

                }

                @Override
                public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {

                }

                @Override
                public void afterTextChanged(Editable editable) {
                    updateUI();
                }
            };
            mEditBmiWeight.addTextChangedListener(watcher);
            mEditBmiLength.addTextChangedListener(watcher);
        }
        else{
            mEditBmiWeight.setEnabled(false);
            mEditBmiLength.setEnabled(false);
        }


        mTxtBmiYear1 = (TextView)getView().findViewById(R.id.txt_bmi_record_year_1);
        mTxtBmiMonth1 = (TextView)getView().findViewById(R.id.txt_bmi_record_month_1);
        mTxtBmiDay1 = (TextView)getView().findViewById(R.id.txt_bmi_record_day_1);
        mTxtBmiResult1 = (TextView)getView().findViewById(R.id.txt_bmi_record_result_1);

        mTxtBmiYear2 = (TextView)getView().findViewById(R.id.txt_bmi_record_year_2);
        mTxtBmiMonth2 = (TextView)getView().findViewById(R.id.txt_bmi_record_month_2);
        mTxtBmiDay2 = (TextView)getView().findViewById(R.id.txt_bmi_record_day_2);
        mTxtBmiResult2 = (TextView)getView().findViewById(R.id.txt_bmi_record_result_2);

        mTxtBmiYear3 = (TextView)getView().findViewById(R.id.txt_bmi_record_year_3);
        mTxtBmiMonth3 = (TextView)getView().findViewById(R.id.txt_bmi_record_month_3);
        mTxtBmiDay3 = (TextView)getView().findViewById(R.id.txt_bmi_record_day_3);
        mTxtBmiResult3 = (TextView)getView().findViewById(R.id.txt_bmi_record_result_3);


        mTxtMedicalRecord = (TextView)getView().findViewById(R.id.txt_medical_record);
        mBtnEditMedicalRecord = getView().findViewById(R.id.img_edit_medical_record);
        if (mCanEdit) {
            mBtnEditMedicalRecord.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    Bundle b = new Bundle();
                    b.putString(InputDialogFragment.KEY_MESSAGE, mPet.medical_record);
                    b.putString(InputDialogFragment.KEY_MESSAGE_HINT, getString(R.string.pet_lb_health_record));
                    b.putString(InputDialogFragment.KEY_POSITIVE_BTN, getString(R.string.msg_ok));
                    b.putString(InputDialogFragment.KEY_NEGATIVE_BTN, getString(R.string.msg_cancel));

                    InputDialogFragment frag = InputDialogFragment.getInstance(b, new InputDialogFragment.InputDialogFragmentCallback() {
                        @Override
                        public void onClickPositiveButton(InputDialogFragment fragment, DialogInterface dialog, EditText editText, int id) {
                            String rawText = editText.getText().toString();
                            mPet.medical_record = rawText;
                            updateUI();
                        }

                        @Override
                        public void onClickNegativeButton(InputDialogFragment fragment, DialogInterface dialog, EditText editText, int id) {

                        }
                    });
                    try {
                        frag.show(getFragmentManager(), "input_dialog");
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                }
            });
        }
        else{
            mBtnEditMedicalRecord.setVisibility(View.GONE);
        }


        mTxtLastHealthCheckYear = (TextView)getView().findViewById(R.id.txt_last_health_check_year);
        mTxtLastHealthCheckMonth = (TextView)getView().findViewById(R.id.txt_last_health_check_month);
        mTxtLastHealthCheckDay = (TextView)getView().findViewById(R.id.txt_last_health_check_day);
        mTxtNextHealthCheckYear = (TextView)getView().findViewById(R.id.txt_next_health_check_year);
        mTxtNextHealthCheckMonth = (TextView)getView().findViewById(R.id.txt_next_health_check_month);
        mTxtNextHealthCheckDay = (TextView)getView().findViewById(R.id.txt_next_health_check_day);

    }

    @Override
    protected void updateUIRunnable() {
        super.updateUIRunnable();

        Pet pet = mPet;

        mTxtName.setText(pet.description);

        int inactiveGenderColor =  Util.getColor(MyApplication.getAppContext(), R.color.main_text_color_lighter);
        mImgGenderGirl.setColorFilter(pet.gender == Constant.GENDER_FEMALE ? Util.getColor(MyApplication.getAppContext(), R.color.pet_girl) : inactiveGenderColor);
        mImgGenderBoy.setColorFilter(pet.gender == Constant.GENDER_MALE ? Util.getColor(MyApplication.getAppContext(), R.color.pet_boy) : inactiveGenderColor);

        mImgProfileBorder.setColorFilter(pet.gender == Constant.GENDER_FEMALE ? Util.getColor(MyApplication.getAppContext(), R.color.pet_girl) : Util.getColor(MyApplication.getAppContext(), R.color.pet_boy));
        if (mSelectedPhotoUri == null){
            Picasso.with(MyApplication.getAppContext()).load(ServiceManager.sharedInstance().getImageUrl(ServiceManager.IMAGE_URL_PET, mPet.image_id)).into(mImgProfile);
        }
        else{

            Bitmap bitmap = getBitmap(mSelectedPhotoUri, mImgProfile.getMeasuredWidth());
            if (bitmap != null){
                mImgProfile.setImageBitmap(bitmap);
            }
        }

        mTxtTypeLeft.setText(Constant.getPetTypeCategory(pet.pet_type));
        mTxtType.setText(Constant.getPetTypes(pet.pet_type).get(pet.pet_type));


        String dateStr = "";
        try {
            Date createTimeDate = originalTimeFormat.parse(pet.birth_date);
            dateStr = targetTimeFormat.format(createTimeDate);
        } catch (Exception e) {
            e.printStackTrace();
        }
        mTxtBirthday.setText(dateStr);

        mLayoutPinCard.removeAllViews();
        if (pet.pin_card_list != null){
            LayoutInflater inflater = (LayoutInflater)MyApplication.getAppContext().getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            LinearLayout rowView = null;
            for (int i = 0; i < pet.pin_card_list.size() + 1; i ++) {
                if (i % 3 == 0) {
                    if (rowView != null) {
                        mLayoutPinCard.addView(rowView, new LinearLayout.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.WRAP_CONTENT));
                        rowView = null;
                    }

                    rowView = new LinearLayout(MyApplication.getAppContext());
                    rowView.setOrientation(LinearLayout.HORIZONTAL);
                    rowView.setWeightSum(3);
//                    rowView.setBackgroundColor(Util.getColor(MyApplication.getAppContext(), R.color.theme_color));
                }

                View cellView = null;
                if (i < pet.pin_card_list.size()){
                    PinCard pinCard = pet.pin_card_list.get(i);
                    cellView = inflater.inflate(R.layout.cell_pin_card, mLayoutPinCard, false);
                    TextView txtName = (TextView) cellView.findViewById(R.id.txt_name);
                    txtName.setText(pinCard.name);
                    cellView.setTag(pinCard);

                    if (mPinCard == pinCard){
                        cellView.findViewById(R.id.img_arr).setVisibility(View.VISIBLE);
                        txtName.setTextColor(Util.getColor(MyApplication.getAppContext(), R.color.main_text_color_invert));
                        ((ImageView)cellView.findViewById(R.id.img_border)).setColorFilter(Util.getColor(MyApplication.getAppContext(), R.color.theme_color));

                        Calendar lastCalendar = Calendar.getInstance();
                        Calendar nextCalendar = Calendar.getInstance();
                        try {
                            Date lastDate = originalTimeFormat.parse(pinCard.recent_pin_record_date);
                            lastCalendar.setTime(lastDate);

                            Date nextDate = originalTimeFormat.parse(pinCard.next_pin_record_date);
                            nextCalendar.setTime(nextDate);
                        } catch (Exception e) {
                            e.printStackTrace();
                        }
                        mTxtPinCardLastYear.setText(String.valueOf(lastCalendar.get(Calendar.YEAR)));
                        mTxtPinCardLastMonth.setText(String.valueOf(lastCalendar.get(Calendar.MONTH) + 1));
                        mTxtPinCardLastDay.setText(String.valueOf(lastCalendar.get(Calendar.DAY_OF_MONTH)));
                        mTxtPinCardNextYear.setText(String.valueOf(nextCalendar.get(Calendar.YEAR)));
                        mTxtPinCardNextMonth.setText(String.valueOf(nextCalendar.get(Calendar.MONTH) + 1));
                        mTxtPinCardNextDay.setText(String.valueOf(nextCalendar.get(Calendar.DAY_OF_MONTH)));
                        mTxtPinCardDescription.setText(pinCard.description);
                    }
                }
                else{
                    cellView = inflater.inflate(R.layout.cell_pin_card_add, mLayoutPinCard, false);
                    cellView.setTag(null);
                    if (!mCanEdit){
                        cellView.setVisibility(View.INVISIBLE);
                    }
                }
                mLayoutPinCardDetail.setVisibility(mPinCard != null ? View.VISIBLE : View.GONE);
                cellView.setOnClickListener(mOnClickPinCardCell);
                rowView.addView(cellView, new LinearLayout.LayoutParams(0, ViewGroup.LayoutParams.WRAP_CONTENT, 1));
            }

            if (rowView != null) {
                mLayoutPinCard.addView(rowView, new LinearLayout.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.WRAP_CONTENT));
            }
        }

        mTxtLastHairCutting.setText(pet.last_bath_hair_cut_description);

        String bmiResult = "";
        String bmiWeightStr = mEditBmiWeight.getText().toString();
        String bmiLengthStr = mEditBmiLength.getText().toString();
        mImgBmiStatus.setVisibility(View.INVISIBLE);
        mTxtBmiStatusStr.setText("");
        if (bmiWeightStr.length() > 0 && bmiLengthStr.length() > 0) {
            try {
                float bmiWeight = Float.parseFloat(bmiWeightStr);
                float bmiLength = Float.parseFloat(bmiLengthStr);
                if (bmiWeight > 0 && bmiLength > 0) {
                    float bmi = bmiWeight / (bmiLength * bmiLength);
                    bmiResult = String.format("%.2f", bmi);
                    mImgBmiStatus.setVisibility(View.VISIBLE);
                    mImgBmiStatus.setImageResource(Constant.getBmiStatusImgRes(bmi));
                    mTxtBmiStatusStr.setText(Constant.getBmiStatusStr(bmi));
                }
            }
            catch (Exception e){
                e.printStackTrace();
            }
        }
        mTxtBmiResult.setText(bmiResult);


        String bmiRecordResult1 = "";
        if (pet.bmi_record_length_1 > 0 && pet.bmi_record_weight_1 > 0) {
            float bmi = pet.bmi_record_weight_1 / (pet.bmi_record_length_1 * pet.bmi_record_length_1);
            bmiRecordResult1 = String.format("%.2f", bmi);

            Calendar calendar = Calendar.getInstance();
            try {
                Date date = originalTimeFormat.parse(mPet.bmi_record_date_1);
                calendar.setTime(date);
            } catch (Exception e) {
                e.printStackTrace();
            }
            mTxtBmiYear1.setText(String.valueOf(calendar.get(Calendar.YEAR)));
            mTxtBmiMonth1.setText(String.valueOf(calendar.get(Calendar.MONTH) + 1));
            mTxtBmiDay1.setText(String.valueOf(calendar.get(Calendar.DAY_OF_MONTH)));
        }
        mTxtBmiResult1.setText(bmiRecordResult1);

        String bmiRecordResult2 = "";
        if (pet.bmi_record_length_2 > 0 && pet.bmi_record_weight_2 > 0) {
            float bmi = pet.bmi_record_weight_2 / (pet.bmi_record_length_2 * pet.bmi_record_length_2);
            bmiRecordResult2 = String.format("%.2f", bmi);

            Calendar calendar = Calendar.getInstance();
            try {
                Date date = originalTimeFormat.parse(mPet.bmi_record_date_2);
                calendar.setTime(date);
            } catch (Exception e) {
                e.printStackTrace();
            }
            mTxtBmiYear2.setText(String.valueOf(calendar.get(Calendar.YEAR)));
            mTxtBmiMonth2.setText(String.valueOf(calendar.get(Calendar.MONTH) + 1));
            mTxtBmiDay2.setText(String.valueOf(calendar.get(Calendar.DAY_OF_MONTH)));
        }
        mTxtBmiResult2.setText(bmiRecordResult2);

        String bmiRecordResult3 = "";
        if (pet.bmi_record_length_3 > 0 && pet.bmi_record_weight_3 > 0) {
            float bmi = pet.bmi_record_weight_3 / (pet.bmi_record_length_3 * pet.bmi_record_length_3);
            bmiRecordResult3 = String.format("%.2f", bmi);

            Calendar calendar = Calendar.getInstance();
            try {
                Date date = originalTimeFormat.parse(mPet.bmi_record_date_3);
                calendar.setTime(date);
            } catch (Exception e) {
                e.printStackTrace();
            }
            mTxtBmiYear3.setText(String.valueOf(calendar.get(Calendar.YEAR)));
            mTxtBmiMonth3.setText(String.valueOf(calendar.get(Calendar.MONTH) + 1));
            mTxtBmiDay3.setText(String.valueOf(calendar.get(Calendar.DAY_OF_MONTH)));
        }
        mTxtBmiResult3.setText(bmiRecordResult3);


        mTxtMedicalRecord.setText(mPet.medical_record);


        Calendar lastCalendar = Calendar.getInstance();
        try {
            Date lastDate = originalTimeFormat.parse(mPet.last_body_check_date);
            lastCalendar.setTime(lastDate);
            mTxtLastHealthCheckYear.setText(String.valueOf(lastCalendar.get(Calendar.YEAR)));
            mTxtLastHealthCheckMonth.setText(String.valueOf(lastCalendar.get(Calendar.MONTH) + 1));
            mTxtLastHealthCheckDay.setText(String.valueOf(lastCalendar.get(Calendar.DAY_OF_MONTH)));
        } catch (Exception e) {
            e.printStackTrace();
            mTxtLastHealthCheckYear.setText("");
            mTxtLastHealthCheckMonth.setText("");
            mTxtLastHealthCheckDay.setText("");
        }


        Calendar nextCalendar = Calendar.getInstance();
        try {
            Date lastDate = originalTimeFormat.parse(mPet.next_body_check_date);
            nextCalendar.setTime(lastDate);
            mTxtNextHealthCheckYear.setText(String.valueOf(nextCalendar.get(Calendar.YEAR)));
            mTxtNextHealthCheckMonth.setText(String.valueOf(nextCalendar.get(Calendar.MONTH) + 1));
            mTxtNextHealthCheckDay.setText(String.valueOf(nextCalendar.get(Calendar.DAY_OF_MONTH)));
        } catch (Exception e) {
            e.printStackTrace();
            mTxtNextHealthCheckYear.setText("");
            mTxtNextHealthCheckMonth.setText("");
            mTxtNextHealthCheckDay.setText("");
        }

    }

    @Override
    public void updateData() {
        super.updateData();

        if (mPet.id == 0) {
            updateUI();
            return;
        }

        showLoadingScreen(true, null, Util.getString(R.string.msg_loading));

        PetService service = ServiceManager.sharedInstance().getServiceHandler().create(PetService.class);
        Call<ResponseWrapper<Pet>> call = service.getPetDetail(mPet.id);
        call.enqueue(new ResponseHandler<ResponseWrapper<Pet>>() {
            @Override
            public void onResponseParsed(Call<ResponseWrapper<Pet>> call, Response<ResponseWrapper<Pet>> response) {
                showLoadingScreen(false, null, null);

                if (response.body() != null && response.body().data != null){
                    mPet = response.body().data;
                    updateUI();
                }
            }

            @Override
            public void onFailure(Call<ResponseWrapper<Pet>> call, Throwable t) {
                showLoadingScreen(false, null, null);
                onHandleError(Util.getString(R.string.msg_error), null, null, new MessageDialogFragment.MessageDialogFragmentCallback() {

                    @Override
                    public void onClickPositiveButton(MessageDialogFragment fragment, DialogInterface dialog, int id) {
                        updateData();
                    }

                    @Override
                    public void onClickNegativeButton(MessageDialogFragment fragment, DialogInterface dialog, int id) {

                    }
                });
            }
        });
    }


    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        if (resultCode == Activity.RESULT_OK){
            Uri selectedImageUri = data.getData();
            if (selectedImageUri != null) {
                Log.i(TAG, "selected image: " + selectedImageUri.toString());
                mSelectedPhotoUri = selectedImageUri.toString();
                updateUI();
            }
            else{
                Log.e(TAG, "failed to select image");
            }

        }
    }



    protected boolean checkDate(String fieldName, String year, String month, String day){
        if (year.length() != 4
            || (month.length() != 2 && month.length() != 1)
            || (day.length() != 2 && day.length() != 1)){
            onShowMessage(fieldName + ": " + Util.getString(R.string.error_date), Util.getString(R.string.msg_ok), null);
            return false;
        }
        return true;
    }
}
