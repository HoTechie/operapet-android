package com.hotechie.operapet.ui;

import android.app.AlertDialog;
import android.app.Dialog;
import android.content.DialogInterface;
import android.content.res.Resources;
import android.os.Bundle;
import android.support.v4.app.DialogFragment;
import android.text.InputType;
import android.util.TypedValue;
import android.view.ViewGroup;
import android.widget.EditText;
import android.widget.LinearLayout;


/**
 * Created by duncan on 15/10/2016.
 */

public class TwoInputDialogsFragment extends DialogFragment {
    private static final String TAG = "InputDialogFragment";

    public static final String KEY_TITLE = "title";
    public static final String KEY_MESSAGE1 = "message1";
    public static final String KEY_MESSAGE2 = "message2";
    public static final String KEY_MESSAGE_HINT1 = "message_hint1";
    public static final String KEY_MESSAGE_HINT2 = "message_hint2";
    public static final String KEY_MESSAGE_TYPE = "message_type";
    public static final String KEY_POSITIVE_BTN = "btn_positive";
    public static final String KEY_NEGATIVE_BTN = "btn_negative";

    public static TwoInputDialogsFragment getInstance(Bundle b, TwoInputDialogsFragment.TwoInputDialogsFragmentCallback callback){
        TwoInputDialogsFragment f = new TwoInputDialogsFragment();
        if (b != null)
            f.setArguments(b);
        f.mCallback = callback;
        return f;
    }


    public static interface TwoInputDialogsFragmentCallback {
        public void onClickPositiveButton(TwoInputDialogsFragment fragment, DialogInterface dialog, EditText editText1, EditText editText2, int id);
        public void onClickNegativeButton(TwoInputDialogsFragment fragment, DialogInterface dialog, EditText editText1, EditText editText2, int id);
    }


    private TwoInputDialogsFragment.TwoInputDialogsFragmentCallback mCallback = null;


    @Override
    public Dialog onCreateDialog(Bundle savedInstanceState) {
        Bundle infoBundle = new Bundle();
        if (getArguments() != null)
            infoBundle = getArguments();
        else if (savedInstanceState != null)
            infoBundle = savedInstanceState;

        String title = infoBundle.getString(KEY_TITLE, null);
        String message1 = infoBundle.getString(KEY_MESSAGE1, null);
        String messageHint1 = infoBundle.getString(KEY_MESSAGE_HINT1, "");
        String message2 = infoBundle.getString(KEY_MESSAGE2, null);
        String messageHint2 = infoBundle.getString(KEY_MESSAGE_HINT2, "");
        String positiveBtn = infoBundle.getString(KEY_POSITIVE_BTN, null);
        String negativeBtn = infoBundle.getString(KEY_NEGATIVE_BTN, null);
        int messageType = infoBundle.getInt(KEY_MESSAGE_TYPE, InputType.TYPE_CLASS_TEXT);

        AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
        if (title != null)
            builder.setTitle(title);


        Resources r = getResources();
        LinearLayout layout = new LinearLayout(builder.getContext());
        int paddingPx = (int)TypedValue.applyDimension(TypedValue.COMPLEX_UNIT_DIP, 10, r.getDisplayMetrics());
        layout.setOrientation(LinearLayout.VERTICAL);
        layout.setPadding(paddingPx, paddingPx, paddingPx, paddingPx);

        final EditText editText = new EditText(builder.getContext());
        editText.setMaxLines(1);
        editText.setInputType(messageType);
        if (message1 != null || messageHint1 != null) {
            editText.setText(message1 != null ? message1 : "");
            editText.setHint(messageHint1 != null ? messageHint1 : "");
            int editTextMinHeight = (int) TypedValue.applyDimension(TypedValue.COMPLEX_UNIT_DIP, 60, r.getDisplayMetrics());
            editText.setMinHeight(editTextMinHeight);
            layout.addView(editText, new LinearLayout.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.WRAP_CONTENT));
        }

        final EditText editText2 = new EditText(builder.getContext());
        if (message2 != null || messageHint2 != null) {
            editText2.setText(message2 != null ? message2 : "");
            editText2.setHint(messageHint2 != null ? messageHint2 : "");
            int editTextMinHeight = (int) TypedValue.applyDimension(TypedValue.COMPLEX_UNIT_DIP, 60, r.getDisplayMetrics());
            editText2.setMinHeight(editTextMinHeight);
            layout.addView(editText2, new LinearLayout.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.WRAP_CONTENT));
        }


        builder.setView(layout);

        if (positiveBtn != null)
            builder.setPositiveButton(positiveBtn, new DialogInterface.OnClickListener() {
                public void onClick(DialogInterface dialog, int id) {
                    if (mCallback != null)
                        mCallback.onClickPositiveButton(TwoInputDialogsFragment.this, dialog, editText, editText2, id);
                }
            });
        if (negativeBtn != null)
            builder.setNegativeButton(negativeBtn, new DialogInterface.OnClickListener() {
                public void onClick(DialogInterface dialog, int id) {
                    if (mCallback != null)
                        mCallback.onClickNegativeButton(TwoInputDialogsFragment.this, dialog, editText, editText2, id);
                }
            });

        return builder.create();
    }
}
