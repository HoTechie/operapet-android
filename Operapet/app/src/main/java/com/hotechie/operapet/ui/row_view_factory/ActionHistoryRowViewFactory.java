package com.hotechie.operapet.ui.row_view_factory;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.TextView;

import com.hotechie.operapet.MyApplication;
import com.hotechie.operapet.R;
import com.hotechie.operapet.Util;
import com.hotechie.operapet.service.model.ActionHistory;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

/**
 * Created by duncan on 21/10/2016.
 */

public class ActionHistoryRowViewFactory extends RowViewFactory {
    public static interface ActionHistoryRowViewFactoryCallback {
        public void onClick(ActionHistory actionHistory);
    }

    protected List<ActionHistory> mActionHistoryList= new ArrayList<>();
    protected ActionHistoryRowViewFactory.ActionHistoryRowViewFactoryCallback mCallback = null;

    protected SimpleDateFormat originalTimeFormat;
    protected SimpleDateFormat targetTimeFormat;


    public ActionHistoryRowViewFactory(List<ActionHistory> actionHistoryList, final ActionHistoryRowViewFactory.ActionHistoryRowViewFactoryCallback callback){
        mActionHistoryList = new ArrayList<>(actionHistoryList);
        mCallback = callback;

        originalTimeFormat = new SimpleDateFormat("yyyy-MM-dd HH:mm");
        targetTimeFormat = new SimpleDateFormat(Util.getString(R.string.post_datetime_format));
    }

    @Override
    public int getCount() {
        return mActionHistoryList.size();
    }

    @Override
    public Object getItem(int index) {
        return mActionHistoryList.get(index);
    }

    @Override
    public View getView(final Object object) {
        final ActionHistory actionHistory = (ActionHistory)object;

        int index = mActionHistoryList.indexOf(actionHistory);

        LayoutInflater inflater = (LayoutInflater) MyApplication.getAppContext().getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        View rowView = inflater.inflate(R.layout.row_credit_record, null, false);

        TextView txtCredit = (TextView)rowView.findViewById(R.id.txt_credit);
        TextView txtDescription = (TextView)rowView.findViewById(R.id.txt_description);
        TextView txtDate = (TextView)rowView.findViewById(R.id.txt_date);

        String createTimeStr = "";
        try {
            Date createTimeDate = originalTimeFormat.parse(actionHistory.create_time);
            createTimeStr = targetTimeFormat.format(createTimeDate);
        } catch (Exception e) {
            e.printStackTrace();
        }
        txtDate.setText(createTimeStr);

        if (actionHistory.credit >= 0){
            txtCredit.setTextColor(Util.getColor(MyApplication.getAppContext(), R.color.theme_color));
            txtCredit.setText("+" + String.valueOf(actionHistory.credit));
        }
        else{
            txtCredit.setTextColor(Util.getColor(MyApplication.getAppContext(), R.color.credit_record_negative_credit_color));
            txtCredit.setText(String.valueOf(actionHistory.credit));
        }

        txtDescription.setText(actionHistory.description);

        if (index == 0){
            rowView.findViewById(R.id.layout_paper_head).setVisibility(View.VISIBLE);
        }

        if (index == mActionHistoryList.size() - 1){
            rowView.findViewById(R.id.layout_paper_end).setVisibility(View.VISIBLE);
            rowView.findViewById(R.id.layout_continuous).setVisibility(View.GONE);
        }

        if (mCallback != null){
            rowView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    mCallback.onClick(actionHistory);
                }
            });
        }

        return rowView;
    }

    @Override
    public boolean modifyViewLayout(View contentView, Object object, int index) {
        return false;
    }

}
