package com.hotechie.operapet.fragment;

import android.app.Activity;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.hotechie.operapet.BaseEditFragmentActivity;
import com.hotechie.operapet.BaseFragmentActivity;
import com.hotechie.operapet.BaseTabFragmentActivity;
import com.hotechie.operapet.LoginActivity;
import com.hotechie.operapet.MyApplication;
import com.hotechie.operapet.R;
import com.hotechie.operapet.SessionManager;
import com.hotechie.operapet.Util;
import com.hotechie.operapet.service.ProfileService;
import com.hotechie.operapet.service.ResponseHandler;
import com.hotechie.operapet.service.ServiceManager;
import com.hotechie.operapet.service.model.Profile;
import com.hotechie.operapet.service.model.ResponseWrapper;
import com.hotechie.util.MessageDialogFragment;
import com.squareup.picasso.Picasso;

import java.util.ArrayList;

import retrofit2.Call;
import retrofit2.Response;

/**
 * Created by duncan on 19/7/2016.
 */
public class MenuFragment extends BaseFragment {
    private static String TAG = "MenuFragment";

    public static MenuFragment getInstance(Bundle b){
        MenuFragment frag = new MenuFragment();
        frag.setArguments(b);
        return frag;
    }

    // FIXME initial value should be null
    protected static BaseFragment currentFragment = null;

    protected Profile mProfile = null;
    protected ImageView mImgProfilePic = null;
    protected TextView mTxtName = null;
    protected View mLayoutNotificationCount = null;
    protected TextView mTxtNotificationCount = null;

    protected View mLayoutLogout = null;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_menu, container, false);
    }

    public BaseFragment getCurrentContentFragment(){
        // TODO return the selected fragment instance
        if (currentFragment == null) {
            Log.e(TAG, "Null content fragment, content fragment must be created before moving to next activity");
//            currentFragment = new BaseFragment();
        }

        return currentFragment;
    }

    /**
     * Create content fragment before moving to next activity
     * @param fragment nex content fragment
     */
    public static void setNextContentFragment(BaseFragment fragment){
        currentFragment = fragment;
    }

    public void configUI(){
        mProfile = SessionManager.sharedInstance().getProfile();
        mImgProfilePic = (ImageView)getView().findViewById(R.id.img_profile_pic);
        mTxtName = (TextView)getView().findViewById(R.id.txt_name);

        mTxtNotificationCount = (TextView)getView().findViewById(R.id.txt_notification_count);
        mLayoutNotificationCount = getView().findViewById(R.id.layout_notification_count);
        mLayoutNotificationCount.setVisibility(View.GONE);

        View.OnClickListener menuItemOnClickListener = new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                BaseFragmentActivity activity = (BaseFragmentActivity)getActivity();
                activity.toggleMenu(false);

                if (view.getTag() != null)
                    moveToPage((String)view.getTag());
            }
        };
        ArrayList<View> menuItemVies = new ArrayList<>();
        getView().findViewsWithText(menuItemVies, "menuItem", View.FIND_VIEWS_WITH_CONTENT_DESCRIPTION);
        for (View view : menuItemVies){
            view.setOnClickListener(menuItemOnClickListener);
        }

        mLayoutLogout = getView().findViewById(R.id.layout_logout);
        mLayoutLogout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                SessionManager.sharedInstance().setProfile(null);
                Activity activity = getActivity();
                if (activity != null && !activity.isFinishing()) {
                    Intent newIntent = new Intent(activity, LoginActivity.class);
                    Util.moveToActivity(activity, newIntent, false);
                }
            }
        });

        getView().findViewById(R.id.layout_wall).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                WallFragment frag = WallFragment.getInstance(mProfile.id);
                setNextContentFragment(frag);
                Activity activity = getActivity();
                if (activity != null && !activity.isFinishing()) {
                    Intent newIntent = new Intent(activity, BaseTabFragmentActivity.class);
                    Util.moveToActivity(activity, newIntent, false);
                }
            }
        });

        updateUI();
        updateData();
    }

    public void moveToPage(String pageTitle){
        if (pageTitle == null)
            return;

        // TODO implement menu items actions
        if (pageTitle.equals(Util.getString(R.string.title_profile))) {
            // TODO create fragment and move to activity
            BaseFragment frag = ProfileFragment.getInstance(null);
            setNextContentFragment(frag);
            Intent newIntent = new Intent(getActivity(), BaseEditFragmentActivity.class);
            newIntent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
            Util.moveToActivity(getActivity(), newIntent, false);
        }
        else if (pageTitle.equals(Util.getString(R.string.title_posts))){
            // TODO create fragment and move to activity
            BaseFragment frag = PostListFragment.getInstance(null);
            setNextContentFragment(frag);
            Intent newIntent = new Intent(getActivity(), BaseTabFragmentActivity.class);
            newIntent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
            Util.moveToActivity(getActivity(), newIntent, false);
        }
        else if (pageTitle.equals(Util.getString(R.string.title_gift))){
            // TODO create fragment and move to activity
            GiftListFragment frag = GiftListFragment.getInstance();
            setNextContentFragment(frag);
            Intent newIntent = new Intent(getActivity(), BaseFragmentActivity.class);
            newIntent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
            Util.moveToActivity(getActivity(), newIntent, false);
        }
        else if (pageTitle.equals(Util.getString(R.string.title_pet))){
            // TODO create fragment and move to activity
            Profile profile = SessionManager.sharedInstance().getProfile();
            PetListFragment frag = PetListFragment.getInstance(profile.id, true);
            setNextContentFragment(frag);
            Intent newIntent = new Intent(getActivity(), BaseFragmentActivity.class);
            newIntent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
            Util.moveToActivity(getActivity(), newIntent, false);
        }
        else if (pageTitle.equals(Util.getString(R.string.title_invite_friend))){
            // TODO create fragment and move to activity
            InviteFriendFragment frag = InviteFriendFragment.getInstance();
            setNextContentFragment(frag);
            Intent newIntent = new Intent(getActivity(), BaseFragmentActivity.class);
            newIntent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
            Util.moveToActivity(getActivity(), newIntent, false);
        }
        else if (pageTitle.equals(Util.getString(R.string.title_about))){
            // TODO create fragment and move to activity
            AboutFragment frag = AboutFragment.getInstance();
            setNextContentFragment(frag);
            Intent newIntent = new Intent(getActivity(), BaseFragmentActivity.class);
            newIntent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
            Util.moveToActivity(getActivity(), newIntent, false);
        }
        else if (pageTitle.equals(Util.getString(R.string.title_notification))) {
            // TODO create fragment and move to activity
            Profile profile = SessionManager.sharedInstance().getProfile();
            NotificationListFragment frag = NotificationListFragment.getInstance();
            setNextContentFragment(frag);
            Intent newIntent = new Intent(getActivity(), BaseFragmentActivity.class);
            newIntent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
            Util.moveToActivity(getActivity(), newIntent, false);
        }
    }

    public void updateUIRunnable(){
        if (mProfile == null){
            mImgProfilePic.setImageDrawable(null);
            mTxtName.setText("");
        }
        else{
            Picasso.with(MyApplication.getAppContext()).load(ServiceManager.sharedInstance().getImageUrl(ServiceManager.IMAGE_URL_PROFILE, mProfile.image_id)).into(mImgProfilePic);
            mTxtName.setText(mProfile.user_name);
        }
    }

    public void updateData(){
        ProfileService service = ServiceManager.sharedInstance().getServiceHandler().create(ProfileService.class);
        Call<ResponseWrapper<Profile>> call = service.getProfile();
        call.enqueue(new ResponseHandler<ResponseWrapper<Profile>>() {

            @Override
            public void onResponseParsed(Call<ResponseWrapper<Profile>> call, Response<ResponseWrapper<Profile>> response) {

                showLoadingScreen(false, "", Util.getString(R.string.msg_loading));

                if (response.body() != null && response.body().data != null) {
                    mProfile = response.body().data;
                    SessionManager.sharedInstance().setProfile(mProfile);
                    updateUI();
                }
            }

            @Override
            public void onFailure(Call<ResponseWrapper<Profile>> call, Throwable t) {
                showLoadingScreen(false, "", Util.getString(R.string.msg_loading));
                onHandleError(Util.getString(R.string.msg_error), null, null, new MessageDialogFragment.MessageDialogFragmentCallback() {

                    @Override
                    public void onClickPositiveButton(MessageDialogFragment fragment, DialogInterface dialog, int id) {
                        updateData();
                    }

                    @Override
                    public void onClickNegativeButton(MessageDialogFragment fragment, DialogInterface dialog, int id) {

                    }
                });
            }
        });
    }
}
