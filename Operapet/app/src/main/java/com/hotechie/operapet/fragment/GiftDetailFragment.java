package com.hotechie.operapet.fragment;

import android.app.Activity;
import android.content.DialogInterface;
import android.media.Image;
import android.os.Bundle;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.hotechie.operapet.R;
import com.hotechie.operapet.Util;
import com.hotechie.operapet.service.GiftService;
import com.hotechie.operapet.service.ResponseHandler;
import com.hotechie.operapet.service.ServiceManager;
import com.hotechie.operapet.service.model.Gift;
import com.hotechie.operapet.service.model.ResponseWrapper;
import com.hotechie.operapet.ui.media_slideshow.MediaSlideshowFragment;
import com.hotechie.operapet.ui.media_slideshow.MediaSlideshowGiftImageViewModel;
import com.hotechie.operapet.ui.media_slideshow.MediaSlideshowViewModel;
import com.hotechie.util.MessageDialogFragment;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import okhttp3.MediaType;
import okhttp3.RequestBody;
import retrofit2.Call;
import retrofit2.Response;

/**
 * Created by duncan on 18/10/2016.
 */

public class GiftDetailFragment extends BaseFragment {
    private static String TAG = "GiftDetailFragment";

    public static GiftDetailFragment getInstance(Gift gift){
        GiftDetailFragment frag = new GiftDetailFragment();
        frag.mGift = gift;
        return frag;
    }

    protected Gift mGift = null;

    protected SimpleDateFormat originalTimeFormat;
    protected SimpleDateFormat targetTimeFormat;

    protected MediaSlideshowFragment mSlideshowFragment = null;

    protected TextView mTxtCredit = null;
    protected TextView mTxtName = null;
    protected ViewGroup mLayoutSlideshow = null;
    protected TextView mTxtDate = null;
    protected TextView mTxtDescription = null;

    protected View mTxtRedeem = null;

    @Override
    public void onResume() {
        super.onResume();

        updateUI();
    }

    @Override
    public String getTitle() {
        return Util.getString(R.string.title_gift_detail);
    }

    @Override
    public int layoutRes() {
        return R.layout.fragment_gift_detail;
    }

    @Override
    public void configUI() {
        super.configUI();

        originalTimeFormat = new SimpleDateFormat("yyyy-MM-dd HH:mm");
        targetTimeFormat = new SimpleDateFormat(Util.getString(R.string.post_datetime_format));

        mTxtCredit = (TextView)getView().findViewById(R.id.txt_credit);
        mTxtName = (TextView)getView().findViewById(R.id.txt_name);
        mTxtDate = (TextView)getView().findViewById(R.id.txt_date);
        mTxtDescription = (TextView)getView().findViewById(R.id.txt_description);
        mLayoutSlideshow = (ViewGroup)getView().findViewById(R.id.layout_slideshow);

        mTxtRedeem = getView().findViewById(R.id.txt_redeem);
        mTxtRedeem.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                RequestBody rbGiftId = RequestBody.create(MediaType.parse("text/plain"), String.valueOf(mGift.id));

                GiftService service = ServiceManager.sharedInstance().getServiceHandler().create(GiftService.class);
                Call<ResponseWrapper<Gift>> call = service.postRedeemGift(rbGiftId);
                call.enqueue(new ResponseHandler<ResponseWrapper<Gift>>() {
                    @Override
                    public void onResponseParsed(Call<ResponseWrapper<Gift>> call, Response<ResponseWrapper<Gift>> response) {
                        if (response.body() != null){
                            onShowMessage(Util.getString(R.string.gift_redeem_success), Util.getString(R.string.msg_ok), new MessageDialogFragment.MessageDialogFragmentCallback() {
                                @Override
                                public void onClickPositiveButton(MessageDialogFragment fragment, DialogInterface dialog, int id) {

                                    Activity activity = getActivity();
                                    if (activity != null && !activity.isFinishing()){
                                        Util.backToActivity(activity, true);
                                    }
                                }

                                @Override
                                public void onClickNegativeButton(MessageDialogFragment fragment, DialogInterface dialog, int id) {

                                    Activity activity = getActivity();
                                    if (activity != null && !activity.isFinishing()){
                                        Util.backToActivity(activity, true);
                                    }
                                }
                            });
                        }
                        else{
                            onShowMessage(Util.getString(R.string.msg_error), Util.getString(R.string.msg_ok), null);
                        }
                    }

                    @Override
                    public void onFailure(Call<ResponseWrapper<Gift>> call, Throwable t) {
                        onShowMessage(Util.getString(R.string.msg_error), Util.getString(R.string.msg_ok), null);
                    }
                });
            }
        });
    }

    @Override
    protected void updateUIRunnable() {
        super.updateUIRunnable();

        Gift gift = mGift;

        List<Integer> imageIds = gift.images;
        List<MediaSlideshowViewModel> slideshowViewModels = new ArrayList<>();

        for (Integer imageId : imageIds){
            slideshowViewModels.add(new MediaSlideshowGiftImageViewModel(ServiceManager.sharedInstance().getImageUrl(ServiceManager.IMAGE_URL_GIFT, imageId)));
        }
        if (slideshowViewModels.size() > 0){
            Bundle b = new Bundle();
            b.putBoolean(MediaSlideshowFragment.KEY_IS_SHOW_INDICATOR, true);
            b.putInt(MediaSlideshowFragment.KEY_INDICATOR_FILL_RES, R.drawable.icon_slideshow_indicator_fill);
            b.putInt(MediaSlideshowFragment.KEY_INDICATOR_UNFILL_RES, R.drawable.icon_slideshow_indicator_unfill);
            mSlideshowFragment = MediaSlideshowFragment.getInstance(b, slideshowViewModels);
            replaceContentFragment(R.id.layout_slideshow, mSlideshowFragment);
        }

        mTxtCredit.setText(String.valueOf(gift.credit));
        mTxtName.setText(gift.name);

        String createTimeStr = "";
        try {
            Date createTimeDate = originalTimeFormat.parse(gift.create_time);
            createTimeStr = targetTimeFormat.format(createTimeDate);
        } catch (ParseException e) {
            e.printStackTrace();
        }
        mTxtDate.setText(createTimeStr);

        mTxtDescription.setText(gift.description);
    }

    @Override
    public void updateData() {
        super.updateData();

        GiftService service = ServiceManager.sharedInstance().getServiceHandler().create(GiftService.class);
        Call<ResponseWrapper<Gift>> call = service.getGifts(mGift.id);
        call.enqueue(new ResponseHandler<ResponseWrapper<Gift>>() {
            @Override
            public void onResponseParsed(Call<ResponseWrapper<Gift>> call, Response<ResponseWrapper<Gift>> response) {
                if (response.body() != null && response.body().data != null){
                    mGift = response.body().data;
                    updateUI();
                }
            }

            @Override
            public void onFailure(Call<ResponseWrapper<Gift>> call, Throwable t) {
                onShowMessage(Util.getString(R.string.msg_error), Util.getString(R.string.msg_ok), null);
            }
        });
    }
}
