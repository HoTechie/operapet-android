package com.hotechie.operapet.fragment;

import android.app.Activity;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Handler;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.hotechie.operapet.Constant;
import com.hotechie.operapet.InnerPageFragmentActivity;
import com.hotechie.operapet.MyApplication;
import com.hotechie.operapet.R;
import com.hotechie.operapet.Util;
import com.hotechie.operapet.service.PostsService;
import com.hotechie.operapet.service.ResponseHandler;
import com.hotechie.operapet.service.ServiceManager;
import com.hotechie.operapet.service.model.BaseModel;
import com.hotechie.operapet.service.model.Comment;
import com.hotechie.operapet.service.model.Post;
import com.hotechie.operapet.service.model.ResponseWrapper;
import com.hotechie.operapet.service.model.Tag;
import com.hotechie.operapet.ui.row_view_factory.PostCommentRowViewFactory;
import com.hotechie.operapet.ui.row_view_factory.RowViewFactoryListAdapter;
import com.hotechie.util.MessageDialogFragment;
import com.squareup.picasso.Picasso;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import okhttp3.MediaType;
import okhttp3.RequestBody;
import retrofit2.Call;
import retrofit2.Response;

/**
 * Created by duncan on 30/9/2016.
 */

public class PostDetailFragment extends BaseListFragment {
    private static String TAG = "PostDetailFragment";


    public static PostDetailFragment getInstance(Post post){
        PostDetailFragment frag = new PostDetailFragment();
        frag.mPost = post;
        frag.mPostId = post.id;
        return frag;
    }

    public static PostDetailFragment getInstance(int postId){
        PostDetailFragment frag = new PostDetailFragment();
        frag.mPostId = postId;
        return frag;
    }

    protected int mPostId = 0;
    protected Post mPost = null;
    protected List<Comment> mComments = new ArrayList<>();
    protected int mSlideshowIndex = 0;

    protected PostCommentRowViewFactory mFactory = null;
    protected PostCommentRowViewFactory.PostCommentRowViewFactoryCallback mFactoryCallback = null;

    protected EditText mEditComment = null;
    protected View mViewSend = null;

    protected View mPostView = null;
    protected ImageView mImgProfile = null;
    protected TextView mTxtProfileName, mTxtProfileLevel, mTxtDate, mTxtDescription;
    protected LinearLayout mLayoutTag;
    protected View mLayoutPostImg, mBtnImgNext, mBtnImgPrev;
    protected ImageView mImgPost;
    protected ImageButton mBtnLike, mBtnShare;
    protected TextView mTxtLikeCount, mTxtCommentCount;

    protected Runnable mSlideshowRunnable = null;
    protected Handler mHandler = new Handler();

    @Override
    public int layoutRes() {
        return R.layout.fragment_post_detail;
    }

    @Override
    public void onStart() {
        super.onStart();
        updateData();
    }

    @Override
    public void configUI() {
        super.configUI();

        mEditComment = (EditText)getView().findViewById(R.id.edit_comment);
        mViewSend = getView().findViewById(R.id.txt_send);
        mViewSend.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                String comment = mEditComment.getText().toString();
                if (comment.length() > 0){
                    RequestBody rbPostId = RequestBody.create(MediaType.parse("text/plain"), String.valueOf(mPost.id));
                    RequestBody rbComment = RequestBody.create(MediaType.parse("text/plain"), comment);

                    PostsService service = ServiceManager.sharedInstance().getServiceHandler().create(PostsService.class);
                    Call<ResponseWrapper<Comment>> call = service.postCreateComment(rbComment, rbPostId);
                    call.enqueue(new ResponseHandler<ResponseWrapper<Comment>>() {
                        @Override
                        public void onResponseParsed(Call<ResponseWrapper<Comment>> call, Response<ResponseWrapper<Comment>> response) {
                            mHandler.post(new Runnable() {
                                @Override
                                public void run() {
                                    mEditComment.setText("");
                                    mEditComment.clearFocus();
                                }
                            });
                            updateData();
                        }

                        @Override
                        public void onFailure(Call<ResponseWrapper<Comment>> call, Throwable t) {
                            updateData();
                        }
                    });
                }
            }
        });


        LayoutInflater inflater = (LayoutInflater) MyApplication.getAppContext().getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        View postView = inflater.inflate(R.layout.view_post_detail, null, false);
        mPostView = postView;
        mImgProfile = (ImageView)postView.findViewById(R.id.img_profile);
        mTxtProfileName = (TextView)postView.findViewById(R.id.txt_profile_name);
        mTxtProfileLevel = (TextView)postView.findViewById(R.id.txt_profile_level);
        mTxtDate = (TextView)postView.findViewById(R.id.txt_date);
        mTxtDescription = (TextView)postView.findViewById(R.id.txt_description);
        mLayoutTag = (LinearLayout)postView.findViewById(R.id.layout_tag);
        mLayoutPostImg = postView.findViewById(R.id.layout_post_img);
        mBtnImgNext = postView.findViewById(R.id.btn_img_next);
        mBtnImgPrev = postView.findViewById(R.id.btn_img_prev);
        mImgPost = (ImageView)postView.findViewById(R.id.img_post);
        mBtnLike = (ImageButton)postView.findViewById(R.id.btn_like);
        mBtnShare = (ImageButton)postView.findViewById(R.id.btn_share);
        mTxtLikeCount = (TextView)postView.findViewById(R.id.txt_like);
        mTxtCommentCount = (TextView)postView.findViewById(R.id.txt_comment);



        mBtnLike.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                RequestBody rbPostId = RequestBody.create(MediaType.parse("text/plain"), String.valueOf(mPost.id));

                PostsService service = ServiceManager.sharedInstance().getServiceHandler().create(PostsService.class);
                Call<ResponseWrapper<BaseModel>> call = service.postCreateLike(rbPostId);
                call.enqueue(new ResponseHandler<ResponseWrapper<BaseModel>>() {
                    @Override
                    public void onResponseParsed(Call<ResponseWrapper<BaseModel>> call, Response<ResponseWrapper<BaseModel>> response) {
                        updateData();
                    }

                    @Override
                    public void onFailure(Call<ResponseWrapper<BaseModel>> call, Throwable t) {
                        updateData();
                    }
                });
            }
        });


        mBtnShare.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                RequestBody rbPostId = RequestBody.create(MediaType.parse("text/plain"), String.valueOf(mPost.id));

                PostsService service = ServiceManager.sharedInstance().getServiceHandler().create(PostsService.class);
                Call<ResponseWrapper<BaseModel>> call = service.postCreateShare(rbPostId);
                call.enqueue(new ResponseHandler<ResponseWrapper<BaseModel>>() {
                    @Override
                    public void onResponseParsed(Call<ResponseWrapper<BaseModel>> call, Response<ResponseWrapper<BaseModel>> response) {
                        onShowMessage(Util.getString(R.string.post_share_success), Util.getString(R.string.msg_ok), null);

                        updateData();
                    }

                    @Override
                    public void onFailure(Call<ResponseWrapper<BaseModel>> call, Throwable t) {
                        updateData();
                    }
                });
            }
        });

        mTxtLikeCount.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (mPost != null && mPost.no_of_like > 0){
                    PostLikeUserListFragment frag = PostLikeUserListFragment.getInstance(mPost.id);
                    MenuFragment.setNextContentFragment(frag);
                    Activity activity = getActivity();
                    if (activity != null && !activity.isFinishing()) {
                        Intent newIntent = new Intent(activity, InnerPageFragmentActivity.class);
                        Util.moveToActivity(activity, newIntent, true);
                    }
                }
            }
        });


        mFactoryCallback = new PostCommentRowViewFactory.PostCommentRowViewFactoryCallback() {
            @Override
            public void onClickLike(Comment comment) {
                RequestBody rbCommentId = RequestBody.create(MediaType.parse("text/plain"), String.valueOf(comment.id));

                PostsService service = ServiceManager.sharedInstance().getServiceHandler().create(PostsService.class);
                Call<ResponseWrapper<BaseModel>> call = service.postCreateCommentLike(rbCommentId);
                call.enqueue(new ResponseHandler<ResponseWrapper<BaseModel>>() {
                    @Override
                    public void onResponseParsed(Call<ResponseWrapper<BaseModel>> call, Response<ResponseWrapper<BaseModel>> response) {
                        updateData();
                    }

                    @Override
                    public void onFailure(Call<ResponseWrapper<BaseModel>> call, Throwable t) {
                        updateData();
                    }
                });
            }
        };
        mFactory = new PostCommentRowViewFactory(mComments, mPostView, mFactoryCallback);
        mAdapter = new RowViewFactoryListAdapter(mFactory);
        mListView.setAdapter(mAdapter);

        mSlideshowRunnable = new Runnable() {
            @Override
            public void run() {
                if (mSlideshowIndex >= mPost.images.size()){
                    mSlideshowIndex = mPost.images.size() - 1;
                }
                else if (mSlideshowIndex < 0){
                    mSlideshowIndex = 0;
                }

                if (mSlideshowIndex == 0){
                    mBtnImgPrev.setEnabled(false);
                }
                else {
                    mBtnImgPrev.setEnabled(true);
                }

                if (mSlideshowIndex == mPost.images.size() - 1){
                    mBtnImgNext.setEnabled(false);
                }
                else{
                    mBtnImgNext.setEnabled(true);
                }

                mLayoutPostImg.setVisibility(View.VISIBLE);
                Picasso.with(MyApplication.getAppContext()).load(ServiceManager.sharedInstance().getImageUrl(ServiceManager.IMAGE_URL_POST, mPost.images.get(mSlideshowIndex))).into(mImgPost);
            }
        };
        mBtnImgNext.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                mSlideshowIndex ++;
                mHandler.post(mSlideshowRunnable);
            }
        });
        mBtnImgPrev.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                mSlideshowIndex --;
                mHandler.post(mSlideshowRunnable);
            }
        });

        updateUI();
    }

    @Override
    protected void updateUIRunnable() {
        super.updateUIRunnable();

        Post post = mPost;

        if (post == null)
            return;

        String createTimeStr = "";
        SimpleDateFormat originalTimeFormat = new SimpleDateFormat("yyyy-MM-dd HH:mm");
        SimpleDateFormat targetTimeFormat = new SimpleDateFormat(Util.getString(R.string.post_datetime_format));
        try {
            Date createTimeDate = originalTimeFormat.parse(post.create_time);
            createTimeStr = targetTimeFormat.format(createTimeDate);
        } catch (ParseException e) {
            e.printStackTrace();
        }

        mTxtProfileName.setText(post.profile.user_name);
        mTxtProfileLevel.setText(Constant.getMemberLevelStr(post.profile.member_level));
        mTxtDate.setText(createTimeStr);
        mTxtDescription.setText(post.description);

        Picasso.with(MyApplication.getAppContext()).load(ServiceManager.sharedInstance().getImageUrl(ServiceManager.IMAGE_URL_PROFILE, post.profile.image_id)).into(mImgProfile);

        mLayoutTag.removeAllViews();
        LayoutInflater inflater = (LayoutInflater) MyApplication.getAppContext().getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        View.OnClickListener tagOnClickListener = new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Tag tag = (Tag)view.getTag();
                if (tag != null){
                    PostByTagListFragment frag = PostByTagListFragment.getInstance(tag);
                    MenuFragment.setNextContentFragment(frag);
                    Activity activity = getActivity();
                    if (activity != null && !activity.isFinishing()) {
                        Intent intent = new Intent(activity, InnerPageFragmentActivity.class);
                        Util.moveToActivity(activity, intent, true);
                    }
                }
            }
        };
        for (int i = 0; i < post.tags.size(); i ++){
            Tag tag = post.tags.get(i);
            View view = inflater.inflate(R.layout.cell_tag, null, false);
            ((TextView)view.findViewById(R.id.txt_tag)).setText(tag.description);
            view.setTag(tag);
            mLayoutTag.addView(view);

            view.setOnClickListener(tagOnClickListener);
        }

        if (post.images == null || post.images.size() == 0){
            mLayoutPostImg.setVisibility(View.GONE);
        }
        else {
            mLayoutPostImg.setVisibility(View.VISIBLE);
//            Picasso.with(MyApplication.getAppContext()).load(ServiceManager.sharedInstance().getImageUrl(ServiceManager.IMAGE_URL_POST, post.images.get(mSlideshowIndex))).into(mImgPost);
            // TODO slideshow
            mSlideshowRunnable.run();
        }

//        mBtnLike.setSelected(post.is_like);
        mBtnLike.setColorFilter(Util.getColor(MyApplication.getAppContext(), post.is_like ? R.color.theme_color : R.color.main_text_color_lighter));
        mTxtLikeCount.setText(String.format(Util.getString(R.string.post_like_format), post.no_of_like));
        mTxtCommentCount.setText(String.format(Util.getString(R.string.post_comment_format), post.no_of_comment));


        mAdapter.notifyDataSetChanged();

    }

    @Override
    public void updateData() {
        super.updateData();

        showLoadingScreen(true, "", Util.getString(R.string.msg_loading));
        PostsService service = ServiceManager.sharedInstance().getServiceHandler().create(PostsService.class);
        Call<ResponseWrapper<Post>> call = service.getPostDetail(mPostId);
        call.enqueue(new ResponseHandler<ResponseWrapper<Post>>() {

            @Override
            public void onResponseParsed(Call<ResponseWrapper<Post>> call, Response<ResponseWrapper<Post>> response) {

                showLoadingScreen(false, "", Util.getString(R.string.msg_loading));

                if (response.body() != null && response.body().data != null) {
                    mPost = response.body().data;
                    mComments = new ArrayList<Comment>(mPost.cms);
                    mFactory = new PostCommentRowViewFactory(mComments, mPostView, mFactoryCallback);
                    mAdapter.setFactory(mFactory);
                    updateUI();
                }
            }

            @Override
            public void onFailure(Call<ResponseWrapper<Post>> call, Throwable t) {
                showLoadingScreen(false, "", Util.getString(R.string.msg_loading));
                onHandleError(Util.getString(R.string.msg_error), null, null, new MessageDialogFragment.MessageDialogFragmentCallback() {

                    @Override
                    public void onClickPositiveButton(MessageDialogFragment fragment, DialogInterface dialog, int id) {
                        updateData();
                    }

                    @Override
                    public void onClickNegativeButton(MessageDialogFragment fragment, DialogInterface dialog, int id) {

                    }
                });
            }
        });
    }
}
