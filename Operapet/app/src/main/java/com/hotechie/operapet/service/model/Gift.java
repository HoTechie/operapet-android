package com.hotechie.operapet.service.model;

import java.util.List;

/**
 * Created by duncan on 4/10/2016.
 */

public class Gift extends BaseModel {
    /*
    {
        "id": 245,
        "name": "asd",
        "description": "",
        "credit": 0,
        "create_time": "30/9/2016 1:00:00",
        "images": [
            38
        ],
        status: 1
    }
     */

    public int id;
    public String name;
    public String description;
    public int credit;
    public String create_time;
    public List<Integer> images;

    public int status = 1;
    public int stock;
}
