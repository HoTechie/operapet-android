package com.hotechie.operapet.service.model;

import java.util.List;

/**
 * Created by duncan on 17/8/2016.
 */
public class Post extends BaseModel {
    public int id;
    public int no_of_like;
    public int no_of_comment;
    public String description;
    public boolean is_like;
    public String create_time;
    public int category_id;
    public String category_name;
    public Profile profile;
    public List<Tag> tags;
    public List<Integer> images;
    public List<Comment> cms;

}

/*
{
    "id": 1,
    "no_of_like": 1,
    "no_of_comment": 2,
    "description": "alan testing",
    "is_like": false,
    "create_time": "2016-09-09 04:17",
    "category_id": 1,
    "category_name": "Post Category 1",
    "profile": {
        "id": 1,
        "user_name": "Nick Tai Man Chan",
        "image_id": 0,
        "mobile": "91235201",
        "email": "achantaiman@palmary.com.hk",
        "member_level": 0,
        "credit_total": 0,
        "remain_credit": 0,
        "no_of_share": 0,
        "no_of_post": 2
    },
    "tags": [],
    "images": null
}
 */