package com.hotechie.operapet.fragment;


import android.app.Activity;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Bitmap;
import android.net.Uri;
import android.os.Bundle;
import android.app.Fragment;
import android.util.Log;
import android.view.View;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;

import com.hotechie.operapet.Constant;
import com.hotechie.operapet.GcmManager;
import com.hotechie.operapet.InnerPageFragmentActivity;
import com.hotechie.operapet.MyApplication;
import com.hotechie.operapet.R;
import com.hotechie.operapet.Util;
import com.hotechie.operapet.service.MemberService;
import com.hotechie.operapet.service.PetService;
import com.hotechie.operapet.service.ProfileService;
import com.hotechie.operapet.service.ResponseHandler;
import com.hotechie.operapet.service.ServiceManager;
import com.hotechie.operapet.service.model.Pet;
import com.hotechie.operapet.service.model.PinCard;
import com.hotechie.operapet.service.model.Profile;
import com.hotechie.operapet.service.model.ResponseWrapper;
import com.hotechie.operapet.ui.TwoInputDialogsFragment;
import com.hotechie.util.InputDialogFragment;
import com.hotechie.util.MessageDialogFragment;
import com.squareup.picasso.Picasso;

import org.json.JSONArray;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;

import okhttp3.MediaType;
import okhttp3.RequestBody;
import retrofit2.Call;
import retrofit2.Response;

/**
 * A simple {@link Fragment} subclass.
 */
public class ProfileFragment extends BaseFragment {
    private static String TAG = "ProfileFragment";

    public static ProfileFragment getInstance(Bundle b){
        ProfileFragment frag = new ProfileFragment();
        frag.setArguments(b);
        return frag;
    }

    protected Profile mProfile = null;
    protected String mSelectedPhotoUri = null;

    protected ImageView mImgProfile = null;
    protected TextView mTxtName = null;
    protected TextView mTxtPhone = null;
    protected TextView mTxtEmail = null;
    protected TextView mTxtMemberLevel = null;
    protected TextView mTxtCreditTotal = null;
    protected TextView mTxtCreditUsable = null;
    protected ImageView mImgEditName = null;
    protected ImageView mImgEditEmail = null;
    protected ImageView mImgQuestion= null;

    protected View mTxtGiftRecord = null;
    protected View mTxtActionHistory = null;

    public ProfileFragment() {
        // Required empty public constructor
    }


    @Override
    public String getTitle() {
        return Util.getString(R.string.title_profile);
    }

    @Override
    public int layoutRes() {
        return R.layout.fragment_profile;
    }

    @Override
    public void configUI() {
        super.configUI();

        mImgProfile = (ImageView)getView().findViewById(R.id.img_profile);
        mImgEditName = (ImageView)getView().findViewById(R.id.img_edit_name);
        mImgEditEmail = (ImageView)getView().findViewById(R.id.img_edit_email);
        mImgQuestion = (ImageView)getView().findViewById(R.id.img_question);
        mTxtName = (TextView)getView().findViewById(R.id.txt_name);
        mTxtPhone = (TextView)getView().findViewById(R.id.txt_phone);
        mTxtEmail = (TextView)getView().findViewById(R.id.txt_email);
        mTxtMemberLevel = (TextView)getView().findViewById(R.id.txt_member_level);
        mTxtCreditTotal = (TextView)getView().findViewById(R.id.txt_credit_total);
        mTxtCreditUsable = (TextView)getView().findViewById(R.id.txt_credit_usable);

        mImgQuestion.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (mProfile == null)
                    return;

                CreditFragment frag = CreditFragment.getInstance(mProfile);
                MenuFragment.setNextContentFragment(frag);
                Activity activity = getActivity();
                if (activity != null && !activity.isFinishing()){
                    Intent intent = new Intent(activity, InnerPageFragmentActivity.class);
                    Util.moveToActivity(activity, intent, true);
                }
            }
        });

        mImgProfile.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent();
                intent.setType("image/*");
                intent.setAction(Intent.ACTION_GET_CONTENT);
                startActivityForResult(Intent.createChooser(intent, getTitle()), 11);
            }
        });

        mImgEditName.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                // FIXME
                Bundle b = new Bundle();
                b.putString(TwoInputDialogsFragment.KEY_MESSAGE1, mProfile.firstname);
                b.putString(TwoInputDialogsFragment.KEY_MESSAGE_HINT1, getString(R.string.profile_lb_firstname));
                b.putString(TwoInputDialogsFragment.KEY_MESSAGE2, mProfile.lastname);
                b.putString(TwoInputDialogsFragment.KEY_MESSAGE_HINT2, getString(R.string.profile_lb_lastname));
                b.putString(TwoInputDialogsFragment.KEY_POSITIVE_BTN, getString(R.string.msg_ok));
                b.putString(TwoInputDialogsFragment.KEY_NEGATIVE_BTN, getString(R.string.msg_cancel));

                TwoInputDialogsFragment frag = TwoInputDialogsFragment.getInstance(b, new TwoInputDialogsFragment.TwoInputDialogsFragmentCallback() {
                    @Override
                    public void onClickPositiveButton(TwoInputDialogsFragment fragment, DialogInterface dialog, EditText editText, EditText editText2, int id) {
                        String rawText1 = editText.getText().toString();
                        String rawText2 = editText2.getText().toString();

                        mProfile.user_name = rawText1 + " " + rawText2;
                        mProfile.firstname = rawText1;
                        mProfile.lastname = rawText2;
                        updateUI();
                    }

                    @Override
                    public void onClickNegativeButton(TwoInputDialogsFragment fragment, DialogInterface dialog, EditText editText, EditText editText2, int id) {

                    }
                });
                try {
                    frag.show(getFragmentManager(), "input_dialog");
                }
                catch (Exception e){
                    e.printStackTrace();
                }
            }
        });

        mImgEditEmail.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Bundle b = new Bundle();
                b.putString(InputDialogFragment.KEY_MESSAGE, mProfile.email);
                b.putString(InputDialogFragment.KEY_MESSAGE_HINT, getString(R.string.profile_lb_email));
                b.putString(InputDialogFragment.KEY_POSITIVE_BTN, getString(R.string.msg_ok));
                b.putString(InputDialogFragment.KEY_NEGATIVE_BTN, getString(R.string.msg_cancel));

                InputDialogFragment frag = InputDialogFragment.getInstance(b, new InputDialogFragment.InputDialogFragmentCallback() {
                    @Override
                    public void onClickPositiveButton(InputDialogFragment fragment, DialogInterface dialog, EditText editText, int id) {
                        String rawText = editText.getText().toString();
                        mProfile.email = rawText;
                        updateUI();
                    }

                    @Override
                    public void onClickNegativeButton(InputDialogFragment fragment, DialogInterface dialog, EditText editText, int id) {

                    }
                });
                try {
                    frag.show(getFragmentManager(), "input_dialog");
                }
                catch (Exception e){
                    e.printStackTrace();
                }
            }
        });


        mTxtGiftRecord = getView().findViewById(R.id.txt_gift_record);
        mTxtGiftRecord.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                // TODO move to gift record
                GiftRecordListFragment frag = GiftRecordListFragment.getInstance();
                MenuFragment.setNextContentFragment(frag);
                Activity activity = getActivity();
                if (activity != null && !activity.isFinishing()){
                    Intent intent = new Intent(activity, InnerPageFragmentActivity.class);
                    Util.moveToActivity(activity, intent, true);
                }
            }
        });

        mTxtActionHistory = getView().findViewById(R.id.txt_action_history);
        mTxtActionHistory.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                // TODO move to action history page
                ActionHistoryListFragment frag = ActionHistoryListFragment.getInstance();
                MenuFragment.setNextContentFragment(frag);
                Activity activity = getActivity();
                if (activity != null && !activity.isFinishing()){
                    Intent intent = new Intent(activity, InnerPageFragmentActivity.class);
                    Util.moveToActivity(activity, intent, true);
                }
            }
        });

    }

    @Override
    protected void updateUIRunnable() {
        super.updateUIRunnable();

        if (mProfile == null){
            mImgProfile.setImageDrawable(null);
            mTxtName.setText("");
            mTxtPhone.setText("");
            mTxtEmail.setText("");
            mTxtMemberLevel.setText("");
            mTxtCreditTotal.setText("");
            mTxtCreditUsable.setText("");
        }
        else {
            if (mSelectedPhotoUri == null){
                Picasso.with(MyApplication.getAppContext()).load(ServiceManager.sharedInstance().getImageUrl(ServiceManager.IMAGE_URL_PROFILE, mProfile.image_id)).into(mImgProfile);
            }
            else{

                Bitmap bitmap = getBitmap(mSelectedPhotoUri, mImgProfile.getMeasuredWidth());
                if (bitmap != null){
                    mImgProfile.setImageBitmap(bitmap);
                }
            }

            mTxtName.setText(mProfile.user_name);
            mTxtPhone.setText(mProfile.mobile);
            mTxtEmail.setText(mProfile.email);
            mTxtMemberLevel.setText(Constant.getMemberLevelStr(mProfile.member_level));
            mTxtCreditTotal.setText(String.valueOf(mProfile.credit_total));
            mTxtCreditUsable.setText(String.valueOf(mProfile.remain_credit));
        }
    }

    @Override
    public void updateData() {
        updateUI();
        showLoadingScreen(true, "", Util.getString(R.string.msg_loading));
        ProfileService service = ServiceManager.sharedInstance().getServiceHandler().create(ProfileService.class);
        Call<ResponseWrapper<Profile>> call = service.getProfile();
        call.enqueue(new ResponseHandler<ResponseWrapper<Profile>>() {

            @Override
            public void onResponseParsed(Call<ResponseWrapper<Profile>> call, Response<ResponseWrapper<Profile>> response) {

                showLoadingScreen(false, "", Util.getString(R.string.msg_loading));

                if (response.body() != null && response.body().data != null) {
                    mProfile = response.body().data;
                    updateUI();
                }
            }

            @Override
            public void onFailure(Call<ResponseWrapper<Profile>> call, Throwable t) {
                showLoadingScreen(false, "", Util.getString(R.string.msg_loading));
                onHandleError(Util.getString(R.string.msg_error), null, null, new MessageDialogFragment.MessageDialogFragmentCallback() {

                    @Override
                    public void onClickPositiveButton(MessageDialogFragment fragment, DialogInterface dialog, int id) {
                        updateData();
                    }

                    @Override
                    public void onClickNegativeButton(MessageDialogFragment fragment, DialogInterface dialog, int id) {

                    }
                });
            }
        });
    }

    @Override
    public void onResume() {
        super.onResume();

        updateData();
    }


    @Override
    public boolean onClickActionBarItem(View view) {
        if (view.getId() == R.id.txt_right){
            // TODO save to server and trigger reload

            RequestBody rbDeviceId = RequestBody.create(MediaType.parse("text/plain"), GcmManager.sharedInstance().gcm_token);
            RequestBody rbDeviceType = RequestBody.create(MediaType.parse("text/plain"), "1");
            RequestBody rbMobileNo = RequestBody.create(MediaType.parse("text/plain"), mProfile.mobile);
            RequestBody rbLastname = RequestBody.create(MediaType.parse("text/plain"), mProfile.lastname);
            RequestBody rbFirstname = RequestBody.create(MediaType.parse("text/plain"), mProfile.firstname);
            RequestBody rbEmail = RequestBody.create(MediaType.parse("text/plain"), mProfile.email);


            RequestBody rbProfileImage = null;
            if (mSelectedPhotoUri != null) {
                Bitmap bitmap = getBitmap(mSelectedPhotoUri, 500);
                File file = new File(MyApplication.getAppContext().getCacheDir(), String.valueOf(0) + ".jpg");
                FileOutputStream outStream = null;
                try {
                    outStream = new FileOutputStream(file);
                    bitmap.compress(Bitmap.CompressFormat.JPEG, 100, outStream);
                    outStream.flush();
                    outStream.close();
                    Log.i(TAG, file.getAbsolutePath());
                } catch (FileNotFoundException e) {
                    e.printStackTrace();
                } catch (IOException e) {
                    e.printStackTrace();
                }
                rbProfileImage = RequestBody.create(MediaType.parse("image/jpeg"), file);
            }


            MemberService service = ServiceManager.sharedInstance().getServiceHandler().create(MemberService.class);
            Call<ResponseWrapper<Profile>> call = service.postUpdateMember(rbProfileImage, rbDeviceId, rbDeviceType, rbMobileNo, rbLastname, rbFirstname, rbEmail);
            call.enqueue(new ResponseHandler<ResponseWrapper<Profile>>() {
                @Override
                public void onResponseParsed(Call<ResponseWrapper<Profile>> call, Response<ResponseWrapper<Profile>> response) {
                    if (response.body() != null){
                        updateData();
                    }
                    else{
                        try {
                            onShowMessage(Util.getString(R.string.msg_error), Util.getString(R.string.msg_ok), null);
                        } catch (Exception e) {
                            e.printStackTrace();
                        }
                    }
                }

                @Override
                public void onFailure(Call<ResponseWrapper<Profile>> call, Throwable t) {
                    try {
                        onShowMessage(Util.getString(R.string.msg_error), Util.getString(R.string.msg_ok), null);
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                }
            });

            return true;
        }

        return false;
    }


    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        if (resultCode == Activity.RESULT_OK){
            Uri selectedImageUri = data.getData();
            if (selectedImageUri != null) {
                Log.i(TAG, "selected image: " + selectedImageUri.toString());
                mSelectedPhotoUri = selectedImageUri.toString();
                updateUI();
            }
            else{
                Log.e(TAG, "failed to select image");
            }

        }
    }

}
