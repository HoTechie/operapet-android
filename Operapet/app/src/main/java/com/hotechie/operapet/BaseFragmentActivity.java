package com.hotechie.operapet;

import android.app.ProgressDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Point;
import android.graphics.drawable.Drawable;
import android.os.Looper;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentActivity;
import android.os.Bundle;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.view.Display;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.TextView;

import com.hotechie.operapet.fragment.BaseFragment;
import com.hotechie.operapet.fragment.MenuFragment;
import com.hotechie.operapet.fragment.PostListFragment;
import com.hotechie.util.ListDialogFragment;
import com.hotechie.util.MessageDialogFragment;

public class BaseFragmentActivity extends FragmentActivity {
    private static String TAG = "BaseFragmentActivity";

    protected ViewGroup mLayoutContent = null;
    protected ViewGroup mLayoutContentContainer = null;
    protected ViewGroup mLayoutMenu = null;
    protected ViewGroup mLayoutActionBar = null;

    protected MenuFragment mMenuFragment = null;
    protected BaseFragment mContentFragment = null;

    protected ProgressDialog mProgressDialog = null;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(layoutRes());

        configUI();
    }

    public int layoutRes(){
        return R.layout.activity_base_fragment;
    }

    public void configUI(){

        mLayoutContent = (ViewGroup)findViewById(R.id.layout_content_frag);
        mLayoutContentContainer = (ViewGroup)findViewById(R.id.layout_content_container);
        mLayoutMenu = (ViewGroup)findViewById(R.id.layout_menu_frag);
        mLayoutActionBar = (ViewGroup)findViewById(R.id.layout_action_bar);

        Display display = getWindowManager().getDefaultDisplay();
        Point screenSize = new Point();
        display.getSize(screenSize);
        if (mLayoutContentContainer != null) {
            mLayoutContentContainer.getLayoutParams().width = screenSize.x;

            if (mLayoutMenu != null)
                mLayoutMenu.setX(screenSize.x);

            mMenuFragment = MenuFragment.getInstance(null);
            if (mLayoutMenu != null)
                replaceContentFragment(R.id.layout_menu_frag, mMenuFragment);
            mContentFragment = mMenuFragment.getCurrentContentFragment();
            mMenuFragment.setNextContentFragment(null);
            if (mContentFragment != null) {
                replaceContentFragment(R.id.layout_content_frag, mContentFragment);
                setupActionBar(mContentFragment.getTitle(), null, Util.getDrawable(this, R.drawable.btn_menu), null, null);
            } else {
//            setupActionBar("", null, Util.getDrawable(this, R.drawable.btn_menu), null, null);

                // FIXME
                mContentFragment = PostListFragment.getInstance(null);
                replaceContentFragment(R.id.layout_content_frag, mContentFragment);
                setupActionBar(mContentFragment.getTitle(), null, Util.getDrawable(this, R.drawable.btn_menu), null, null);
            }
        }
    }

    public boolean onClickActionBarItem(View view){
        boolean isHandled = false;

        if (mContentFragment != null)
            isHandled = mContentFragment.onClickActionBarItem(view);

        if (!isHandled) {
            int id = view.getId();
            switch (id) {
                case R.id.btn_left:
//                onBackPressed();
//                isHandled = true;
                    toggleMenu(true);
                    isHandled = true;
                    break;
                case R.id.btn_right:
                    toggleMenu(true);
                    isHandled = true;
                    break;
            }
        }

        return isHandled;
    }


    @Override
    public void onBackPressed() {
        if (Looper.myLooper() == Looper.getMainLooper() && !this.isFinishing()) {
//            super.onBackPressed();
            if (this instanceof LoginActivity) {
                onShowMessage(getString(R.string.msg_leave_app_q), getString(R.string.msg_ok), getString(R.string.msg_cancel), new MessageDialogFragment.MessageDialogFragmentCallback() {
                    @Override
                    public void onClickPositiveButton(MessageDialogFragment fragment, DialogInterface dialog, int id) {
                        Util.backToActivity(BaseFragmentActivity.this, true);
                    }

                    @Override
                    public void onClickNegativeButton(MessageDialogFragment fragment, DialogInterface dialog, int id) {

                    }
                });
            }
            else if (this instanceof BaseFragmentActivity) {
                onShowMessage(getString(R.string.msg_logout_q), getString(R.string.msg_yes), getString(R.string.msg_no), new MessageDialogFragment.MessageDialogFragmentCallback() {
                    @Override
                    public void onClickPositiveButton(MessageDialogFragment fragment, DialogInterface dialog, int id) {
                        SessionManager.sharedInstance().setProfile(null);
//                        Util.backToActivity(BaseFragmentActivity.this, true);
                        Intent intent = new Intent(BaseFragmentActivity.this, LoginActivity.class);
                        intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
                        Util.moveToActivity(BaseFragmentActivity.this, intent, false);
                    }

                    @Override
                    public void onClickNegativeButton(MessageDialogFragment fragment, DialogInterface dialog, int id) {

                    }
                });
            }
        }
        else{
            runOnUiThread(new Runnable() {
                @Override
                public void run() {
                    onBackPressed();
                }
            });
        }

    }

    public void toggleMenu(boolean isShow){
        if (isShow && mLayoutContentContainer.findViewWithTag("overlay_for_menu") == null){
            mLayoutContentContainer.post(new Runnable() {
                @Override
                public void run() {
                    View overlay = new View(BaseFragmentActivity.this);
                    overlay.setBackgroundColor(Util.getColor(BaseFragmentActivity.this, R.color.main_content_overlay));
                    overlay.setTag("overlay_for_menu");
                    overlay.setClickable(true);
                    overlay.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View view) {
                            toggleMenu(false);
                        }
                    });
                    mLayoutContentContainer.addView(overlay);

//                    RelativeLayout.LayoutParams params = (RelativeLayout.LayoutParams) mLayoutContent.getLayoutParams();
//                    params.setMargins(getResources().getDimensionPixelSize(R.dimen.menu_width_inverse), 0, 0, 0);
                    mLayoutContentContainer.setX(getResources().getDimensionPixelSize(R.dimen.menu_width));
                    Display display = getWindowManager().getDefaultDisplay();
                    Point screenSize = new Point();
                    display.getSize(screenSize);
                    mLayoutMenu.setX(0);
                }
            });
        }
        else if (!isShow){
            mLayoutContentContainer.post(new Runnable() {
                @Override
                public void run() {
                    View overlay = mLayoutContentContainer.findViewWithTag("overlay_for_menu");
                    if (overlay != null){
                        mLayoutContentContainer.removeView(overlay);
                    }

//                    RelativeLayout.LayoutParams params = (RelativeLayout.LayoutParams) mLayoutContent.getLayoutParams();
//                    params.setMargins(0, 0, 0, 0);
                    mLayoutContentContainer.setX(0);
                    Display display = getWindowManager().getDefaultDisplay();
                    Point screenSize = new Point();
                    display.getSize(screenSize);
                    mLayoutMenu.setX(getResources().getDimensionPixelSize(R.dimen.menu_width_inverse));
                }
            });
        }
    }

    public void setupActionBar(final String titleText, final Drawable titleIcon, final Drawable leftIcon, final Drawable rightIcon){
        this.setupActionBar(titleText, titleIcon, leftIcon, rightIcon, null);
    }

    public void setupActionBar(final String titleText, final Drawable titleIcon, final Drawable leftIcon, final Drawable rightIcon, final Drawable rightIcon2){
        runOnUiThread(new Runnable() {
            @Override
            public void run() {
                if (mLayoutActionBar != null) {
//                    getActionBar().setDisplayHomeAsUpEnabled(true);
//                    getActionBar().setHomeButtonEnabled(true);
//                    getActionBar().setDisplayShowCustomEnabled(true);
//                    getActionBar().setDisplayShowTitleEnabled(false);
                    LayoutInflater inflator = LayoutInflater.from(BaseFragmentActivity.this);
                    View v = inflator.inflate(R.layout.action_bar_content_view, null);
//                    ActionBar.LayoutParams layoutParams = new ActionBar.LayoutParams(ActionBar.LayoutParams.MATCH_PARENT, ActionBar.LayoutParams.MATCH_PARENT);
//                    getActionBar().setCustomView(v, layoutParams);
                    ViewGroup.LayoutParams layoutParams = new ViewGroup.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.MATCH_PARENT);
                    mLayoutActionBar.addView(v, layoutParams);
                    TextView txtTitle = ((TextView) v.findViewById(R.id.title));
                    if (txtTitle != null)
                        txtTitle.setText(titleText);
                    ((ImageView) v.findViewById(R.id.icon)).setImageDrawable(titleIcon);
                    ImageButton btnLeft = ((ImageButton) v.findViewById(R.id.btn_left));
                    if (leftIcon == null)
                        btnLeft.setVisibility(View.GONE);
                    else {
                        btnLeft.setImageDrawable(leftIcon);
                        btnLeft.setVisibility(View.VISIBLE);
                    }
                    ImageButton btnRight = ((ImageButton) v.findViewById(R.id.btn_right));
                    if (rightIcon == null)
                        btnRight.setVisibility(View.INVISIBLE);
                    else {
                        btnRight.setImageDrawable(rightIcon);
                        btnRight.setVisibility(View.VISIBLE);
                    }

                    ImageButton btnRight2 = ((ImageButton) v.findViewById(R.id.btn_right_2));
                    if (rightIcon2 == null)
                        btnRight2.setVisibility(View.GONE);
                    else {
                        btnRight2.setImageDrawable(rightIcon2);
                        btnRight2.setVisibility(View.VISIBLE);
                    }

                    View.OnClickListener onClickListener = new View.OnClickListener() {
                        @Override
                        public void onClick(View view) {
                            onClickActionBarItem(view);
                        }
                    };
                    btnLeft.setOnClickListener(onClickListener);
                    btnRight.setOnClickListener(onClickListener);
                    btnRight2.setOnClickListener(onClickListener);

//                    if (Build.VERSION.SDK_INT >= 21)
//                        getActionBar().setElevation(0);
                }
            }
        });

    }


    protected void replaceContentFragment(BaseFragment frag){
        replaceContentFragment(R.id.layout_content_frag, frag);
    }


    protected void replaceContentFragment(int layoutRes, Fragment frag){

        FragmentManager fragmentManager = getSupportFragmentManager();
        FragmentTransaction fragmentTransaction = fragmentManager.beginTransaction();
        fragmentTransaction.replace(layoutRes, frag).commit();
    }

    public void showLoadingScreen(final boolean isShow, final String title, final String msg){
        runOnUiThread(new Runnable() {
            @Override
            public void run() {
                if (mProgressDialog != null && mProgressDialog.isShowing())
                    mProgressDialog.dismiss();

                if (isShow) {
                    mProgressDialog = ProgressDialog.show(BaseFragmentActivity.this, title, msg);
                }
            }
        });

    }

    protected void onHandleError(String msg, String btnPositive, String btnNegative, MessageDialogFragment.MessageDialogFragmentCallback callback){
        Bundle b = new Bundle();
//        b.putString(MessageDialogFragment.KEY_TITLE, getString(R.string.msg_error));
        if (msg != null)
            b.putString(MessageDialogFragment.KEY_MESSAGE, msg);
        else
            b.putString(MessageDialogFragment.KEY_MESSAGE, getString(R.string.msg_error));

        if (btnPositive != null)
            b.putString(MessageDialogFragment.KEY_POSITIVE_BTN, btnPositive);
        else
            b.putString(MessageDialogFragment.KEY_POSITIVE_BTN, getString(R.string.msg_reload));

        if (btnNegative != null)
            b.putString(MessageDialogFragment.KEY_NEGATIVE_BTN, btnNegative);
        else
            b.putString(MessageDialogFragment.KEY_NEGATIVE_BTN, getString(R.string.msg_cancel));


        MessageDialogFragment frag = MessageDialogFragment.getInstance(b, callback);
        frag.setCancelable(false);
        frag.show(getSupportFragmentManager(), "error_message");
    }


    protected void onHandleSelection(String title, String[] itemList, ListDialogFragment.ListDialogFragmentCallback callback){
        Bundle b = new Bundle();
        b.putString(ListDialogFragment.KEY_TITLE, title);
        b.putStringArray(ListDialogFragment.KEY_ITEM_LIST, itemList);
        b.putBoolean(ListDialogFragment.KEY_IS_SINGLE_CHOICE, true);

        ListDialogFragment frag = ListDialogFragment.getInstance(b, callback);
//        frag.setCancelable(false);
        frag.show(getSupportFragmentManager(), "selection_list");
    }


    protected void onShowMessage(String msg, String btnPositive, MessageDialogFragment.MessageDialogFragmentCallback callback){
        Bundle b = new Bundle();
//        b.putString(MessageDialogFragment.KEY_TITLE, getString(R.string.msg_error));
        if (msg != null)
            b.putString(MessageDialogFragment.KEY_MESSAGE, msg);
        else
            b.putString(MessageDialogFragment.KEY_MESSAGE, getString(R.string.msg_error));

        if (btnPositive != null)
            b.putString(MessageDialogFragment.KEY_POSITIVE_BTN, btnPositive);
        else
            b.putString(MessageDialogFragment.KEY_POSITIVE_BTN, getString(R.string.msg_ok));


        MessageDialogFragment frag = MessageDialogFragment.getInstance(b, callback);
        frag.setCancelable(false);
        frag.show(getSupportFragmentManager(), "error_message");
    }


    protected void onShowMessage(String msg, String btnPositive, String btnNegative, MessageDialogFragment.MessageDialogFragmentCallback callback){
        onHandleError(msg, btnPositive == null ? getString(R.string.msg_ok) : btnPositive, btnNegative, callback);
    }

}
