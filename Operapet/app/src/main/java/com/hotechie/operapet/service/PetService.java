package com.hotechie.operapet.service;

import com.hotechie.operapet.service.model.Pet;
import com.hotechie.operapet.service.model.Profile;
import com.hotechie.operapet.service.model.ResponseWrapper;

import okhttp3.RequestBody;
import retrofit2.Call;
import retrofit2.http.Field;
import retrofit2.http.FormUrlEncoded;
import retrofit2.http.GET;
import retrofit2.http.Multipart;
import retrofit2.http.POST;
import retrofit2.http.Part;
import retrofit2.http.Path;

/**
 * Created by duncan on 10/10/2016.
 */

public interface PetService {

    @Multipart
    @POST("CreatePetApi")
    Call<ResponseWrapper<Pet>> postCreatePet(@Part("pet_id") RequestBody pet_id,
                                             @Part("gender") RequestBody gender,
                                             @Part("member_id") RequestBody member_id,
                                             @Part("pet_type") RequestBody pet_type,
                                             @Part("birth_date") RequestBody birth_date,
                                             @Part("description") RequestBody description,
                                             @Part("\"pet_image\\\"; filename=\\\"pet_image.jpg\\\" \"") RequestBody pet_image,
                                             @Part("medical_record") RequestBody medical_record,
                                             @Part("last_body_check_date") RequestBody last_body_check_date,
                                             @Part("next_body_check_date") RequestBody next_body_check_date,
                                             @Part("last_bath_hair_cut_description") RequestBody last_bath_hair_cut_description,
                                             @Part("bmi_record_weight_1") RequestBody bmi_record_weight_1,
                                             @Part("bmi_record_length_1") RequestBody bmi_record_length_1,
                                             @Part("bmi_record_weight_2") RequestBody bmi_record_weight_2,
                                             @Part("bmi_record_length_2") RequestBody bmi_record_length_2,
                                             @Part("bmi_record_weight_3") RequestBody bmi_record_weight_3,
                                             @Part("bmi_record_length_3") RequestBody bmi_record_length_3,
                                             @Part("bmi_record_date_1") RequestBody bmi_record_date_1,
                                             @Part("bmi_record_date_2") RequestBody bmi_record_date_2,
                                             @Part("bmi_record_date_3") RequestBody bmi_record_date_3,
                                             @Part("pin_card_json_string") RequestBody pin_card_json_string);
    @GET("petdetailapi/{pet_id}")
    Call<ResponseWrapper<Pet>> getPetDetail(@Path("pet_id") int pet_id);

    @Multipart
    @POST("DeletePetApi")
    Call<Object> postDeletePet(@Part("pet_id") RequestBody pet_id);

}
