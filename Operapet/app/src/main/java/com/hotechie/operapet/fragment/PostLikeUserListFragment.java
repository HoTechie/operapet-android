package com.hotechie.operapet.fragment;

import android.content.DialogInterface;

import com.hotechie.operapet.R;
import com.hotechie.operapet.Util;
import com.hotechie.operapet.service.PostsService;
import com.hotechie.operapet.service.ResponseHandler;
import com.hotechie.operapet.service.ServiceManager;
import com.hotechie.operapet.service.model.Profile;
import com.hotechie.operapet.service.model.ResponseListWrapper;
import com.hotechie.operapet.ui.row_view_factory.RowViewFactoryListAdapter;
import com.hotechie.operapet.ui.row_view_factory.UserRowViewFactory;
import com.hotechie.util.MessageDialogFragment;

import java.security.interfaces.RSAKey;
import java.util.ArrayList;
import java.util.List;

import retrofit2.Call;
import retrofit2.Response;

/**
 * Created by duncan on 3/10/2016.
 */

public class PostLikeUserListFragment extends BaseListFragment {
    private static String TAG = "PostLikeUserListFragt";

    public static PostLikeUserListFragment getInstance(int postId){
        PostLikeUserListFragment frag = new PostLikeUserListFragment();
        frag.mPostId = postId;
        return frag;
    }

    protected int mPostId;

    protected List<Profile> mUsers = new ArrayList<>();
    protected UserRowViewFactory mFactory;

    @Override
    public String getTitle() {
        return Util.getString(R.string.title_post_like_user_list);
    }

    @Override
    public void onResume() {
        super.onResume();
        updateData();
    }

    @Override
    public void configUI() {
        super.configUI();

        mFactory = new UserRowViewFactory(mUsers);
        mAdapter = new RowViewFactoryListAdapter(mFactory);
        mListView.setAdapter(mAdapter);
    }

    @Override
    protected void updateUIRunnable() {
        super.updateUIRunnable();

        mAdapter.setFactory(mFactory);
        mAdapter.notifyDataSetChanged();
    }

    @Override
    public void updateData() {
        super.updateData();

        PostsService service = ServiceManager.sharedInstance().getServiceHandler().create(PostsService.class);
        Call<ResponseListWrapper<Profile>> call = service.getWhoLikePost(mPostId);
        call.enqueue(new ResponseHandler<ResponseListWrapper<Profile>>() {
            @Override
            public void onResponseParsed(Call<ResponseListWrapper<Profile>> call, Response<ResponseListWrapper<Profile>> response) {
                if (response.body() != null && response.body().data != null) {
                    mUsers = new ArrayList<Profile>(response.body().data);
                    mFactory = new UserRowViewFactory(mUsers);
                    updateUI();
                }
            }

            @Override
            public void onFailure(Call<ResponseListWrapper<Profile>> call, Throwable t) {
                onHandleError(Util.getString(R.string.msg_error), null, null, new MessageDialogFragment.MessageDialogFragmentCallback() {

                    @Override
                    public void onClickPositiveButton(MessageDialogFragment fragment, DialogInterface dialog, int id) {
                        updateData();
                    }

                    @Override
                    public void onClickNegativeButton(MessageDialogFragment fragment, DialogInterface dialog, int id) {

                    }
                });
            }
        });
    }
}
