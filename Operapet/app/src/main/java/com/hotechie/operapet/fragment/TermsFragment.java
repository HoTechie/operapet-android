package com.hotechie.operapet.fragment;

import com.hotechie.operapet.R;
import com.hotechie.operapet.Util;
import com.hotechie.operapet.service.model.Profile;
import com.hotechie.operapet.ui.row_view_factory.FriendRequestRowViewFactory;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by duncan on 7/2/2017.
 */

public class TermsFragment extends BaseFragment {

    private static String TAG = "TermsFragment";

    public static TermsFragment getInstance(){
        TermsFragment frag = new TermsFragment();
        return frag;
    }

    @Override
    public String getTitle() {
        return "使用條款";
    }

    @Override
    public int layoutRes() {
        return R.layout.fragment_terms;
    }

    @Override
    public void onResume() {
        super.onResume();
    }

    @Override
    public void configUI() {
        super.configUI();
    }
}
