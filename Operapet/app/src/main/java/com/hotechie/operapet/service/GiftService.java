package com.hotechie.operapet.service;

import com.hotechie.operapet.service.model.Gift;
import com.hotechie.operapet.service.model.Post;
import com.hotechie.operapet.service.model.ResponseListWrapper;
import com.hotechie.operapet.service.model.ResponseWrapper;

import okhttp3.RequestBody;
import retrofit2.Call;
import retrofit2.http.GET;
import retrofit2.http.Multipart;
import retrofit2.http.POST;
import retrofit2.http.Part;
import retrofit2.http.Path;

/**
 * Created by duncan on 5/10/2016.
 */

public interface GiftService {

    @GET("giftapi")
    Call<ResponseListWrapper<Gift>> getGifts();

    @GET("giftapi/{gift_id}")
    Call<ResponseWrapper<Gift>> getGifts(@Path("gift_id") int gift_id);

    @Multipart
    @POST("RedeemGiftApi")
    Call<ResponseWrapper<Gift>> postRedeemGift(@Part("gift_id") RequestBody gift_id);

    @Multipart
    @POST("CollectGiftApi")
    Call<ResponseWrapper<Gift>> postCollectGift(@Part("redemption_id") RequestBody redemption_id);
}
