package com.hotechie.operapet;

import android.app.Application;
import android.content.Context;
import android.content.Intent;
import android.os.Build;

import com.crashlytics.android.Crashlytics;
import com.facebook.FacebookSdk;

import io.fabric.sdk.android.Fabric;

/**
 * Created by duncan on 17/7/2016.
 */
public class MyApplication extends Application {

    protected static Context mContext = null;

    @Override
    public void onCreate() {
        super.onCreate();
        if (!BuildConfig.DEBUG)
            Fabric.with(this, new Crashlytics());
        mContext = this.getApplicationContext();

        Intent intent = new Intent(this, GcmManager.RegistrationIntentService.class);
        startService(intent);

        FacebookSdk.sdkInitialize(MyApplication.getAppContext());
    }



    public static Context getAppContext(){
        return mContext;
    }
}
