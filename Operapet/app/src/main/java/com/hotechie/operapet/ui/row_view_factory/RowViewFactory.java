package com.hotechie.operapet.ui.row_view_factory;

import android.view.View;


/**
 * Created by duncan on 10/7/2016.
 */
public class RowViewFactory extends Object {

    public int getCount(){
        return 0;
    }

    public Object getItem(int index){
        return null;
    }

    public View getView(Object object){
        return null;
    }

    /**
     *
     * @param contentView
     * @return should standard params apply?
     */
    public boolean modifyViewLayout(View contentView, Object object, int index){
        return true;
    }
}
