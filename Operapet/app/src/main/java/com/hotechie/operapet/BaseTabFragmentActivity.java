package com.hotechie.operapet;

import android.content.Intent;
import android.net.Uri;
import android.view.View;
import android.widget.ImageView;

import com.hotechie.operapet.fragment.BaseFragment;
import com.hotechie.operapet.fragment.CreatePostFragment;
import com.hotechie.operapet.fragment.MenuFragment;
import com.hotechie.operapet.fragment.PostListFragment;
import com.hotechie.operapet.fragment.ProfileFragment;
import com.hotechie.operapet.fragment.SearchFriendFragment;
import com.hotechie.operapet.fragment.SearchTagFragment;
import com.hotechie.operapet.fragment.WallFragment;
import com.hotechie.operapet.service.AdvService;
import com.hotechie.operapet.service.ResponseHandler;
import com.hotechie.operapet.service.ServiceManager;
import com.hotechie.operapet.service.model.Adv;
import com.hotechie.operapet.service.model.ResponseListWrapper;
import com.squareup.picasso.Picasso;

import java.util.ArrayList;

import retrofit2.Call;
import retrofit2.Response;

/**
 * Created by duncan on 23/9/2016.
 */

public class BaseTabFragmentActivity extends BaseFragmentActivity {
    private static String TAG = "BaseTabFragmentActivity";

    public static String INTENT_SHOW_ADV = "show_adv";


    @Override
    public int layoutRes(){
        return R.layout.activity_base_tab_fragment;
    }

    @Override
    public void configUI() {
        super.configUI();

        if (mContentFragment != null) {
            String pageTitle = mContentFragment.getTitle();
            if (pageTitle.equals(getString(R.string.title_posts))){
                ((ImageView)findViewById(R.id.tab_posts)).setColorFilter(Util.getColor(MyApplication.getAppContext(), R.color.tab_icon_color_highlight));
            }
            else if (pageTitle.equals(getString(R.string.title_my_wall))){
                ((ImageView)findViewById(R.id.tab_profile)).setColorFilter(Util.getColor(MyApplication.getAppContext(), R.color.tab_icon_color_highlight));
            }
            else if (pageTitle.equals(Util.getString(R.string.title_search_tag))){
                ((ImageView)findViewById(R.id.tab_tags)).setColorFilter(Util.getColor(MyApplication.getAppContext(), R.color.tab_icon_color_highlight));
            }
            else if (pageTitle.equals(Util.getString(R.string.title_search_friend))){
                ((ImageView)findViewById(R.id.tab_friends)).setColorFilter(Util.getColor(MyApplication.getAppContext(), R.color.tab_icon_color_highlight));
            }
        }

        View.OnClickListener tabItemOnClickListener = new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                int viewId = view.getId();
                if (viewId == R.id.tab_posts){
                    BaseFragment frag = PostListFragment.getInstance(null);
                    MenuFragment.setNextContentFragment(frag);
                    Intent newIntent = new Intent(BaseTabFragmentActivity.this, BaseTabFragmentActivity.class);
                    newIntent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                    Util.moveToActivity(BaseTabFragmentActivity.this, newIntent, false);
                }
                else if (viewId == R.id.tab_profile){
                    BaseFragment frag = WallFragment.getInstance(SessionManager.sharedInstance().getMemberId());
                    MenuFragment.setNextContentFragment(frag);
                    Intent newIntent = new Intent(BaseTabFragmentActivity.this, BaseTabFragmentActivity.class);
                    newIntent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                    Util.moveToActivity(BaseTabFragmentActivity.this, newIntent, false);
                }
                else if (viewId == R.id.tab_create_post){
                    BaseFragment frag = CreatePostFragment.getInstance();
                    MenuFragment.setNextContentFragment(frag);
                    Intent newIntent = new Intent(BaseTabFragmentActivity.this, InnerPageFragmentActivity.class);
                    newIntent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                    Util.moveToActivity(BaseTabFragmentActivity.this, newIntent, false);
                }
                else if (viewId == R.id.tab_tags){
                    BaseFragment frag = SearchTagFragment.getInstance();
                    MenuFragment.setNextContentFragment(frag);
                    Intent newIntent = new Intent(BaseTabFragmentActivity.this, BaseTabFragmentActivity.class);
                    newIntent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                    Util.moveToActivity(BaseTabFragmentActivity.this, newIntent, false);
                }
                else if (viewId == R.id.tab_friends){
                    BaseFragment frag = SearchFriendFragment.getInstance();
                    MenuFragment.setNextContentFragment(frag);
                    Intent newIntent = new Intent(BaseTabFragmentActivity.this, BaseTabFragmentActivity.class);
                    newIntent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                    Util.moveToActivity(BaseTabFragmentActivity.this, newIntent, false);
                }
            }
        };

        ArrayList<View> tabItemVies = new ArrayList<>();
        findViewById(R.id.layout_tabs).findViewsWithText(tabItemVies, "tabItems", View.FIND_VIEWS_WITH_CONTENT_DESCRIPTION);
        for (View view : tabItemVies){
            view.setOnClickListener(tabItemOnClickListener);
        }


        if (getIntent() != null && getIntent().getBooleanExtra(INTENT_SHOW_ADV, false)){
            // TODO show adv
            final View layoutAdv = findViewById(R.id.layout_adv);
            final ImageView imgAdv = (ImageView)layoutAdv.findViewById(R.id.img_adv);
            ImageView imgAdvClose = (ImageView)layoutAdv.findViewById(R.id.img_adv_close);
            imgAdvClose.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    layoutAdv.setVisibility(View.GONE);
                }
            });

            AdvService service = ServiceManager.sharedInstance().getServiceHandler().create(AdvService.class);
            Call<ResponseListWrapper<Adv>> call = service.getAdvListByType(Constant.ADV_TYPE_POP_UP);
            call.enqueue(new ResponseHandler<ResponseListWrapper<Adv>>() {
                @Override
                public void onResponseParsed(Call<ResponseListWrapper<Adv>> call, Response<ResponseListWrapper<Adv>> response) {
                    if (response.body() != null && response.body().data != null){
                        final Adv adv = response.body().data.get(0);
                        layoutAdv.setVisibility(View.VISIBLE);
                        Picasso.with(MyApplication.getAppContext()).load(ServiceManager.sharedInstance().getImageUrl(ServiceManager.IMAGE_URL_ADV, adv.image_id)).into(imgAdv);
                        imgAdv.setOnClickListener(new View.OnClickListener() {
                            @Override
                            public void onClick(View view) {
                                Intent i = new Intent(Intent.ACTION_VIEW);
                                i.setData(Uri.parse(adv.link));
                                startActivity(i);
                            }
                        });
                    }
                }

                @Override
                public void onFailure(Call<ResponseListWrapper<Adv>> call, Throwable t) {

                }
            });
        }

    }
}
