package com.hotechie.operapet;

import android.content.SharedPreferences;


import com.hotechie.operapet.service.model.Profile;

import java.util.HashMap;

/**
 * Created by duncan on 19/7/2016.
 */
public class SessionManager extends Object {

    private static SessionManager mInstance = null;
    public static SessionManager sharedInstance(){
        if (mInstance == null)
            mInstance = new SessionManager();
        return mInstance;
    }

    SharedPreferences storage;
    SharedPreferences.Editor editor;

    private SessionManager(){

        storage = MyApplication.getAppContext().getSharedPreferences("session", 0);
        editor = storage.edit();
    }

    protected static Profile _profile = null;
    public void setProfile(Profile profile){
        _profile = profile;

        if (_profile != null){
            editor.putInt("member_id", profile.id);
            if (_profile.token != null)
                editor.putString("token", profile.token);
        }
        else{
            editor.remove("member_id");
            editor.remove("token");
        }
        editor.commit();
    }
    public Profile getProfile(){
        return _profile;
    }
    public int getMemberId(){
        return storage.getInt("member_id", 1);
    }
    public String getMemberToken(){
        return storage.getString("token", "");
    }


    /* dynamic object */
    public static String VAULT_PIN_CARD_EDIT = "pin_card_edit";
    public static String VAULT_OPEN_NOTIFICATION_LIST = "open_notification_list";
    protected HashMap<String, Object> mDynamicVault = new HashMap<>();
    public void putIn(String key, Object object){
        mDynamicVault.put(key, object);
    }
    public Object getObject(String key){
        return mDynamicVault.get(key);
    }
}
