package com.hotechie.operapet;

import android.content.Intent;
import android.graphics.Paint;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.facebook.AccessToken;
import com.facebook.CallbackManager;
import com.facebook.FacebookCallback;
import com.facebook.FacebookException;
import com.facebook.FacebookSdk;
import com.facebook.GraphRequest;
import com.facebook.GraphResponse;
import com.facebook.HttpMethod;
import com.facebook.login.LoginManager;
import com.facebook.login.LoginResult;
import com.facebook.login.widget.LoginButton;
import com.hotechie.operapet.fragment.GiftListFragment;
import com.hotechie.operapet.fragment.MenuFragment;
import com.hotechie.operapet.fragment.TermsFragment;
import com.hotechie.operapet.service.MemberService;
import com.hotechie.operapet.service.ProfileService;
import com.hotechie.operapet.service.ResponseHandler;
import com.hotechie.operapet.service.ServiceManager;
import com.hotechie.operapet.service.model.BaseModel;
import com.hotechie.operapet.service.model.Profile;
import com.hotechie.operapet.service.model.ResponseWrapper;

import org.json.JSONException;
import org.json.JSONObject;

import okhttp3.MediaType;
import okhttp3.RequestBody;
import retrofit2.Call;
import retrofit2.Response;

/**
 * Created by duncan on 3/10/2016.
 */

public class LoginActivity extends BaseFragmentActivity {
    private static String TAG = "LoginActivity";

    protected static int STEP_MENU = 1;
    protected static int STEP_LOGIN_PHONE = 2;
    protected static int STEP_REGISTER = 3;
    protected static int STEP_FACEBOOK_REGISTER = 4;
    protected static int STEP_PHONE_REGISTER = 5;
    protected static int STEP_SMS_VERIFY = 6;
    protected static int STEP_SMS_CONFIRM = 7;

    public int mStep = STEP_MENU;

    protected String mMobileNo = null;
    protected String mFacebookId = null, mFacebookToken = null;
    protected String mFacebookFirstname, mFacebookLastname, mFacebookEmail;

    protected View mBtnBack = null;
    protected View mLayoutTop = null;
    protected ImageView mImageTopBg = null;
    protected View mLayoutBottom = null;
    protected View mLayoutLoginMenu = null;
    protected View mLayoutRegister = null;
    protected View mLayoutLoginPhone = null;
    protected View mLayoutSMSVerify = null;
    protected View mLayoutSMSConfirm = null;


    protected CallbackManager callbackManager;

    @Override
    public int layoutRes() {
        return R.layout.activity_login;
    }

    @Override
    public void configUI() {
        super.configUI();

        mBtnBack = findViewById(R.id.img_back);
        mLayoutTop = findViewById(R.id.layout_top);
        mImageTopBg = (ImageView) findViewById(R.id.img_top_bg);
        mLayoutBottom = findViewById(R.id.layout_bottom);

        TextView txtTermsLogin = (TextView)findViewById(R.id.txt_login_terms);
        txtTermsLogin.setPaintFlags(txtTermsLogin.getPaintFlags() |   Paint.UNDERLINE_TEXT_FLAG);
        txtTermsLogin.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                TermsFragment frag = TermsFragment.getInstance();
                MenuFragment.setNextContentFragment(frag);
                Intent newIntent = new Intent(LoginActivity.this, InnerPageFragmentActivity.class);
                Util.moveToActivity(LoginActivity.this, newIntent, false);
            }
        });
        TextView txtTermsRegister = (TextView)findViewById(R.id.txt_register_terms);
        txtTermsRegister.setPaintFlags(txtTermsRegister.getPaintFlags() |   Paint.UNDERLINE_TEXT_FLAG);
        txtTermsRegister.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                TermsFragment frag = TermsFragment.getInstance();
                MenuFragment.setNextContentFragment(frag);
                Intent newIntent = new Intent(LoginActivity.this, InnerPageFragmentActivity.class);
                Util.moveToActivity(LoginActivity.this, newIntent, false);
            }
        });


        // login menu
        mLayoutLoginMenu = findViewById(R.id.layout_login_menu);
        FacebookSdk.sdkInitialize(MyApplication.getAppContext());
        callbackManager = CallbackManager.Factory.create();
        mLayoutLoginMenu.findViewById(R.id.img_facebook_login).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                // TODO facebook login

                AccessToken token = AccessToken.getCurrentAccessToken();
                if (token != null){
                    LoginManager.getInstance().logOut();
                }

                final LoginButton loginButton = (LoginButton) findViewById(R.id.fb_login);
                loginButton.setReadPermissions("email");
                LoginManager.getInstance().registerCallback(callbackManager,
                        new FacebookCallback<LoginResult>() {
                            @Override
                            public void onSuccess(LoginResult loginResult) {
                                AccessToken token = loginResult.getAccessToken();

                                showLoadingScreen(true, "", Util.getString(R.string.msg_saving));

                                final String accessToken = token.getToken();
                                Log.i(TAG, "accessToken = " + accessToken);

                                mFacebookToken = accessToken;

                                GraphRequest request = GraphRequest.newMeRequest(token, new GraphRequest.GraphJSONObjectCallback() {
                                    @Override
                                    public void onCompleted(JSONObject object, GraphResponse response) {
                                        showLoadingScreen(false, null, null);

                                        Log.i(TAG, response.toString());
                                        String userId = null;
                                        try{
                                            userId = object.getString("id");

                                        } catch (JSONException e) {
                                            e.printStackTrace();
                                        }

                                        if (userId != null){
                                            // TODO take the id and move to phone number
                                            try {
                                                mFacebookFirstname = object.getString("first_name");
                                                mFacebookLastname = object.getString("last_name");
                                                mFacebookEmail = object.getString("email");

                                                mFacebookId = userId;


                                                final RequestBody rbFacebookId = RequestBody.create(MediaType.parse("text/plain"), String.valueOf(mFacebookId));
                                                final MemberService service = ServiceManager.sharedInstance().getServiceHandler().create(MemberService.class);
                                                Call<Object> callIsFbExist = service.postIsFbExist(rbFacebookId);
                                                callIsFbExist.enqueue(new ResponseHandler<Object>() {
                                                    @Override
                                                    public void onResponseParsed(Call<Object> call, Response<Object> response) {
                                                        if (response.body() != null){
                                                            // TODO registered before
                                                            Call<ResponseWrapper<Profile>> callFbLogin = service.postFbLoginSendSms(rbFacebookId);
                                                            callFbLogin.enqueue(new ResponseHandler<ResponseWrapper<Profile>>() {
                                                                @Override
                                                                public void onResponseParsed(Call<ResponseWrapper<Profile>> call, Response<ResponseWrapper<Profile>> response) {
                                                                    if (response.body() != null){
                                                                        Log.e(TAG, "postFbLogin " + String.valueOf(response.body() != null));
                                                                        showLoadingScreen(false, null, null);
                                                                        mMobileNo = response.body().data.mobile;
                                                                        mStep = STEP_SMS_VERIFY;
                                                                        updateUI();
                                                                    }
                                                                    else{
                                                                        showLoadingScreen(false, null, null);
                                                                        onShowMessage(Util.getString(R.string.msg_error), null, null);
                                                                    }
                                                                }

                                                                @Override
                                                                public void onFailure(Call<ResponseWrapper<Profile>> call, Throwable t) {
                                                                    showLoadingScreen(false, null, null);
                                                                    onShowMessage(Util.getString(R.string.msg_error), null, null);
                                                                }
                                                            });
                                                        }
                                                        else{
                                                            // TODO going to register
                                                            mStep = STEP_PHONE_REGISTER;
                                                            updateUI();
                                                        }
                                                    }

                                                    @Override
                                                    public void onFailure(Call<Object> call, Throwable t) {
                                                        showLoadingScreen(false, null, null);
                                                        onShowMessage(Util.getString(R.string.msg_error), null, null);
                                                    }
                                                });
                                            } catch (JSONException e) {
                                                e.printStackTrace();
                                            }

                                            if (mFacebookId == null){
                                                onShowMessage(Util.getString(R.string.msg_error), Util.getString(R.string.msg_ok), null);
                                            }
                                        }
                                    }
                                });

                                Bundle parameters = new Bundle();
                                parameters.putString("fields", "id,last_name,first_name,email");
                                request.setParameters(parameters);
                                request.executeAsync();
                            }

                            @Override
                            public void onCancel() {
                                System.out.println("onCancel");
                            }

                            @Override
                            public void onError(FacebookException exception) {
                                System.out.println("onError");
                                Log.d(TAG, exception.getCause().toString());
                            }
                        });


                loginButton.performClick();
            }
        });
        mLayoutLoginMenu.findViewById(R.id.img_phone_login).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                // show login
                mFacebookId = null;
                mStep = STEP_LOGIN_PHONE;
                updateUI();
            }
        });
        mLayoutLoginMenu.findViewById(R.id.txt_register).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                mFacebookId = null;
                mStep = STEP_REGISTER;
                updateUI();
            }
        });


        // register by phone
        mLayoutRegister = findViewById(R.id.layout_register);
        mLayoutRegister.findViewById(R.id.layout_btn_register).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                String lastname = ((TextView)mLayoutRegister.findViewById(R.id.edit_text_lastname)).getText().toString();
                String firstname = ((TextView)mLayoutRegister.findViewById(R.id.edit_text_firstname)).getText().toString();
                String email = ((TextView)mLayoutRegister.findViewById(R.id.edit_text_email)).getText().toString();
                final String mobileNo = ((TextView)mLayoutRegister.findViewById(R.id.edit_text_tel)).getText().toString();
                ImageView btnCheckbox = (ImageView)mLayoutRegister.findViewById(R.id.btn_register_tc);

                if (btnCheckbox.isSelected()) {
                    showLoadingScreen(true, null, Util.getString(R.string.msg_loading));

                    RequestBody rbLastname = RequestBody.create(MediaType.parse("text/plain"), lastname);
                    RequestBody rbFirstname = RequestBody.create(MediaType.parse("text/plain"), firstname);
                    RequestBody rbEmail = RequestBody.create(MediaType.parse("text/plain"), email);
                    final RequestBody rbMobileNo = RequestBody.create(MediaType.parse("text/plain"), mobileNo);
                    RequestBody rbDeviceId = RequestBody.create(MediaType.parse("text/plain"), GcmManager.sharedInstance().gcm_token);
                    RequestBody rbDeviceType = RequestBody.create(MediaType.parse("text/plain"), "2");

                    final MemberService service = ServiceManager.sharedInstance().getServiceHandler().create(MemberService.class);
                    Call<ResponseWrapper<Profile>> call = service.postCreateMember(rbDeviceId, rbDeviceType, rbMobileNo, rbLastname, rbFirstname, rbEmail);
                    call.enqueue(new ResponseHandler<ResponseWrapper<Profile>>() {
                        @Override
                        public void onResponseParsed(Call<ResponseWrapper<Profile>> call, Response<ResponseWrapper<Profile>> response) {
                            Log.e(TAG, "postCreateMember " + String.valueOf(response.body() != null));

                            if (response.body() != null) {
                                mMobileNo = mobileNo;
                                Call<ResponseWrapper<Profile>> callSms = service.postNormalLoginSendSms(rbMobileNo);
                                callSms.enqueue(new ResponseHandler<ResponseWrapper<Profile>>() {
                                    @Override
                                    public void onResponseParsed(Call<ResponseWrapper<Profile>> call, Response<ResponseWrapper<Profile>> response) {
                                        Log.e(TAG, "postNormalLoginSendSms " + String.valueOf(response.body() != null));
                                        showLoadingScreen(false, null, null);
                                        mStep = STEP_SMS_VERIFY;
                                        updateUI();
                                    }

                                    @Override
                                    public void onFailure(Call<ResponseWrapper<Profile>> call, Throwable t) {
                                        showLoadingScreen(false, null, null);
                                        onShowMessage(Util.getString(R.string.msg_error), null, null);
                                    }
                                });
                            } else {
                                showLoadingScreen(false, null, null);
                                onShowMessage(Util.getString(R.string.msg_error), null, null);
                            }
                        }

                        @Override
                        public void onFailure(Call<ResponseWrapper<Profile>> call, Throwable t) {
                            showLoadingScreen(false, null, null);
                            onShowMessage(Util.getString(R.string.msg_error), null, null);
                        }
                    });
                }
                else{
                    onShowMessage(Util.getString(R.string.login_agree_tc), Util.getString(R.string.msg_ok), null);
                }
            }
        });
        mLayoutRegister.findViewById(R.id.layout_register_tc).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                ImageView btnCheckbox = (ImageView)mLayoutRegister.findViewById(R.id.btn_register_tc);
                btnCheckbox.setSelected(!btnCheckbox.isSelected());
            }
        });


        // login by phone
        mLayoutLoginPhone = findViewById(R.id.layout_login_phone);
        mLayoutLoginPhone.findViewById(R.id.layout_login_phone_tc).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                ImageView btnCheckbox = (ImageView)mLayoutLoginPhone.findViewById(R.id.btn_login_phone_tc);
                btnCheckbox.setSelected(!btnCheckbox.isSelected());
            }
        });
        mLayoutLoginPhone.findViewById(R.id.layout_btn_login_phone).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                final String mobileNo = ((TextView)mLayoutLoginPhone.findViewById(R.id.edit_login_phone)).getText().toString();
                ImageView btnCheckbox = (ImageView)mLayoutLoginPhone.findViewById(R.id.btn_login_phone_tc);

                if (mobileNo.length() == 8 && btnCheckbox.isSelected()) {
                    mMobileNo = mobileNo;

                    showLoadingScreen(true, null, Util.getString(R.string.msg_loading));

                    final RequestBody rbMobileNo = RequestBody.create(MediaType.parse("text/plain"), mobileNo);

                    final MemberService service = ServiceManager.sharedInstance().getServiceHandler().create(MemberService.class);

                    if (mFacebookId == null) {
                        Call<ResponseWrapper<Profile>> callSms = service.postNormalLoginSendSms(rbMobileNo);
                        callSms.enqueue(new ResponseHandler<ResponseWrapper<Profile>>() {
                            @Override
                            public void onResponseParsed(Call<ResponseWrapper<Profile>> call, Response<ResponseWrapper<Profile>> response) {
                                Log.e(TAG, "postNormalLoginSendSms " + String.valueOf(response.body() != null));
                                showLoadingScreen(false, null, null);
                                mStep = STEP_SMS_VERIFY;
                                updateUI();
                            }

                            @Override
                            public void onFailure(Call<ResponseWrapper<Profile>> call, Throwable t) {showLoadingScreen(false, null, null);
                                onShowMessage(Util.getString(R.string.msg_error), null, null);
                            }
                        });
                    }
                    else{
                        // TODO facebook login or register
                        final RequestBody rbFacebookId = RequestBody.create(MediaType.parse("text/plain"), mFacebookId);

//                        final Runnable runnableSendSMS = new Runnable() {
//                            @Override
//                            public void run() {
//                                Call<ResponseWrapper<Profile>> callFbLogin = service.postFbLoginSendSms(rbFacebookId);
//                                callFbLogin.enqueue(new ResponseHandler<ResponseWrapper<Profile>>() {
//                                    @Override
//                                    public void onResponseParsed(Call<ResponseWrapper<Profile>> call, Response<ResponseWrapper<Profile>> response) {
//                                        if (response.body() != null){
//                                            Log.e(TAG, "postFbLogin " + String.valueOf(response.body() != null));
//                                            showLoadingScreen(false, null, null);
//                                            mStep = STEP_SMS_VERIFY;
//                                            updateUI();
//                                        }
//                                        else{
//                                            showLoadingScreen(false, null, null);
//                                            onShowMessage(Util.getString(R.string.msg_error), null, null);
//                                        }
//                                    }
//
//                                    @Override
//                                    public void onFailure(Call<ResponseWrapper<Profile>> call, Throwable t) {
//                                        showLoadingScreen(false, null, null);
//                                        onShowMessage(Util.getString(R.string.msg_error), null, null);
//                                    }
//                                });
//                            }
//                        };

                        final Runnable runnableRegister = new Runnable() {
                            @Override
                            public void run() {

                                RequestBody rbLastname = RequestBody.create(MediaType.parse("text/plain"), mFacebookLastname);
                                RequestBody rbFirstname = RequestBody.create(MediaType.parse("text/plain"), mFacebookFirstname);
                                RequestBody rbEmail = RequestBody.create(MediaType.parse("text/plain"), mFacebookEmail);
                                RequestBody rbDeviceId = RequestBody.create(MediaType.parse("text/plain"), GcmManager.sharedInstance().gcm_token);
                                RequestBody rbDeviceType = RequestBody.create(MediaType.parse("text/plain"), "2");
                                final RequestBody rbFacebookId = RequestBody.create(MediaType.parse("text/plain"), mFacebookId);
                                RequestBody rbFacebookToken = RequestBody.create(MediaType.parse("text/plain"), mFacebookToken);

                                final MemberService service = ServiceManager.sharedInstance().getServiceHandler().create(MemberService.class);
                                Call<ResponseWrapper<Profile>> call = service.postCreateFbMember(rbDeviceId, rbDeviceType, rbMobileNo, rbFacebookId, rbFacebookToken, null, rbFirstname, rbLastname, rbEmail);
                                call.enqueue(new ResponseHandler<ResponseWrapper<Profile>>() {
                                    @Override
                                    public void onResponseParsed(Call<ResponseWrapper<Profile>> call, Response<ResponseWrapper<Profile>> response) {
                                        Log.e(TAG, "postCreateMember " + String.valueOf(response.body() != null));

                                        if (response.body() != null) {
                                            mMobileNo = mobileNo;
                                            Call<ResponseWrapper<Profile>> callSms = service.postFbLoginSendSms(rbFacebookId);
                                            callSms.enqueue(new ResponseHandler<ResponseWrapper<Profile>>() {
                                                @Override
                                                public void onResponseParsed(Call<ResponseWrapper<Profile>> call, Response<ResponseWrapper<Profile>> response) {
                                                    Log.e(TAG, "postFbLoginSendSms " + String.valueOf(response.body() != null));
                                                    showLoadingScreen(false, null, null);
                                                    mStep = STEP_SMS_VERIFY;
                                                    updateUI();
                                                }

                                                @Override
                                                public void onFailure(Call<ResponseWrapper<Profile>> call, Throwable t) {
                                                    showLoadingScreen(false, null, null);
                                                    onShowMessage(Util.getString(R.string.msg_error), null, null);
                                                }
                                            });
                                        } else {
                                            showLoadingScreen(false, null, null);
                                            onShowMessage(Util.getString(R.string.msg_error), null, null);
                                        }
                                    }

                                    @Override
                                    public void onFailure(Call<ResponseWrapper<Profile>> call, Throwable t) {
                                        showLoadingScreen(false, null, null);
                                        onShowMessage(Util.getString(R.string.msg_error), null, null);
                                    }
                                });
                            }
                        };

                        runnableRegister.run();
//                        Call<ResponseWrapper<Profile>> callIsFbExist = service.postIsFbExist(rbFacebookId);
//                        callIsFbExist.enqueue(new ResponseHandler<ResponseWrapper<Profile>>() {
//                            @Override
//                            public void onResponseParsed(Call<ResponseWrapper<Profile>> call, Response<ResponseWrapper<Profile>> response) {
////                                if (response.body() != null){
////                                    // TODO registered before
////                                    runnableSendSMS.run();
////                                }
////                                else{
//                                    // TODO going to register
//                                    runnableRegister.run();
////                                }
//                            }
//
//                            @Override
//                            public void onFailure(Call<ResponseWrapper<Profile>> call, Throwable t) {
//                                showLoadingScreen(false, null, null);
//                                onShowMessage(Util.getString(R.string.msg_error), null, null);
//                            }
//                        });
                    }
                }
                else {
                    if (!btnCheckbox.isSelected()){
                        onShowMessage(Util.getString(R.string.login_agree_tc), Util.getString(R.string.msg_ok), null);
                    }
                    else if (mobileNo.length() != 8){
                        onShowMessage(Util.getString(R.string.login_mobile_no_8), Util.getString(R.string.msg_ok), null);
                    }
                }
            }
        });

        // sms verify
        mLayoutSMSVerify = findViewById(R.id.layout_sms_verify);
        mLayoutSMSVerify.findViewById(R.id.layout_btn_sms_verify).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                showLoadingScreen(true, null, Util.getString(R.string.msg_loading));

                String code = ((TextView)mLayoutSMSVerify.findViewById(R.id.edit_sms_verify)).getText().toString();

                if (code.length() > 0) {
                    RequestBody rbMobileNo = RequestBody.create(MediaType.parse("text/plain"), mMobileNo);
                    RequestBody rbVerifyCode = RequestBody.create(MediaType.parse("text/plain"), code);

                    final MemberService service = ServiceManager.sharedInstance().getServiceHandler().create(MemberService.class);
                    Call<ResponseWrapper<Profile>> call = null;
                    if (mFacebookId == null) {
                        call = service.postNormalLogin(rbMobileNo, rbVerifyCode);
                    }
                    else{
                        RequestBody rbFacebookId = RequestBody.create(MediaType.parse("text/plain"), mFacebookId);
                        call = service.postFbLogin(rbFacebookId, rbVerifyCode);
                    }

                    call.enqueue(new ResponseHandler<ResponseWrapper<Profile>>() {
                        @Override
                        public void onResponseParsed(Call<ResponseWrapper<Profile>> call, Response<ResponseWrapper<Profile>> response) {
                            Log.e(TAG, "postNormalLogin " + String.valueOf(response.body() != null));
                            showLoadingScreen(false, null, null);
                            if (response.body() != null) {
                                Profile profile = response.body().data;
                                SessionManager.sharedInstance().setProfile(profile);

                                mStep = STEP_SMS_CONFIRM;
                                updateUI();
                            } else {
                                onShowMessage(Util.getString(R.string.msg_error), null, null);
                            }
                        }

                        @Override
                        public void onFailure(Call<ResponseWrapper<Profile>> call, Throwable t) {
                            showLoadingScreen(false, null, null);
                            onShowMessage(Util.getString(R.string.msg_error), null, null);
                        }
                    });
                }
            }
        });

        // sms confirm
        mLayoutSMSConfirm = findViewById(R.id.layout_sms_confirm);
        mLayoutSMSConfirm.findViewById(R.id.layout_btn_sms_confirm).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                ProfileService service = ServiceManager.sharedInstance().getServiceHandler().create(ProfileService.class);
                String token = GcmManager.sharedInstance().gcm_token;
                Log.i(TAG, "device gcm token " + token);
                RequestBody rbDeviceId = RequestBody.create(MediaType.parse("text/plain"), token);
                RequestBody rbDeviceType = RequestBody.create(MediaType.parse("text/plain"), "2");
                Call<ResponseWrapper<BaseModel>> updateCall = service.postUpdateDeviceTokenApi(rbDeviceId, rbDeviceType);
                final String finalToken = token;
                updateCall.enqueue(new ResponseHandler<ResponseWrapper<BaseModel>>() {
                    @Override
                    public void onResponseParsed(Call<ResponseWrapper<BaseModel>> call, Response<ResponseWrapper<BaseModel>> response) {
                        Log.i(TAG, "update device id request success: " + finalToken);
                    }

                    @Override
                    public void onFailure(Call<ResponseWrapper<BaseModel>> call, Throwable t) {
                        Log.i(TAG, "update device id request fail: " + finalToken);
                    }
                });


                Intent newIntent = new Intent(LoginActivity.this, BaseTabFragmentActivity.class);
                newIntent.addFlags(Intent.FLAG_ACTIVITY_NO_ANIMATION);
                newIntent.putExtra(BaseTabFragmentActivity.INTENT_SHOW_ADV, true);
                Util.moveToActivity(LoginActivity.this, newIntent, false);

                mStep = STEP_MENU;
                updateUI();
            }
        });

        updateUI();

        // FIXME auto login
        if (SessionManager.sharedInstance().getMemberToken().length() > 0){
            showLoadingScreen(true, "", Util.getString(R.string.msg_loading));
            final ProfileService service = ServiceManager.sharedInstance().getServiceHandler().create(ProfileService.class);
            Call<ResponseWrapper<Profile>> call = service.getProfile();
            call.enqueue(new ResponseHandler<ResponseWrapper<Profile>>() {

                @Override
                public void onResponseParsed(Call<ResponseWrapper<Profile>> call, Response<ResponseWrapper<Profile>> response) {

                    showLoadingScreen(false, "", Util.getString(R.string.msg_loading));

                    if (response.body() != null && response.body().data != null) {

                        Profile profile = response.body().data;
                        SessionManager.sharedInstance().setProfile(profile);

                        String token = GcmManager.sharedInstance().gcm_token;
                        Log.i(TAG, "device gcm token " + token);
                        RequestBody rbDeviceId = RequestBody.create(MediaType.parse("text/plain"), token);
                        RequestBody rbDeviceType = RequestBody.create(MediaType.parse("text/plain"), "2");
                        Call<ResponseWrapper<BaseModel>> updateCall = service.postUpdateDeviceTokenApi(rbDeviceId, rbDeviceType);
                        final String finalToken = token;
                        updateCall.enqueue(new ResponseHandler<ResponseWrapper<BaseModel>>() {
                            @Override
                            public void onResponseParsed(Call<ResponseWrapper<BaseModel>> call, Response<ResponseWrapper<BaseModel>> response) {
                                Log.i(TAG, "update device id request success: " + finalToken);
                            }

                            @Override
                            public void onFailure(Call<ResponseWrapper<BaseModel>> call, Throwable t) {
                                Log.i(TAG, "update device id request fail: " + finalToken);
                            }
                        });

                        Intent newIntent = new Intent(LoginActivity.this, BaseTabFragmentActivity.class);
                        newIntent.addFlags(Intent.FLAG_ACTIVITY_NO_ANIMATION);
                        newIntent.putExtra(BaseTabFragmentActivity.INTENT_SHOW_ADV, true);
                        Util.moveToActivity(LoginActivity.this, newIntent, false);
                    }
                }

                @Override
                public void onFailure(Call<ResponseWrapper<Profile>> call, Throwable t) {
                    showLoadingScreen(false, "", Util.getString(R.string.msg_loading));
                    onShowMessage(Util.getString(R.string.msg_error), null, null);
                }
            });
        }
    }

    protected void updateUI(){
        mBtnBack.setVisibility(View.VISIBLE);
        mLayoutLoginMenu.setVisibility(View.GONE);
        mLayoutRegister.setVisibility(View.GONE);
        mLayoutLoginPhone.setVisibility(View.GONE);
        mLayoutSMSVerify.setVisibility(View.GONE);
        mLayoutSMSConfirm.setVisibility(View.GONE);

        if (mStep == STEP_MENU){
            mMobileNo = null;
            mFacebookId = null;
            mLayoutTop.setLayoutParams(new LinearLayout.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, 0, 2));
            mImageTopBg.setImageResource(R.drawable.login_bg_only);
            mLayoutBottom.setLayoutParams(new LinearLayout.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, 0, 1));
            mBtnBack.setOnClickListener(null);
            mLayoutLoginMenu.setVisibility(View.VISIBLE);
            mBtnBack.setVisibility(View.GONE);
        }
        else if (mStep == STEP_LOGIN_PHONE){
            mLayoutTop.setLayoutParams(new LinearLayout.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, 0, 2));
            mImageTopBg.setImageResource(R.drawable.login_bg_only);
            mLayoutBottom.setLayoutParams(new LinearLayout.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, 0, 1));
            mLayoutLoginPhone.setVisibility(View.VISIBLE);
            mBtnBack.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    mStep = STEP_MENU;
                    updateUI();
                }
            });
        }
        else if (mStep == STEP_REGISTER){
            mLayoutTop.setLayoutParams(new LinearLayout.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, 0, 1));
            mImageTopBg.setImageResource(R.drawable.login_bg_only_short);
            mLayoutBottom.setLayoutParams(new LinearLayout.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, 0, 2));
            mLayoutRegister.setVisibility(View.VISIBLE);
            mBtnBack.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    mStep = STEP_MENU;
                    updateUI();
                }
            });
        }
        else if (mStep == STEP_SMS_VERIFY){
            mLayoutTop.setLayoutParams(new LinearLayout.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, 0, 2));
            mImageTopBg.setImageResource(R.drawable.login_bg_only);
            mLayoutBottom.setLayoutParams(new LinearLayout.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, 0, 1));
            mLayoutSMSVerify.setVisibility(View.VISIBLE);
            mBtnBack.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    mStep = STEP_MENU;
                    updateUI();
                }
            });
        }
        else if (mStep == STEP_SMS_CONFIRM){
            mLayoutTop.setLayoutParams(new LinearLayout.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, 0, 2));
            mImageTopBg.setImageResource(R.drawable.login_bg_only);
            mLayoutBottom.setLayoutParams(new LinearLayout.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, 0, 1));
            mLayoutSMSConfirm.setVisibility(View.VISIBLE);
            mBtnBack.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    mStep = STEP_MENU;
                    updateUI();
                }
            });
        }
        else if (mStep == STEP_PHONE_REGISTER){
            mLayoutTop.setLayoutParams(new LinearLayout.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, 0, 2));
            mImageTopBg.setImageResource(R.drawable.login_bg_only);
            mLayoutBottom.setLayoutParams(new LinearLayout.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, 0, 1));
            mLayoutLoginPhone.setVisibility(View.VISIBLE);
            mBtnBack.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    mStep = STEP_MENU;
                    updateUI();
                }
            });
        }
    }

    @Override
    public void onBackPressed() {
        if (mStep == STEP_MENU){
            super.onBackPressed();
        }
        else{
            mBtnBack.callOnClick();
        }
    }


    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        callbackManager.onActivityResult(requestCode, resultCode, data);
    }
}
