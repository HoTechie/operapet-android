package com.hotechie.operapet.service;

import com.hotechie.operapet.service.model.ActionHistory;
import com.hotechie.operapet.service.model.BaseModel;
import com.hotechie.operapet.service.model.Profile;
import com.hotechie.operapet.service.model.ResponseListWrapper;
import com.hotechie.operapet.service.model.ResponseWrapper;

import okhttp3.RequestBody;
import retrofit2.Call;
import retrofit2.http.Field;
import retrofit2.http.FormUrlEncoded;
import retrofit2.http.GET;
import retrofit2.http.Multipart;
import retrofit2.http.POST;
import retrofit2.http.Part;
import retrofit2.http.Path;

/**
 * Created by duncan on 3/10/2016.
 */

public interface MemberService {

    @Multipart
    @POST("normalLoginApi")
    Call<ResponseWrapper<Profile>> postNormalLogin(@Part("mobile_number") RequestBody mobile_number, @Part("verify_code") RequestBody verify_code);

    @Multipart
    @POST("NormalLoginSendSmsApi")
    Call<ResponseWrapper<Profile>> postNormalLoginSendSms(@Part("mobile_number") RequestBody mobile_number);

    @Multipart
    @POST("CreateMemberApi")
    Call<ResponseWrapper<Profile>> postCreateMember(@Part("device_id") RequestBody device_id, @Part("device_type") RequestBody device_type, @Part("mobile_no") RequestBody mobile_no, @Part("lastname") RequestBody lastname, @Part("firstname") RequestBody firstname, @Part("email") RequestBody email);

    @Multipart
    @POST("CreateFbMemberApi")
    Call<ResponseWrapper<Profile>> postCreateFbMember(@Part("device_id") RequestBody device_id,
                                                      @Part("device_type") RequestBody device_type,
                                                      @Part("mobile_no") RequestBody mobile_no,
                                                      @Part("facebook_id") RequestBody Facebook_id,
                                                      @Part("facebook_token") RequestBody facebook_token,
                                                      @Part("refer_code") RequestBody refer_code,
                                                      @Part("firstname") RequestBody firstname,
                                                      @Part("lastname") RequestBody lastname,
                                                      @Part("email") RequestBody email);

    @Multipart
    @POST("isfbexistapi")
    Call<Object> postIsFbExist(@Part("facebook_id") RequestBody facebook_id);

    @Multipart
    @POST("FbLoginApi")
    Call<ResponseWrapper<Profile>> postFbLogin(@Part("facebook_id") RequestBody facebook_id, @Part("verify_code") RequestBody verify_code);

    @Multipart
    @POST("FbLoginSendSmsApi")
    Call<ResponseWrapper<Profile>> postFbLoginSendSms(@Part("facebook_id") RequestBody facebook_id);

    @Multipart
    @POST("RequestFriendApi")
    Call<ResponseWrapper<BaseModel>> postRequestFriend(@Part("target_member_id") RequestBody target_member_id);

    @Multipart
    @POST("AcceptFriendApi")
    Call<ResponseWrapper<BaseModel>> postAcceptFriend(@Part("target_member_id") RequestBody target_member_id);

    @Multipart
    @POST("DeleteFriendApi")
    Call<ResponseWrapper<BaseModel>> postDeleteFriend(@Part("friend_relationship_id") RequestBody friend_relationship_id);

    @GET("GetFriendListApi/{target_member_id}")
    Call<ResponseListWrapper<Profile>> getFriendList(@Path("target_member_id") int target_member_id);

    @Multipart
    @POST("updatememberapi")
    Call<ResponseWrapper<Profile>> postUpdateMember(@Part("\"profile_image\\\"; filename=\\\"profile_image.jpg\\\" \"") RequestBody profile_image, @Part("device_id") RequestBody device_id, @Part("device_type") RequestBody device_type, @Part("mobile_no") RequestBody mobile_no, @Part("lastname") RequestBody lastname, @Part("firstname") RequestBody firstname, @Part("email") RequestBody email);

    @GET("GetFriendRequestListApi")
    Call<ResponseListWrapper<Profile>> getFriendRequestList();

    @Multipart
    @POST("GetSearchFriendResultApi")
    Call<ResponseListWrapper<Profile>> postGetSearchFriendResult(@Part("search_key") RequestBody search_key);

    @Multipart
    @POST("ReferalFriendSendSmsApi")
    Call<ResponseWrapper<BaseModel>> postReferalFriendSendSms(@Part("phone_list_csv") RequestBody phone_list_csv);

    @GET("ActionHistoryApi")
    Call<ResponseListWrapper<ActionHistory>> getActionHistory();

//    @Multipart
//    @POST("UpdateDeviceTokenApi")
//    Call<ResponseWrapper<Object>> postUpdateDeviceTokenApi(@Part("device_id") RequestBody device_id, @Part("device_type") RequestBody device_type);
}
