package com.hotechie.operapet.fragment;

import android.app.Activity;
import android.content.Intent;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.View;
import android.widget.EditText;

import com.hotechie.operapet.InnerPageFragmentActivity;
import com.hotechie.operapet.R;
import com.hotechie.operapet.Util;
import com.hotechie.operapet.service.PetService;
import com.hotechie.operapet.service.PostsService;
import com.hotechie.operapet.service.ResponseHandler;
import com.hotechie.operapet.service.ServiceManager;
import com.hotechie.operapet.service.model.Pet;
import com.hotechie.operapet.service.model.ResponseListWrapper;
import com.hotechie.operapet.service.model.ResponseWrapper;
import com.hotechie.operapet.service.model.Tag;
import com.hotechie.operapet.ui.row_view_factory.RowViewFactoryListAdapter;
import com.hotechie.operapet.ui.row_view_factory.TagRowViewFactory;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import okhttp3.MediaType;
import okhttp3.RequestBody;
import retrofit2.Call;
import retrofit2.Response;

/**
 * Created by duncan on 13/10/2016.
 */

public class SearchTagFragment extends BaseListFragment {
    private static String TAG = "SearchTagFrag";

    public static SearchTagFragment getInstance (){
        SearchTagFragment frag = new SearchTagFragment();
        return frag;
    }

    protected String mSearch = null;
    protected Call<ResponseListWrapper<Tag>> mCall = null;
    protected List<Tag> mTags = new ArrayList<>();

    protected TagRowViewFactory mFactory = null;
    protected TagRowViewFactory.TagRowViewFactoryCallback mFactoryCallback = null;

    protected View mImgNoResult = null;

    protected EditText mEditSearch = null;
    protected View mImgSearchBtnClear = null;

    @Override
    public String getTitle() {
        return Util.getString(R.string.title_search_tag);
    }

    @Override
    public int layoutRes() {
        return R.layout.fragment_serach_tag;
    }

    @Override
    public void configUI() {
        super.configUI();

        mFactoryCallback = new TagRowViewFactory.TagRowViewFactoryCallback() {
            @Override
            public void onClick(Tag tag) {
                // TODO show post list
                PostByTagListFragment frag = PostByTagListFragment.getInstance(tag);
                MenuFragment.setNextContentFragment(frag);
                Activity activity = getActivity();
                if (activity != null && !activity.isFinishing()) {
                    Intent intent = new Intent(activity, InnerPageFragmentActivity.class);
                    Util.moveToActivity(activity, intent, true);
                }
            }
        };
        mFactory = new TagRowViewFactory(mTags, mFactoryCallback);
        mAdapter = new RowViewFactoryListAdapter(mFactory);
        mListView.setAdapter(mAdapter);

        mImgNoResult = getView().findViewById(R.id.img_no_result);

        mEditSearch = (EditText) getView().findViewById(R.id.edit_search);
        mImgSearchBtnClear = getView().findViewById(R.id.img_btn_search_clear);


        mEditSearch.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {
                final String rawText = mEditSearch.getText().toString();
                mSearch = rawText;
                mHandler.postDelayed(new Runnable() {
                    @Override
                    public void run() {
                        String currentText = mEditSearch.getText().toString();
                        if (currentText.equals(rawText) && currentText.length() > 0){
                            Log.i(TAG, "Search");
                            updateData();
                        }
                        else {
                            mTags = new ArrayList<Tag>();
                            mFactory = new TagRowViewFactory(mTags, mFactoryCallback);
                            updateUI();
                        }
                    }
                }, 1000);
            }

            @Override
            public void afterTextChanged(Editable editable) {

            }
        });
        mImgSearchBtnClear.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                mEditSearch.setText("");
                mSearch = "";
            }
        });
    }

    @Override
    protected void updateUIRunnable() {
        super.updateUIRunnable();

        mAdapter.setFactory(mFactory);
        mAdapter.notifyDataSetChanged();

        if (mTags == null || mTags.size() == 0){
            mImgNoResult.setVisibility(View.VISIBLE);
        }
        else{
            mImgNoResult.setVisibility(View.GONE);
        }

    }


    @Override
    public void updateData() {
        super.updateData();

        String search = mSearch;
        if (search != null){

            RequestBody rbSearchKey = RequestBody.create(MediaType.parse("text/plain"), search);

            PostsService service = ServiceManager.sharedInstance().getServiceHandler().create(PostsService.class);
            mCall = service.postGetSearchTagResult(rbSearchKey);
            mCall.enqueue(new ResponseHandler<ResponseListWrapper<Tag>>() {
                @Override
                public void onResponseParsed(Call<ResponseListWrapper<Tag>> call, Response<ResponseListWrapper<Tag>> response) {
                    if (response.body() != null && response.body().data != null){
                        mImgNoResult.setVisibility(View.GONE);

                        mTags = new ArrayList<Tag>(response.body().data);
                        updateUI();
                    }
                    else{
                        mTags = new ArrayList<Tag>();
//                        try {
//                            onShowMessage(response.errorBody().string(), Util.getString(R.string.msg_ok), null);
//                        } catch (IOException e) {
//                            e.printStackTrace();
//                        }
                    }
                    mFactory = new TagRowViewFactory(mTags, mFactoryCallback);
                    updateUI();
                }

                @Override
                public void onFailure(Call<ResponseListWrapper<Tag>> call, Throwable t) {
                    mTags = new ArrayList<Tag>();
                    mFactory = new TagRowViewFactory(mTags, mFactoryCallback);

                    onShowMessage(Util.getString(R.string.msg_error), Util.getString(R.string.msg_ok), null);

                    updateUI();
                }
            });
        }

    }
}
