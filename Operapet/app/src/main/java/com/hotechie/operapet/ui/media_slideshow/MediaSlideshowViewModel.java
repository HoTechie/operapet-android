package com.hotechie.operapet.ui.media_slideshow;

import android.widget.RelativeLayout;

/**
 * Created by duncan on 24/7/2016.
 */
public interface MediaSlideshowViewModel {
    public void fillView(RelativeLayout container);
}
