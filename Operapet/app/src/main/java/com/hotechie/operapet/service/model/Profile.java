package com.hotechie.operapet.service.model;

import java.util.List;

/**
 * Created by duncan on 17/8/2016.
 */
public class Profile extends BaseModel {
    public int id;
    public String user_name;
    public int image_id;
    public String mobile;
    public String email;
    public int member_level;
    public int credit_total;
    public int remain_credit;
    public int no_of_share;
    public int no_of_post;

    public String lastname = "";
    public String firstname = "";

    public List<Pet> pets;
    public FriendRelation friend_relation;

    // GetWhoLikePostapi
    public String like_date;

    // normalLoginApi & FbLoginApi
    public String token;
}
