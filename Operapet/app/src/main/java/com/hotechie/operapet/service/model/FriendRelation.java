package com.hotechie.operapet.service.model;

/**
 * Created by duncan on 14/10/2016.
 */

public class FriendRelation extends BaseModel {
    /*
    {
        "friend_relationship_id": 2,
        "source_id": 33,
        "destination_id": 1,
        "status": 2,
        "upd_date": "0001-01-01T00:00:00",
        "upd_by": 0,
        "crt_date": "0001-01-01T00:00:00",
        "crt_by": 0,
        "record_status": 0
    }
     */

    public int friend_relationship_id;
    public int source_id;
    public int destination_id;
    public int status;
}
