package com.hotechie.operapet.ui.row_view_factory;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.hotechie.operapet.MyApplication;
import com.hotechie.operapet.R;
import com.hotechie.operapet.Util;
import com.hotechie.operapet.service.ServiceManager;
import com.hotechie.operapet.service.model.Gift;
import com.squareup.picasso.Picasso;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

/**
 * Created by duncan on 4/10/2016.
 */

public class GiftRowViewFactory extends RowViewFactory {
    public static interface GiftRowViewFactoryCallback {
        public void onClick(Gift gift);
    }

    protected List<Gift> mGifts= new ArrayList<>();
    protected GiftRowViewFactoryCallback mCallback = null;

    protected SimpleDateFormat originalTimeFormat;
    protected SimpleDateFormat targetTimeFormat;

    protected int mCreditThreshold = 0;
    protected View mInfoView;

    public GiftRowViewFactory(List<Gift> gifts, View infoView, int creditThreshold, final GiftRowViewFactoryCallback callback){
        mGifts = new ArrayList<>(gifts);
        mCallback = callback;
        mInfoView = infoView;
        mCreditThreshold = creditThreshold;

        originalTimeFormat = new SimpleDateFormat("yyyy-MM-dd HH:mm");
        targetTimeFormat = new SimpleDateFormat(Util.getString(R.string.post_datetime_format));
    }

    @Override
    public int getCount() {
        return mGifts.size() + (mInfoView != null ? 1 : 0);
    }

    @Override
    public Object getItem(int index) {
        if (mInfoView != null && index == 0)
            return null;
        return mGifts.get(index - (mInfoView != null ? 1 : 0));
    }

    @Override
    public View getView(final Object object) {
        final Gift gift = (Gift)object;

        if (gift == null){
            ViewGroup parent = (ViewGroup) mInfoView.getParent();
            if (parent != null){
                parent.removeView(mInfoView);
            }
            return mInfoView;
        }

        LayoutInflater inflater = (LayoutInflater)MyApplication.getAppContext().getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        View rowView = inflater.inflate(R.layout.row_gift, null, false);

        ImageView imgLeft = (ImageView)rowView.findViewById(R.id.img_left);
        TextView txtLbNeededMark = (TextView)rowView.findViewById(R.id.txt_lb_needed_mark);
        TextView txtNeededMark = (TextView)rowView.findViewById(R.id.txt_needed_mark);
        TextView txtName = (TextView)rowView.findViewById(R.id.txt_name);
        TextView txtDate = (TextView)rowView.findViewById(R.id.txt_date);
        ImageView img = (ImageView)rowView.findViewById(R.id.img);

        txtNeededMark.setText(String.valueOf(gift.credit));
        txtName.setText(gift.name);
        if (gift.images != null && gift.images.size() > 0){
            Picasso.with(MyApplication.getAppContext()).load(ServiceManager.sharedInstance().getImageUrl(ServiceManager.IMAGE_URL_GIFT, gift.images.get(0))).into(img);
        }
        else{
            img.setImageDrawable(null);
        }

        String createTimeStr = "";
        try {
            Date createTimeDate = originalTimeFormat.parse(gift.create_time);
            createTimeStr = targetTimeFormat.format(createTimeDate);
        } catch (ParseException e) {
            e.printStackTrace();
        }
        txtDate.setText(createTimeStr);

        if (gift.credit > mCreditThreshold || gift.stock == 0){
            imgLeft.setColorFilter(Util.getColor(MyApplication.getAppContext(), R.color.main_text_color_lighter));
            txtLbNeededMark.setTextColor(Util.getColor(MyApplication.getAppContext(), R.color.main_text_color_invert));
            txtNeededMark.setTextColor(Util.getColor(MyApplication.getAppContext(), R.color.main_text_color_invert));
            txtName.setTextColor(Util.getColor(MyApplication.getAppContext(), R.color.main_text_color));

            if (gift.stock == 0){
                rowView.findViewById(R.id.txt_stock).setVisibility(View.VISIBLE);
            }
        }

        if (mCallback != null && gift.credit <= mCreditThreshold && gift.stock > 0){
            rowView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    mCallback.onClick(gift);
                }
            });
        }

        return rowView;
    }

    @Override
    public boolean modifyViewLayout(View contentView, Object object, int index) {
        return false;
    }
}
