package com.hotechie.operapet.service.model;

/**
 * Created by duncan on 17/8/2016.
 */
public class Comment extends BaseModel {
    public int id;
    public int no_of_like;
    public boolean is_like;
    public String description;
    public String create_time;
    public Profile profile;
}
