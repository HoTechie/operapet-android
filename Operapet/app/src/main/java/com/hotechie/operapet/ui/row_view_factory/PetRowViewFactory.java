package com.hotechie.operapet.ui.row_view_factory;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;

import com.hotechie.operapet.Constant;
import com.hotechie.operapet.MyApplication;
import com.hotechie.operapet.R;
import com.hotechie.operapet.Util;
import com.hotechie.operapet.service.ServiceManager;
import com.hotechie.operapet.service.model.Pet;
import com.squareup.picasso.Picasso;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by duncan on 2/10/2016.
 */

public class PetRowViewFactory extends RowViewFactory {
    public static interface  PetRowViewFactoryCallback{
        public void onClick(Pet pet); // pet is null -> create
    }

    public static int PET_PER_ROW = 3;

    protected boolean mCanCreate = false;

    protected List<Pet> mPets= new ArrayList<>();
    protected PetRowViewFactory.PetRowViewFactoryCallback mCallback = null;

    protected SimpleDateFormat originalTimeFormat;
    protected SimpleDateFormat targetTimeFormat;

    public PetRowViewFactory(List<Pet> pets, final PetRowViewFactoryCallback callback, boolean canCreate){
        mPets = new ArrayList<>(pets);
        mCallback = callback;

        mCanCreate = canCreate;

        if (mCanCreate) {
            Pet dummyPet = new Pet();
            dummyPet.id = -1;
            mPets.add(dummyPet);
        }

        originalTimeFormat = new SimpleDateFormat("yyyy-MM-dd HH:mm");
        targetTimeFormat = new SimpleDateFormat(Util.getString(R.string.post_datetime_format));
    }

    @Override
    public int getCount() {
        return (int)Math.ceil(mPets.size() / (float)PET_PER_ROW);
    }

    @Override
    public Object getItem(int index) {
        List<Pet> pets = new ArrayList<>();
        for (int i = index * PET_PER_ROW; i < (index + 1) * PET_PER_ROW && i < mPets.size(); i ++){
            pets.add(mPets.get(i));
        }
        return pets;
    }

    @Override
    public View getView(final Object object) {
        LinearLayout rowView = new LinearLayout(MyApplication.getAppContext());
        rowView.setLayoutParams(new ViewGroup.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.WRAP_CONTENT));
        rowView.setWeightSum(PET_PER_ROW);

        List<Pet> pets = (List<Pet>)object;
        LayoutInflater inflater = (LayoutInflater)MyApplication.getAppContext().getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        for (int i = 0; i < PET_PER_ROW && i < pets.size(); i ++){
            final Pet pet = pets.get(i);
            final View cell = inflater.inflate(R.layout.row_cell_pet, rowView, false);

            final ImageView imgView = (ImageView) cell.findViewById(R.id.img_view);
            final ImageView imgGender = (ImageView) cell.findViewById(R.id.img_gender);

            if (pet.id == -1){ // new pet
                imgView.setImageResource(R.drawable.btn_pet_add);
                imgGender.setVisibility(View.GONE);
            }
            else {
                Picasso.with(MyApplication.getAppContext()).load(ServiceManager.sharedInstance().getImageUrl(ServiceManager.IMAGE_URL_PET, pet.image_id)).into(imgView);
                imgGender.setColorFilter(Util.getColor(MyApplication.getAppContext(), pet.gender == Constant.GENDER_MALE ? R.color.pet_boy : R.color.pet_girl));
            }

            rowView.addView(cell, new LinearLayout.LayoutParams(0, ViewGroup.LayoutParams.WRAP_CONTENT, 1));


            if (mCallback != null){
                cell.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        if (pet.id == -1)
                            mCallback.onClick(null);
                        else
                            mCallback.onClick(pet);
                    }
                });
            }
        }


        return rowView;
    }

}
