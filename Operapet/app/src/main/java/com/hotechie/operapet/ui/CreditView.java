package com.hotechie.operapet.ui;

import android.content.Context;
import android.content.res.Resources;
import android.graphics.Canvas;
import android.graphics.Paint;
import android.graphics.RectF;
import android.util.AttributeSet;
import android.util.TypedValue;
import android.view.View;

import com.hotechie.operapet.MyApplication;
import com.hotechie.operapet.R;
import com.hotechie.operapet.Util;

/**
 * Created by duncan on 3/12/2016.
 */

public class CreditView extends View {
    private static String TAG = "CreditView";

    protected float mPercent = 0.0f; // 0 - 1

//    private Paint mTrackPaint;
    private Paint mProgressPaint;
    protected int w = 0, h = 0;

    protected final static int StokeWidthDp = 15;
    protected int mStokeWidthHalf = 0;

    public CreditView(Context context) {
        super(context);
        init();
    }

    public CreditView(Context context, AttributeSet attrs) {
        super(context, attrs);
        init();
    }

    public CreditView(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        init();
    }

    @Override
    protected void onSizeChanged(int w, int h, int oldw, int oldh) {
        this.w = w;
        this.h = h;
        super.onSizeChanged(w, h, oldw, oldh);
    }

    protected void init(){


        Resources r = getResources();
        mStokeWidthHalf = (int)TypedValue.applyDimension(TypedValue.COMPLEX_UNIT_DIP, StokeWidthDp, r.getDisplayMetrics());


//        mTrackPaint = new Paint(Paint.ANTI_ALIAS_FLAG);
//        mTrackPaint.setStyle(Paint.Style.STROKE);
//        mTrackPaint.setColor(Util.getColor(getContext(), R.color.theme_color_lighter));
//        mTrackPaint.setStrokeWidth(mStokeWidthHalf * 2);


        mProgressPaint = new Paint(Paint.ANTI_ALIAS_FLAG);
        mProgressPaint.setStyle(Paint.Style.STROKE);
        mProgressPaint.setColor(Util.getColor(getContext(), R.color.theme_color));
        mProgressPaint.setStrokeWidth(mStokeWidthHalf * 2);
    }

    @Override
    protected void onDraw(Canvas canvas) {
        super.onDraw(canvas);

        Resources r = getResources();
        RectF rectF = new RectF(mStokeWidthHalf, mStokeWidthHalf, w - mStokeWidthHalf, h * 2 - mStokeWidthHalf * 2);
//        canvas.drawArc(rectF, 180, 180, false, mTrackPaint);

        canvas.drawArc(rectF, 180, mPercent * 180.0f, false, mProgressPaint);
    }


    @Override
    protected void onMeasure(int widthMeasureSpec, int heightMeasureSpec) {
        int width = MeasureSpec.getSize(widthMeasureSpec);
        int height = width / 2;
        setMeasuredDimension(width, height);
    }


    public void setPercentage(float percentage){
        this.mPercent = percentage;
        invalidate();
    }
}
