package com.hotechie.operapet.ui.row_view_factory;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.hotechie.operapet.MyApplication;
import com.hotechie.operapet.R;
import com.hotechie.operapet.Util;
import com.hotechie.operapet.service.ServiceManager;
import com.hotechie.operapet.service.model.Pet;
import com.hotechie.operapet.service.model.Profile;
import com.squareup.picasso.Picasso;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

/**
 * Created by duncan on 3/10/2016.
 */

public class UserRowViewFactory extends RowViewFactory {
    protected List<Profile> mUsers = new ArrayList<>();

    public interface UserRowViewFactoryCallback{
        public void onClick(Profile profile);
    }

    protected SimpleDateFormat originalTimeFormat;
    protected SimpleDateFormat targetTimeFormat;

    protected boolean mShowDate = false;

    protected int mRowViewRes = R.layout.row_user_like;

    protected UserRowViewFactoryCallback mCallback = null;

    public UserRowViewFactory(List<Profile> users) {
        mUsers = new ArrayList<>(users);

        mShowDate = true;

        originalTimeFormat = new SimpleDateFormat("yyyy-MM-dd HH:mm");
        targetTimeFormat = new SimpleDateFormat(Util.getString(R.string.post_datetime_format));
    }

    public UserRowViewFactory(List<Profile> users, boolean showDate) {
        mUsers = new ArrayList<>(users);

        mShowDate = showDate;

        originalTimeFormat = new SimpleDateFormat("yyyy-MM-dd HH:mm");
        targetTimeFormat = new SimpleDateFormat(Util.getString(R.string.post_datetime_format));
    }

    public UserRowViewFactory(List<Profile> users, boolean showDate, UserRowViewFactoryCallback callback) {
        mUsers = new ArrayList<>(users);

        mShowDate = showDate;

        originalTimeFormat = new SimpleDateFormat("yyyy-MM-dd HH:mm");
        targetTimeFormat = new SimpleDateFormat(Util.getString(R.string.post_datetime_format));

        mCallback = callback;
    }

    @Override
    public int getCount() {
        return mUsers.size();
    }

    @Override
    public Object getItem(int index) {
        return mUsers.get(index);
    }

    @Override
    public View getView(final Object object) {
        LayoutInflater inflater = (LayoutInflater)MyApplication.getAppContext().getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        View rowView = inflater.inflate(mRowViewRes, null, false);

        final Profile user = (Profile)object;
        TextView txtName = (TextView) rowView.findViewById(R.id.txt_name);
        TextView txtDate = (TextView) rowView.findViewById(R.id.txt_date);
        ImageView imgProfile = (ImageView) rowView.findViewById(R.id.img_profile);
        LinearLayout layoutPets = (LinearLayout) rowView.findViewById(R.id.layout_pets);
        if (user != null) {

            txtName.setText(user.user_name);

            Picasso.with(MyApplication.getAppContext()).load(ServiceManager.sharedInstance().getImageUrl(ServiceManager.IMAGE_URL_PROFILE, user.image_id)).into(imgProfile);

            layoutPets.removeAllViews();
            if (user.pets != null) {
                for (Pet pet : user.pets) {
                    View petCellView = inflater.inflate(R.layout.view_user_list_pet, null, false);
                    ImageView imgPetCellView = (ImageView) petCellView.findViewById(R.id.img_view);
                    Picasso.with(MyApplication.getAppContext()).load(ServiceManager.sharedInstance().getImageUrl(ServiceManager.IMAGE_URL_PET, pet.image_id)).into(imgPetCellView);
                    layoutPets.addView(petCellView, new LinearLayout.LayoutParams(ViewGroup.LayoutParams.WRAP_CONTENT, ViewGroup.LayoutParams.MATCH_PARENT));
                }
            }

            if (txtDate != null) {
                if (mShowDate) {
                    txtDate.setVisibility(View.VISIBLE);
                    if (user.like_date != null) {

                        String createTimeStr = "";
                        try {
                            Date createTimeDate = originalTimeFormat.parse(user.like_date);
                            createTimeStr = targetTimeFormat.format(createTimeDate);
                        } catch (ParseException e) {
                            e.printStackTrace();
                        }
                        txtDate.setText(createTimeStr);
                    } else {
                        txtDate.setText("");
                    }
                } else {
                    txtDate.setVisibility(View.GONE);
                }
            }

        }
        else{
            txtName.setText("");

            if (txtDate != null) {
                txtDate.setText("");
            }
        }

        if (mCallback != null){
            rowView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    mCallback.onClick(user);
                }
            });
        }


        return rowView;
    }
}