package com.hotechie.operapet.service;

import com.hotechie.operapet.service.model.BaseModel;
import com.hotechie.operapet.service.model.Comment;
import com.hotechie.operapet.service.model.Post;
import com.hotechie.operapet.service.model.Profile;
import com.hotechie.operapet.service.model.ResponseListWrapper;
import com.hotechie.operapet.service.model.ResponseWrapper;
import com.hotechie.operapet.service.model.Tag;

import okhttp3.RequestBody;
import retrofit2.Call;
import retrofit2.http.GET;
import retrofit2.http.Multipart;
import retrofit2.http.POST;
import retrofit2.http.Part;
import retrofit2.http.Path;

/**
 * Created by duncan on 17/8/2016.
 */
public interface PostsService {
    @GET("allpostapi")
    Call<ResponseListWrapper<Post>> getPosts();

    @GET("CategoryPostApi/{category_id}")
    Call<ResponseListWrapper<Post>> getPostsByCategory(@Path("category_id")int category_id);

    @GET("allpostapi/{member_id}")
    Call<ResponseListWrapper<Post>> getPostsByMember(@Path("member_id")int member_id);

    @Multipart
    @POST("GetSearchTagResultApi")
    Call<ResponseListWrapper<Tag>> postGetSearchTagResult(@Part("search_key")RequestBody search_key);

    @GET("GetPostByTagIdApi/{post_tag_id}")
    Call<ResponseListWrapper<Post>> getPostByTagId(@Path("post_tag_id")int post_tag_id);

    @GET("PostDetailApi/{post_id}")
    Call<ResponseWrapper<Post>> getPostDetail(@Path("post_id")int post_id);

    @GET("GetWhoLikePostapi/{post_id}")
    Call<ResponseListWrapper<Profile>> getWhoLikePost(@Path("post_id")int post_id);

    @Multipart
    @POST("CreateCommentApi")
    Call<ResponseWrapper<Comment>> postCreateComment(@Part("description")RequestBody description,@Part("post_id")RequestBody post_id);

    @Multipart
    @POST("CreateLikeApi")
    Call<ResponseWrapper<BaseModel>> postCreateLike(@Part("post_id")RequestBody post_id);

    @Multipart
    @POST("CreateCommentLikeApi")
    Call<ResponseWrapper<BaseModel>> postCreateCommentLike(@Part("comment_id")RequestBody comment_id);

    @Multipart
    @POST("CreateShareApi")
    Call<ResponseWrapper<BaseModel>> postCreateShare(@Part("post_id")RequestBody post_id);

    @Multipart
    @POST("CreatePostApi")
    Call<ResponseWrapper<Post>> postCreatePost(@Part("description")RequestBody description,
                                               @Part("category_id")RequestBody category_id,
                                               @Part("tags_list_string")RequestBody tags_list_string,
                                               @Part("\"img1\\\"; filename=\\\"img1.jpg\\\" \"")RequestBody img_1,
                                               @Part("\"img2\\\"; filename=\\\"img2.jpg\\\" \"")RequestBody img_2,
                                               @Part("\"img3\\\"; filename=\\\"img3.jpg\\\" \"")RequestBody img_3,
                                               @Part("\"img4\\\"; filename=\\\"img4.jpg\\\" \"")RequestBody img_4,
                                               @Part("\"img5\\\"; filename=\\\"img5.jpg\\\" \"")RequestBody img_5);
}
