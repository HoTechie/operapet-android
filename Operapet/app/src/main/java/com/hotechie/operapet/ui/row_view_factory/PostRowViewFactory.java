package com.hotechie.operapet.ui.row_view_factory;

import android.content.Context;
import android.content.Intent;
import android.content.res.Resources;
import android.net.Uri;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.hotechie.operapet.Constant;
import com.hotechie.operapet.MyApplication;
import com.hotechie.operapet.R;
import com.hotechie.operapet.Util;
import com.hotechie.operapet.service.ServiceManager;
import com.hotechie.operapet.service.model.Adv;
import com.hotechie.operapet.service.model.Post;
import com.hotechie.operapet.service.model.Profile;
import com.hotechie.operapet.service.model.Tag;
import com.squareup.picasso.Picasso;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

/**
 * Created by duncan on 17/8/2016.
 */
public class PostRowViewFactory extends RowViewFactory {
    public static interface  PostRowViewFactoryCallback{
        public void onClick(Post post);
        public void onClickLike(Post post);
        public void onClickShare(Post post);
        public void onClickTag(Tag tag);
        public void onClickProfile(Profile profile);
    }

    protected List<Post> mPosts= new ArrayList<>();
    protected Adv mAdv = null;
    protected PostRowViewFactoryCallback mCallback = null;

    protected SimpleDateFormat originalTimeFormat;
    protected SimpleDateFormat targetTimeFormat;

    public PostRowViewFactory(List<Post> posts, final PostRowViewFactoryCallback callback){
        mPosts = new ArrayList<>(posts);
        mCallback = callback;


        originalTimeFormat = new SimpleDateFormat("yyyy-MM-dd HH:mm");
        targetTimeFormat = new SimpleDateFormat(Util.getString(R.string.post_datetime_format));
    }

    public PostRowViewFactory(List<Post> posts, final PostRowViewFactoryCallback callback, Adv adv){
        mAdv = adv;

        mPosts = new ArrayList<>(posts);
        mCallback = callback;


        originalTimeFormat = new SimpleDateFormat("yyyy-MM-dd HH:mm");
        targetTimeFormat = new SimpleDateFormat(Util.getString(R.string.post_datetime_format));
    }


    @Override
    public int getCount() {
        if (mAdv == null)
            return mPosts.size();

        if (mPosts.size() > 35){
            return mPosts.size() + 4;
        }
        else if (mPosts.size() > 25){
            return mPosts.size() + 3;
        }
        else if (mPosts.size() > 15){
            return mPosts.size() + 2;
        }
        else if (mPosts.size() > 5){
            return mPosts.size() + 1;
        }
        else {
            return mPosts.size();
        }
    }

    @Override
    public Object getItem(int index) {
//        return mPosts.get(index);

        if (mAdv == null)
            return mPosts.get(index);

        if (index == 5 || index == 16 || index == 27 || index == 38)
            return null;

        if (index > 35){
            return mPosts.get(index - 4);
        }
        else if (index > 25){
            return mPosts.get(index - 3);
        }
        else if (index > 15){
            return mPosts.get(index - 2);
        }
        else if (index > 5){
            return mPosts.get(index - 1);
        }
        else {
            return mPosts.get(index);
        }
    }

    @Override
    public View getView(final Object object) {
        LayoutInflater inflater = (LayoutInflater) MyApplication.getAppContext().getSystemService(Context.LAYOUT_INFLATER_SERVICE);

        if (object == null){
            // TODO Adv
            View rowView = inflater.inflate(R.layout.view_adv, null, false);
//            ImageView advView = new ImageView(MyApplication.getAppContext());
            ImageView advView = (ImageView)rowView.findViewById(R.id.img_adv);
            Picasso.with(MyApplication.getAppContext()).load(ServiceManager.sharedInstance().getImageUrl(ServiceManager.IMAGE_URL_ADV, mAdv.image_id)).into(advView);
            advView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    Intent i = new Intent(Intent.ACTION_VIEW);
                    i.setData(Uri.parse(mAdv.link));
                    i.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                    MyApplication.getAppContext().startActivity(i);
                }
            });
            return rowView;
        }

        final Post post = (Post)object;
        View rowView = inflater.inflate(R.layout.row_post, null, false);

        ImageView imgCat = (ImageView)rowView.findViewById(R.id.img_cat);
        TextView txtCat = (TextView)rowView.findViewById(R.id.txt_cat);
        View viewSeparator = rowView.findViewById(R.id.view_separator);
        ImageView imgProfile = (ImageView)rowView.findViewById(R.id.img_profile);
        TextView txtProfileName = (TextView)rowView.findViewById(R.id.txt_profile_name);
        TextView txtProfileLevel = (TextView)rowView.findViewById(R.id.txt_profile_level);
        TextView txtDate = (TextView)rowView.findViewById(R.id.txt_date);
        TextView txtDescription = (TextView)rowView.findViewById(R.id.txt_description);
        LinearLayout layoutPostImage = (LinearLayout)rowView.findViewById(R.id.layout_post_img);
        ImageView imgPost = (ImageView)rowView.findViewById(R.id.img_post);
        ImageView imgPost2 = (ImageView)rowView.findViewById(R.id.img_post_2);
        ImageButton btnLike = (ImageButton)rowView.findViewById(R.id.btn_like);
        ImageButton btnComment = (ImageButton)rowView.findViewById(R.id.btn_comment);
        ImageButton btnShare = (ImageButton)rowView.findViewById(R.id.btn_share);
        TextView txtLike = (TextView)rowView.findViewById(R.id.txt_like);
        TextView txtComment = (TextView)rowView.findViewById(R.id.txt_comment);
        ImageView imgActionBar = (ImageView)rowView.findViewById(R.id.img_action_bar);
        LinearLayout layoutTag = (LinearLayout)rowView.findViewById(R.id.layout_tag);

        String createTimeStr = "";
        try {
            Date createTimeDate = originalTimeFormat.parse(post.create_time);
            createTimeStr = targetTimeFormat.format(createTimeDate);
        } catch (ParseException e) {
            e.printStackTrace();
        }

        txtCat.setText(post.category_name);
        Picasso.with(MyApplication.getAppContext()).load(ServiceManager.sharedInstance().getImageUrl(ServiceManager.IMAGE_URL_PROFILE, post.profile.image_id)).into(imgProfile);
        txtProfileName.setText(post.profile.user_name);
        txtProfileLevel.setText(Constant.getMemberLevelStr(post.profile.member_level));
        txtDate.setText(createTimeStr);
        txtDescription.setText(post.description);
        if (post.images == null || post.images.size() == 0){
            layoutPostImage.setVisibility(View.GONE);
        }
        else {
//            if (post.images.size() == 1){
//                layoutPostImage.setWeightSum(1);
////                ((LinearLayout.LayoutParams)imgPost.getLayoutParams()).weight = 1;
//                imgPost2.setVisibility(View.GONE);
//            }
//            else {
//                layoutPostImage.setWeightSum(2);
//                imgPost.setVisibility(View.VISIBLE);
//                imgPost2.setVisibility(View.VISIBLE);
//                Picasso.with(MyApplication.getAppContext()).load(ServiceManager.sharedInstance().getImageUrl(post.images.get(1))).into(imgPost2);
//            }

            layoutPostImage.setWeightSum(1);
            imgPost2.setVisibility(View.GONE);
            Picasso.with(MyApplication.getAppContext()).load(ServiceManager.sharedInstance().getImageUrl(ServiceManager.IMAGE_URL_POST, post.images.get(0))).into(imgPost);
        }
        btnLike.setSelected(post.is_like);
        txtLike.setText(String.format(Util.getString(R.string.post_like_format), post.no_of_like));
        txtComment.setText(String.format(Util.getString(R.string.post_comment_format), post.no_of_comment));


        View.OnClickListener tagOnClickListener = new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Tag tag = (Tag)view.getTag();
                if (tag != null){
                    if (mCallback != null){
                        mCallback.onClickTag(tag);
                    }
                }
            }
        };
        for (int i = 0; i < post.tags.size(); i ++){
            Tag tag = post.tags.get(i);
            View view = inflater.inflate(R.layout.cell_tag, null, false);
            ((TextView)view.findViewById(R.id.txt_tag)).setText(tag.description);
            view.setTag(tag);
            layoutTag.addView(view);

            view.setOnClickListener(tagOnClickListener);
        }

        int color = 0;
        int colorDarker = 0;

        if (post.category_id == Constant.POST_CAT_1){
            color = Util.getColor(MyApplication.getAppContext(), R.color.theme_color);
            colorDarker = Util.getColor(MyApplication.getAppContext(), R.color.theme_color_darker);
            imgCat.setImageResource(R.drawable.icon_cat_1);
        }
        else if (post.category_id == Constant.POST_CAT_2){
            color = Util.getColor(MyApplication.getAppContext(), R.color.theme_color_2);
            colorDarker = Util.getColor(MyApplication.getAppContext(), R.color.theme_color_2_darker);
            imgCat.setImageResource(R.drawable.icon_cat_2);
        }
        else if (post.category_id == Constant.POST_CAT_3){
            color = Util.getColor(MyApplication.getAppContext(), R.color.theme_color_3);
            colorDarker = Util.getColor(MyApplication.getAppContext(), R.color.theme_color_3_darker);
            imgCat.setImageResource(R.drawable.icon_cat_3);
        }
        else if (post.category_id == Constant.POST_CAT_4){
            color = Util.getColor(MyApplication.getAppContext(), R.color.theme_color_4);
            colorDarker = Util.getColor(MyApplication.getAppContext(), R.color.theme_color_4_darker);
            imgCat.setImageResource(R.drawable.icon_cat_4);
        }
        else if (post.category_id == Constant.POST_CAT_5){
            color = Util.getColor(MyApplication.getAppContext(), R.color.theme_color_5);
            colorDarker = Util.getColor(MyApplication.getAppContext(), R.color.theme_color_5_darker);
            imgCat.setImageResource(R.drawable.icon_cat_5);
        }
        else if (post.category_id == Constant.POST_CAT_6){
            color = Util.getColor(MyApplication.getAppContext(), R.color.theme_color_6);
            colorDarker = Util.getColor(MyApplication.getAppContext(), R.color.theme_color_6_darker);
            imgCat.setImageResource(R.drawable.icon_cat_6);
        }
        else if (post.category_id == Constant.POST_CAT_7){
            color = Util.getColor(MyApplication.getAppContext(), R.color.theme_color_7);
            colorDarker = Util.getColor(MyApplication.getAppContext(), R.color.theme_color_7_darker);
            imgCat.setImageResource(R.drawable.icon_cat_7);
        }
        else if (post.category_id == Constant.POST_CAT_8){
            color = Util.getColor(MyApplication.getAppContext(), R.color.theme_color_8);
            colorDarker = Util.getColor(MyApplication.getAppContext(), R.color.theme_color_8_darker);
            imgCat.setImageResource(R.drawable.icon_cat_8);
        }
        else if (post.category_id == Constant.POST_CAT_9){
            color = Util.getColor(MyApplication.getAppContext(), R.color.theme_color_9);
            colorDarker = Util.getColor(MyApplication.getAppContext(), R.color.theme_color_9_darker);
            imgCat.setImageResource(R.drawable.icon_cat_9);
        }

        txtCat.setTextColor(color);
        imgCat.setColorFilter(color);
        viewSeparator.setBackgroundColor(color);
        imgActionBar.setColorFilter(color);
        btnComment.setColorFilter(colorDarker);
        btnShare.setColorFilter(colorDarker);
        txtLike.setTextColor(colorDarker);
        txtComment.setTextColor(colorDarker);
        if (!post.is_like){
            btnLike.setColorFilter(colorDarker);
        }

        if (mCallback != null){
            rowView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    mCallback.onClick(post);
                }
            });

            btnLike.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    mCallback.onClickLike(post);
                }
            });

            btnShare.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    mCallback.onClickShare(post);
                }
            });

            imgProfile.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    mCallback.onClickProfile(post.profile);
                }
            });

            txtProfileName.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    mCallback.onClickProfile(post.profile);
                }
            });
        }

        return rowView;
    }

    @Override
    public boolean modifyViewLayout(View contentView, Object object, int index) {

        if (object == null){
            RelativeLayout.LayoutParams params = (RelativeLayout.LayoutParams) contentView.getLayoutParams();
            params.width = RelativeLayout.LayoutParams.MATCH_PARENT;
            params.height = RelativeLayout.LayoutParams.WRAP_CONTENT;
        }
        else {
            Resources resources = MyApplication.getAppContext().getResources();
            RelativeLayout.LayoutParams params = (RelativeLayout.LayoutParams) contentView.getLayoutParams();
            params.setMargins(resources.getDimensionPixelSize(R.dimen.list_row_margin_horizontal), resources.getDimensionPixelSize(R.dimen.list_row_post_margin_top), resources.getDimensionPixelSize(R.dimen.list_row_margin_horizontal), resources.getDimensionPixelSize(R.dimen.list_row_post_margin_bottom));
        }

        return false;

    }
}
