package com.hotechie.operapet.fragment;

import android.app.Activity;
import android.content.DialogInterface;
import android.content.Intent;

import com.hotechie.operapet.InnerPageFragmentActivity;
import com.hotechie.operapet.R;
import com.hotechie.operapet.Util;
import com.hotechie.operapet.service.MemberService;
import com.hotechie.operapet.service.PostsService;
import com.hotechie.operapet.service.ResponseHandler;
import com.hotechie.operapet.service.ServiceManager;
import com.hotechie.operapet.service.model.Profile;
import com.hotechie.operapet.service.model.ResponseListWrapper;
import com.hotechie.operapet.ui.row_view_factory.RowViewFactoryListAdapter;
import com.hotechie.operapet.ui.row_view_factory.UserRowViewFactory;
import com.hotechie.util.MessageDialogFragment;

import java.util.ArrayList;
import java.util.List;

import retrofit2.Call;
import retrofit2.Response;

/**
 * Created by duncan on 15/10/2016.
 */

public class WallFriendListFragment extends BaseListFragment {
    private static String TAG = "WallFriendListFrag";

    public static WallFriendListFragment getInstance(int memberId){
        WallFriendListFragment frag = new WallFriendListFragment();
        frag.mMemberId = memberId;
        return frag;
    }

    protected int mMemberId;

    protected List<Profile> mUsers = new ArrayList<>();
    protected UserRowViewFactory mFactory;
    protected UserRowViewFactory.UserRowViewFactoryCallback mFactoryCallback = null;

    @Override
    public String getTitle() {
        return Util.getString(R.string.wall_friend);
    }

    @Override
    public void onResume() {
        super.onResume();
        updateData();
    }

    @Override
    public void configUI() {
        super.configUI();

        mFactoryCallback = new UserRowViewFactory.UserRowViewFactoryCallback() {
            @Override
            public void onClick(Profile profile) {

                WallFragment frag = WallFragment.getInstance(profile.id);
                MenuFragment.setNextContentFragment(frag);
                Activity activity = getActivity();
                if (activity != null && !activity.isFinishing()) {
                    Intent intent = new Intent(activity, InnerPageFragmentActivity.class);
                    Util.moveToActivity(activity, intent, true);
                }
            }
        };

        mFactory = new UserRowViewFactory(mUsers, false, mFactoryCallback);
        mAdapter = new RowViewFactoryListAdapter(mFactory);
        mListView.setAdapter(mAdapter);
    }

    @Override
    protected void updateUIRunnable() {
        super.updateUIRunnable();

        mAdapter.setFactory(mFactory);
        mAdapter.notifyDataSetChanged();
    }

    @Override
    public void updateData() {
        super.updateData();

        MemberService service = ServiceManager.sharedInstance().getServiceHandler().create(MemberService.class);
        Call<ResponseListWrapper<Profile>> call = service.getFriendList(mMemberId);
        call.enqueue(new ResponseHandler<ResponseListWrapper<Profile>>() {
            @Override
            public void onResponseParsed(Call<ResponseListWrapper<Profile>> call, Response<ResponseListWrapper<Profile>> response) {
                if (response.body() != null && response.body().data != null) {
                    mUsers = new ArrayList<Profile>(response.body().data);
                    mFactory = new UserRowViewFactory(mUsers, false, mFactoryCallback);
                    updateUI();
                }
            }

            @Override
            public void onFailure(Call<ResponseListWrapper<Profile>> call, Throwable t) {
                onHandleError(Util.getString(R.string.msg_error), null, null, new MessageDialogFragment.MessageDialogFragmentCallback() {

                    @Override
                    public void onClickPositiveButton(MessageDialogFragment fragment, DialogInterface dialog, int id) {
                        updateData();
                    }

                    @Override
                    public void onClickNegativeButton(MessageDialogFragment fragment, DialogInterface dialog, int id) {

                    }
                });
            }
        });
    }
}
