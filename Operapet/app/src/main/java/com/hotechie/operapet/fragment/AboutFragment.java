package com.hotechie.operapet.fragment;

import com.hotechie.operapet.R;
import com.hotechie.operapet.Util;

/**
 * Created by duncan on 3/12/2016.
 */

public class AboutFragment extends BaseFragment {
    private static String TAG = "AboutFragment";

    public static AboutFragment getInstance(){
        AboutFragment frag = new AboutFragment();
        return frag;
    }

    @Override
    public String getTitle() {
        return Util.getString(R.string.title_about_us);
    }

    @Override
    public int layoutRes() {
        return R.layout.fragment_about;
    }

    @Override
    public void configUI() {
        super.configUI();
    }
}
