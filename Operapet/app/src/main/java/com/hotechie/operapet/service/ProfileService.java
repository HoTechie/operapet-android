package com.hotechie.operapet.service;

import com.hotechie.operapet.service.model.BaseModel;
import com.hotechie.operapet.service.model.Gift;
import com.hotechie.operapet.service.model.Notification;
import com.hotechie.operapet.service.model.Pet;
import com.hotechie.operapet.service.model.Profile;
import com.hotechie.operapet.service.model.ResponseListWrapper;
import com.hotechie.operapet.service.model.ResponseWrapper;

import okhttp3.RequestBody;
import retrofit2.Call;
import retrofit2.http.GET;
import retrofit2.http.Multipart;
import retrofit2.http.POST;
import retrofit2.http.Part;
import retrofit2.http.Path;

/**
 * Created by duncan on 18/8/2016.
 */
public interface ProfileService {


    @GET("profileapi")
    Call<ResponseWrapper<Profile>> getProfile();

    @GET("profileapi/{member_id}")
    Call<ResponseWrapper<Profile>> getProfile(@Path("member_id") int member_id);

    @GET("allpetapi/{member_id}")
    Call<ResponseListWrapper<Pet>> getAllPet(@Path("member_id")int member_id);

    @GET("gifthistoryapi")
    Call<ResponseListWrapper<Gift>> getGiftHistory();

    @GET("GetNotificationListApi")
    Call<ResponseListWrapper<Notification>> getNotification();

    @Multipart
    @POST("UpdateDeviceTokenApi")
    Call<ResponseWrapper<BaseModel>> postUpdateDeviceTokenApi(@Part("device_id") RequestBody device_id, @Part("device_type")RequestBody device_type);

}
