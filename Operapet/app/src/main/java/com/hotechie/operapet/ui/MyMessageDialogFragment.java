package com.hotechie.operapet.ui;

import android.app.AlertDialog;
import android.app.Dialog;
import android.content.Context;
import android.content.DialogInterface;
import android.os.Bundle;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.FrameLayout;
import android.widget.TextView;

import com.hotechie.operapet.MyApplication;
import com.hotechie.operapet.R;
import com.hotechie.util.MessageDialogFragment;

/**
 * Created by duncan on 19/10/2016.
 */

public class MyMessageDialogFragment extends MessageDialogFragment {

    public static MyMessageDialogFragment getInstance(Bundle b, MessageDialogFragmentCallback callback){
        MyMessageDialogFragment f = new MyMessageDialogFragment();
        if (b != null)
            f.setArguments(b);
        f.mCallback = callback;
        return f;
    }

    @Override
    public Dialog onCreateDialog(Bundle savedInstanceState) {
        Bundle infoBundle = new Bundle();
        if (getArguments() != null)
            infoBundle = getArguments();
        else if (savedInstanceState != null)
            infoBundle = savedInstanceState;

        String title = infoBundle.getString(KEY_TITLE, null);
        String message = infoBundle.getString(KEY_MESSAGE, null);
        String positiveBtn = infoBundle.getString(KEY_POSITIVE_BTN, null);
        String negativeBtn = infoBundle.getString(KEY_NEGATIVE_BTN, null);

        AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
        if (title != null)
            builder.setTitle(title);


        if (positiveBtn != null)
            builder.setPositiveButton(positiveBtn, new DialogInterface.OnClickListener() {
                public void onClick(DialogInterface dialog, int id) {
                    if (mCallback != null)
                        mCallback.onClickPositiveButton(MyMessageDialogFragment.this, dialog, id);
                }
            });
        if (negativeBtn != null)
            builder.setNegativeButton(negativeBtn, new DialogInterface.OnClickListener() {
                public void onClick(DialogInterface dialog, int id) {
                    if (mCallback != null)
                        mCallback.onClickNegativeButton(MyMessageDialogFragment.this, dialog, id);
                }
            });

        LayoutInflater inflater = (LayoutInflater)MyApplication.getAppContext().getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        View view = inflater.inflate(R.layout.view_dialog_message, null, false);
        builder.setView(view);

        if (message != null) {
            ((TextView)view.findViewById(R.id.txt_message)).setText(message);
//            builder.setMessage(message);
        }

        return builder.create();
    }
}
