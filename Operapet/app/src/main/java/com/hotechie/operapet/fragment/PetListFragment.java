package com.hotechie.operapet.fragment;

import android.content.DialogInterface;
import android.content.Intent;

import com.hotechie.operapet.InnerPageEditFragmentActivity;
import com.hotechie.operapet.InnerPageFragmentActivity;
import com.hotechie.operapet.MyApplication;
import com.hotechie.operapet.R;
import com.hotechie.operapet.Util;
import com.hotechie.operapet.service.ProfileService;
import com.hotechie.operapet.service.ResponseHandler;
import com.hotechie.operapet.service.ServiceManager;
import com.hotechie.operapet.service.model.Pet;
import com.hotechie.operapet.service.model.ResponseListWrapper;
import com.hotechie.operapet.ui.row_view_factory.PetRowViewFactory;
import com.hotechie.operapet.ui.row_view_factory.RowViewFactoryListAdapter;
import com.hotechie.util.MessageDialogFragment;

import java.util.ArrayList;
import java.util.List;

import retrofit2.Call;
import retrofit2.Response;

/**
 * Created by duncan on 2/10/2016.
 */

public class PetListFragment extends BaseListFragment {
    private static String TAG = "PetListFragment";

    public static PetListFragment getInstance(int memberId, boolean canCreate){
        PetListFragment frag = new PetListFragment();
        frag.mMemberId = memberId;
        frag.mCanCreate = canCreate;
        return frag;
    }

    protected PetRowViewFactory mFactory = null;
    protected PetRowViewFactory.PetRowViewFactoryCallback mFactoryCallback = null;

    protected int mMemberId = -1;
    protected boolean mCanCreate = false;
    protected List<Pet> mPets = new ArrayList<>();

    @Override
    public void onResume() {
        super.onResume();
        updateData();
    }

    @Override
    public String getTitle(){
        return MyApplication.getAppContext().getString(R.string.title_pet_list);
    }

    @Override
    public void configUI(){
        super.configUI();

        mFactoryCallback = new PetRowViewFactory.PetRowViewFactoryCallback() {
            @Override
            public void onClick(Pet pet) {
                if (mCanCreate) {
                    PetDetailFragment frag = PetDetailFragment.getInstance(pet, true);
                    MenuFragment.setNextContentFragment(frag);
                    Intent newIntent = new Intent(getActivity(), InnerPageEditFragmentActivity.class);
                    Util.moveToActivity(getActivity(), newIntent, true);
                }
                else{
                    PetDetailFragment frag = PetDetailFragment.getInstance(pet, false);
                    MenuFragment.setNextContentFragment(frag);
                    Intent newIntent = new Intent(getActivity(), InnerPageFragmentActivity.class);
                    Util.moveToActivity(getActivity(), newIntent, true);
                }
            }
        };
        mFactory = new PetRowViewFactory(new ArrayList<Pet>(), mFactoryCallback, mCanCreate);
        mAdapter = new RowViewFactoryListAdapter(mFactory);
        mListView.setAdapter(mAdapter);

    }

    @Override
    public void updateUI(){
        if (getView() != null){
            getView().post(new Runnable() {
                @Override
                public void run() {
                    mAdapter.setFactory(mFactory);
                    mAdapter.notifyDataSetChanged();
                }
            });
        }
    }

    @Override
    public void updateData(){
        ProfileService service = ServiceManager.sharedInstance().getServiceHandler().create(ProfileService.class);
        Call<ResponseListWrapper<Pet>> call = service.getAllPet(mMemberId);
        call.enqueue(new ResponseHandler<ResponseListWrapper<Pet>>() {

            @Override
            public void onResponseParsed(Call<ResponseListWrapper<Pet>> call, Response<ResponseListWrapper<Pet>> response) {
                if (response.body() != null && response.body().data != null) {
                    mPets = new ArrayList<Pet>(response.body().data);
                    mFactory = new PetRowViewFactory(mPets, mFactoryCallback, mCanCreate);
                    updateUI();
                }
            }

            @Override
            public void onFailure(Call<ResponseListWrapper<Pet>> call, Throwable t) {
                onHandleError(Util.getString(R.string.msg_error), null, null, new MessageDialogFragment.MessageDialogFragmentCallback() {

                    @Override
                    public void onClickPositiveButton(MessageDialogFragment fragment, DialogInterface dialog, int id) {
                        updateData();
                    }

                    @Override
                    public void onClickNegativeButton(MessageDialogFragment fragment, DialogInterface dialog, int id) {

                    }
                });
            }
        });
    }
}
