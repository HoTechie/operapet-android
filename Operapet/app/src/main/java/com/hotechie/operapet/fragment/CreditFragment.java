package com.hotechie.operapet.fragment;

import android.util.Log;
import android.widget.TextView;

import com.hotechie.operapet.Constant;
import com.hotechie.operapet.R;
import com.hotechie.operapet.Util;
import com.hotechie.operapet.service.model.Profile;
import com.hotechie.operapet.ui.CreditView;

/**
 * Created by duncan on 3/12/2016.
 */

public class CreditFragment extends BaseFragment {
    private static String TAG = "CreditFragment";

    public static CreditFragment getInstance(Profile profile){
        CreditFragment frag = new CreditFragment();
        frag.mProfile = profile;
        return frag;
    }

    protected Profile mProfile = null;

    protected TextView mTxtCreditCount, mTxtMemberLevel;
    protected CreditView mViewCredit;

    @Override
    public String getTitle() {
        return Util.getString(R.string.credit_title);
    }

    @Override
    public int layoutRes() {
        return R.layout.fragment_credit;
    }

    @Override
    public void configUI() {
        super.configUI();

        if (mProfile == null){
            Log.e(TAG, "profile is null");
            Util.backToActivity(getActivity(), false);
            return;
        }

        mTxtCreditCount = (TextView)getView().findViewById(R.id.txt_credit_count);
        mTxtMemberLevel = (TextView)getView().findViewById(R.id.txt_member_level);
        mViewCredit = (CreditView)getView().findViewById(R.id.view_credit);

        mTxtCreditCount.setText(String.valueOf(mProfile.credit_total));
        mTxtMemberLevel.setText(Constant.getMemberLevelStr(mProfile.member_level));

        // FIXME
        int credit = mProfile.credit_total;
        if (credit > 10000)
            credit = 10000;
        mViewCredit.setPercentage(credit / 10000f);

    }

    @Override
    protected void updateUIRunnable() {
        super.updateUIRunnable();
    }
}
