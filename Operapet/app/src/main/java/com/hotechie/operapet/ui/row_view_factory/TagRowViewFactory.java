package com.hotechie.operapet.ui.row_view_factory;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.hotechie.operapet.MyApplication;
import com.hotechie.operapet.R;
import com.hotechie.operapet.Util;
import com.hotechie.operapet.service.ServiceManager;
import com.hotechie.operapet.service.model.Gift;
import com.hotechie.operapet.service.model.Tag;
import com.squareup.picasso.Picasso;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

/**
 * Created by duncan on 13/10/2016.
 */

public class TagRowViewFactory extends RowViewFactory {

    public static interface  TagRowViewFactoryCallback{
        public void onClick(Tag tag);
    }

    protected List<Tag> mTags= new ArrayList<>();
    protected TagRowViewFactory.TagRowViewFactoryCallback mCallback = null;

    public TagRowViewFactory(List<Tag> tags, final TagRowViewFactory.TagRowViewFactoryCallback callback){
        mTags = new ArrayList<>(tags);
        mCallback = callback;
    }

    @Override
    public int getCount() {
        return mTags.size();
    }

    @Override
    public Object getItem(int index) {
        return mTags.get(index);
    }

    @Override
    public View getView(final Object object) {
        final Tag tag = (Tag)object;

        LayoutInflater inflater = (LayoutInflater) MyApplication.getAppContext().getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        View rowView = inflater.inflate(R.layout.row_tag, null, false);

        ImageView imgProfile = (ImageView)rowView.findViewById(R.id.img_profile);
        TextView txtName = (TextView)rowView.findViewById(R.id.txt_name);

        if (tag.image_id == 0) {
            imgProfile.setImageDrawable(null);
        }
        else{
            Picasso.with(MyApplication.getAppContext()).load(ServiceManager.sharedInstance().getImageUrl(ServiceManager.IMAGE_URL_PROFILE, tag.image_id)).into(imgProfile);
        }
        txtName.setText("#" + tag.description);

        if (mCallback != null){
            rowView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    mCallback.onClick(tag);
                }
            });
        }

        return rowView;
    }

    @Override
    public boolean modifyViewLayout(View contentView, Object object, int index) {
        return false;
    }
}
