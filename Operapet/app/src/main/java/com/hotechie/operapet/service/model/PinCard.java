package com.hotechie.operapet.service.model;

import org.json.JSONException;
import org.json.JSONObject;

/**
 * Created by duncan on 2/10/2016.
 */

public class PinCard extends BaseModel {
    /*
    {
        "id": 1,
        "pet_id": 1,
        "recent_pin_record_date": "2016-10-30 00:00",
        "next_pin_record_date": "2016-10-30 00:00",
        "description": "testing "
    }
     */

    public int id = 0;
    public int pet_id;
    public String recent_pin_record_date = "2016-10-30 00:00";
    public String next_pin_record_date = "2016-10-30 00:00";
    public String description = "";
    public String name = "";

    public JSONObject toJSONObject(){
        JSONObject json = new JSONObject();
        try {
            json.put("id", id);
            json.put("pet_id", pet_id);
            json.put("recent_pin_record_date", recent_pin_record_date);
            json.put("next_pin_record_date", next_pin_record_date);
            json.put("description", description);
            json.put("name", name);
        } catch (JSONException e) {
            e.printStackTrace();
        }
        return json;
    }

}
