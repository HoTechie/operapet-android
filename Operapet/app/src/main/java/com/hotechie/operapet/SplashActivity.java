package com.hotechie.operapet;

import android.Manifest;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.os.Build;
import android.os.Handler;
import android.support.v4.app.FragmentActivity;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;

public class SplashActivity extends FragmentActivity {

    protected Handler mHandler = new Handler();


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_splash);

    }

    @Override
    protected void onResume() {
        super.onResume();


        if (Build.VERSION.SDK_INT >= 23) {
            if (this.checkSelfPermission(Manifest.permission.WRITE_EXTERNAL_STORAGE) != PackageManager.PERMISSION_GRANTED
                    || this.checkSelfPermission(Manifest.permission.READ_EXTERNAL_STORAGE) != PackageManager.PERMISSION_GRANTED
                    || this.checkSelfPermission(Manifest.permission.INTERNET) != PackageManager.PERMISSION_GRANTED) {
                this.requestPermissions(new String[]{
                        Manifest.permission.WRITE_EXTERNAL_STORAGE,
                        Manifest.permission.READ_EXTERNAL_STORAGE,
                        Manifest.permission.INTERNET
                }, 99);


                return;
            }
        }

        mHandler.postDelayed(new Runnable() {
            @Override
            public void run() {
                // TODO move to login
                Intent newIntent = new Intent(SplashActivity.this, LoginActivity.class);
                newIntent.addFlags(Intent.FLAG_ACTIVITY_NO_ANIMATION);
                Util.moveToActivity(SplashActivity.this, newIntent, false);
                SplashActivity.this.finish();
            }
        }, 2000);

    }
}
