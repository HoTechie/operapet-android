package com.hotechie.operapet;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * Created by duncan on 17/8/2016.
 */
public class Constant extends Object {
    public static int POST_CAT_1 = 1;
    public static int POST_CAT_2 = 2;
    public static int POST_CAT_3 = 3;
    public static int POST_CAT_4 = 4;
    public static int POST_CAT_5 = 5;
    public static int POST_CAT_6 = 6;
    public static int POST_CAT_7 = 7;
    public static int POST_CAT_8 = 8;
    public static int POST_CAT_9 = 9;

    public static String getMemberLevelStr(int level){
        String levelStr = "";
        switch (level){
            case 1:
                levelStr = Util.getString(R.string.member_level_1);
                break;
            case 2:
                levelStr = Util.getString(R.string.member_level_2);
                break;
            case 3:
                levelStr = Util.getString(R.string.member_level_3);
                break;
        }
        return levelStr;
    }


    public static class PostCategory{
        public int id = 0;
        public int color = 0;
        public int colorDarker = 0;
        public int iconRes = 0;
        public String name = "";

        public PostCategory(int id, int color, int colorDarker, int iconRes, String name){
            this.id = id;
            this.color = color;
            this.colorDarker = colorDarker;
            this.iconRes = iconRes;
            this.name = name;
        }
    }
    protected static List<PostCategory> _postCategories = null;
    public static List<PostCategory> getPostCategories(boolean isAllIncluded) {
        if (_postCategories == null){
            List<PostCategory> categories = new ArrayList<>();
            categories.add(new PostCategory(0, 0, 0, 0, Util.getString(R.string.post_category_name_0)));
            categories.add(new PostCategory(1, R.color.theme_color, R.color.theme_color, R.drawable.icon_cat_1, Util.getString(R.string.post_category_name_1)));
            categories.add(new PostCategory(2, R.color.theme_color_2, R.color.theme_color_2_darker, R.drawable.icon_cat_2,Util.getString(R.string.post_category_name_2)));
            categories.add(new PostCategory(3, R.color.theme_color_3, R.color.theme_color_3_darker, R.drawable.icon_cat_3,Util.getString(R.string.post_category_name_3)));
            categories.add(new PostCategory(4, R.color.theme_color_4, R.color.theme_color_4_darker, R.drawable.icon_cat_4,Util.getString(R.string.post_category_name_4)));
            categories.add(new PostCategory(5, R.color.theme_color_5, R.color.theme_color_5_darker, R.drawable.icon_cat_5,Util.getString(R.string.post_category_name_5)));
            categories.add(new PostCategory(6, R.color.theme_color_6, R.color.theme_color_6_darker, R.drawable.icon_cat_6,Util.getString(R.string.post_category_name_6)));
            categories.add(new PostCategory(7, R.color.theme_color_7, R.color.theme_color_7_darker, R.drawable.icon_cat_7,Util.getString(R.string.post_category_name_7)));
            categories.add(new PostCategory(8, R.color.theme_color_8, R.color.theme_color_8_darker, R.drawable.icon_cat_8,Util.getString(R.string.post_category_name_8)));
            categories.add(new PostCategory(9, R.color.theme_color_9, R.color.theme_color_9_darker, R.drawable.icon_cat_9,Util.getString(R.string.post_category_name_9)));
            _postCategories = categories;
        }

        List<PostCategory> _categories = new ArrayList<>(_postCategories);
        if (!isAllIncluded)
            _categories.remove(0);
        return _categories;

    }

    protected static List<PostCategory> _postCategoriesForUser = null;
    public static List<PostCategory> getPostCategoriesForUser() {
        if (_postCategoriesForUser == null){
            List<PostCategory> categories = new ArrayList<>();
//            categories.add(new PostCategory(0, 0, 0, 0, Util.getString(R.string.post_category_name_0)));
            categories.add(new PostCategory(1, R.color.theme_color, R.color.theme_color, R.drawable.icon_cat_1, Util.getString(R.string.post_category_name_1)));
            categories.add(new PostCategory(7, R.color.theme_color_7, R.color.theme_color_7_darker, R.drawable.icon_cat_7,Util.getString(R.string.post_category_name_7)));
            _postCategoriesForUser = categories;
        }

        List<PostCategory> _categories = new ArrayList<>(_postCategoriesForUser);
        return _categories;

    }

    public static int GENDER_MALE = 1;
    public static int GENDER_FEMALE = 2;

    protected static Map<Integer, String> _petTypes = null;
    public static Map<Integer, String> getPetTypes(){
        if (_petTypes == null){
            _petTypes = new HashMap<Integer, String>();
            _petTypes.put(1, "家貓");
            _petTypes.put(2, "暹羅貓");
            _petTypes.put(3, "金吉拉");
            _petTypes.put(4, "緬因貓");
            _petTypes.put(5, "伯曼貓");
            _petTypes.put(6, "布偶貓");
            _petTypes.put(7, "波斯貓");
            _petTypes.put(8, "美國短毛貓");
            _petTypes.put(9, "英國短毛貓");
            _petTypes.put(10, "挪威森林貓");
            _petTypes.put(11, "異國短毛貓");
            _petTypes.put(12, "俄羅斯藍貓");
            _petTypes.put(13, "喜瑪拉雅貓");
            _petTypes.put(14, "蘇格蘭折耳貓");
            _petTypes.put(15, "土耳其安哥拉貓");
            _petTypes.put(16, "斯芬克斯貓（加拿大無毛貓）");
            _petTypes.put(17, "其他");
            _petTypes.put(18, "松鼠");
            _petTypes.put(19, "銀狐");
            _petTypes.put(20, "沙皮");
            _petTypes.put(21, "柴犬");
            _petTypes.put(22, "西施");
            _petTypes.put(23, "比熊");
            _petTypes.put(24, "八哥");
            _petTypes.put(25, "哥基");
            _petTypes.put(26, "唐狗");
            _petTypes.put(27, "鬆獅");
            _petTypes.put(28, "薩摩耶");
            _petTypes.put(29, "芝娃娃");
            _petTypes.put(30, "史納莎");
            _petTypes.put(31, "聖班納");
            _petTypes.put(32, "北京狗");
            _petTypes.put(33, "魔天使");
            _petTypes.put(34, "臘腸狗");
            _petTypes.put(35, "蝴蝶犬");
            _petTypes.put(36, "玩具貴婦");
            _petTypes.put(37, "英國曲架");
            _petTypes.put(38, "拉布拉多");
            _petTypes.put(39, "愛斯基摩");
            _petTypes.put(40, "約瑟爹利");
            _petTypes.put(41, "邊界牧羊犬");
            _petTypes.put(42, "金毛尋回犬");
            _petTypes.put(43, "古代牧羊犬");
            _petTypes.put(44, "德國牧羊犬");
            _petTypes.put(45, "小型牧羊犬");
            _petTypes.put(46, "哈士奇（雪橇）");
            _petTypes.put(47, "西高地白爹利");
            _petTypes.put(48, "其他");
        }


        return _petTypes;
    }
    public static Map<Integer, String> getPetTypesCat(){
        Map<Integer, String> typeMap = new HashMap<>();
        Map<Integer, String> types = getPetTypes();
        for (int i = 1 ; i <= 17 ; i ++){
            typeMap.put(i, types.get(i));
        }

        return typeMap;
    }
    public static Map<Integer, String> getPetTypesDog(){
        Map<Integer, String> typeMap = new HashMap<>();
        Map<Integer, String> types = getPetTypes();
        for (int i = 18 ; i <= 48 ; i ++){
            typeMap.put(i, types.get(i));
        }

        return typeMap;
    }
    public static Map<Integer, String> getPetTypes(int id){
        return id <= 17 ? getPetTypesCat() : getPetTypesDog();
    }
    public static int getDefaultCat(){
        return 1;
    }
    public static int getDefaultDog(){
        return 18;
    }
    public static List<String> getPetTypeCategory(){
        List<String> categories = new ArrayList<>();
        categories.add("貓");
        categories.add("狗");
        return categories;
    }
    public static String getPetTypeCategory(int id){
        return id <= 17 ? "貓" : "狗";
    }
    public static int getPetTypeDefaultId(int id){
        return id <= 17 ? 1 : 18;
    }


    public static int FRIENDSHIP_STATUS_NO_FRIEND = 0;
    public static int FRIENDSHIP_STATUS_PENDING = 1;
    public static int FRIENDSHIP_STATUS_CONFIRMED = 2;


    public static int GIFT_STATUS_CANCELED = -1;
    public static int GIFT_STATUS_REDEEMED = 1;
    public static int GIFT_STATUS_COLLECTED = 2;


    public static int ADV_TYPE_POP_UP = 1;
    public static int ADV_TYPE_POST_LIST = 2;
    public static int ADV_TYPE_GIFT = 3;

    public static String getBmiStatusStr(float bmi){
        if (bmi <= 18.0f){
            return "體重過輕";
        }
        else if (bmi <= 24.0f){
            return "正常範圍";
        }
        else if (bmi <= 27.0f){
            return "輕度肥胖";
        }
        else if (bmi <= 33.0f){
            return "中度肥胖";
        }
        else {
            return "重度肥胖";
        }
    }

    public static int getBmiStatusImgRes(float bmi){
        if (bmi <= 18.0f){
            return R.drawable.overweight;
        }
        else if (bmi <= 24.0f){
            return R.drawable.normal;
        }
        else {
            return R.drawable.overweight;
        }
    }
}
