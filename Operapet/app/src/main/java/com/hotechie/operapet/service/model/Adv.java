package com.hotechie.operapet.service.model;

/**
 * Created by duncan on 4/11/2016.
 */

public class Adv extends BaseModel {
    /*
    {
            "image_id": 1,
            "type": 0,
            "link": "http://www.operapet.com",
            "order": 0
        }
     */

    public int image_id;
    public int type;
    public String link;
    public int order;
}
