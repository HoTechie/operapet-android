package com.hotechie.operapet.fragment;

import com.hotechie.operapet.R;
import com.hotechie.operapet.Util;
import com.hotechie.operapet.service.MemberService;
import com.hotechie.operapet.service.ResponseHandler;
import com.hotechie.operapet.service.ServiceManager;
import com.hotechie.operapet.service.model.ActionHistory;
import com.hotechie.operapet.service.model.ResponseListWrapper;
import com.hotechie.operapet.ui.row_view_factory.ActionHistoryRowViewFactory;
import com.hotechie.operapet.ui.row_view_factory.RowViewFactoryListAdapter;

import java.util.ArrayList;
import java.util.List;

import retrofit2.Call;
import retrofit2.Response;

/**
 * Created by duncan on 21/10/2016.
 */

public class ActionHistoryListFragment extends BaseListFragment {
    private static String TAG = "ActionHistoryListFrag";

    public static ActionHistoryListFragment getInstance(){
        ActionHistoryListFragment frag = new ActionHistoryListFragment();
        return frag;
    }

    protected List<ActionHistory> mActionHistoryList = new ArrayList<>();
    protected ActionHistoryRowViewFactory mFactory = null;
    protected ActionHistoryRowViewFactory.ActionHistoryRowViewFactoryCallback mFactoryCallback = null;

    @Override
    public void onResume() {
        super.onResume();

        updateData();
    }

    @Override
    public String getTitle() {
        return Util.getString(R.string.title_action_history);
    }

    @Override
    public void configUI() {
        super.configUI();

        mFactoryCallback = new ActionHistoryRowViewFactory.ActionHistoryRowViewFactoryCallback() {
            @Override
            public void onClick(ActionHistory actionHistory) {

            }
        };

        mFactory = new ActionHistoryRowViewFactory(mActionHistoryList, mFactoryCallback);
        mAdapter = new RowViewFactoryListAdapter(mFactory);
        mListView.setAdapter(mAdapter);

    }

    @Override
    protected void updateUIRunnable() {
        super.updateUIRunnable();

        mAdapter.setFactory(mFactory);
        mAdapter.notifyDataSetChanged();
    }

    @Override
    public void updateData() {
        super.updateData();

        showLoadingScreen(true, null, Util.getString(R.string.msg_loading));

        MemberService service = ServiceManager.sharedInstance().getServiceHandler().create(MemberService.class);
        Call<ResponseListWrapper<ActionHistory>> call = service.getActionHistory();
        call.enqueue(new ResponseHandler<ResponseListWrapper<ActionHistory>>() {
            @Override
            public void onResponseParsed(Call<ResponseListWrapper<ActionHistory>> call, Response<ResponseListWrapper<ActionHistory>> response) {
                showLoadingScreen(false, null, null);
                if (response.body() != null && response.body().data != null){
                    mActionHistoryList = new ArrayList<ActionHistory>(response.body().data);
                    mFactory = new ActionHistoryRowViewFactory(mActionHistoryList, mFactoryCallback);
                    updateUI();
                }
                else{
                    onShowMessage(Util.getString(R.string.msg_error), Util.getString(R.string.msg_ok), null);
                }
            }

            @Override
            public void onFailure(Call<ResponseListWrapper<ActionHistory>> call, Throwable t) {
                showLoadingScreen(false, null, null);
                onShowMessage(Util.getString(R.string.msg_error), Util.getString(R.string.msg_ok), null);
            }
        });
    }


}
