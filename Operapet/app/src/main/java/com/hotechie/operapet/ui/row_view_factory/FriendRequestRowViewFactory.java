package com.hotechie.operapet.ui.row_view_factory;

import android.view.View;

import com.hotechie.operapet.R;
import com.hotechie.operapet.service.model.Profile;

import java.util.List;

/**
 * Created by duncan on 17/10/2016.
 */

public class FriendRequestRowViewFactory extends UserRowViewFactory {

    public static interface FriendRequestRowViewFactoryCallback{
        public void onClick(Profile profile);
        public void onClickResponse(Profile profile, boolean isAccept);
    }

    protected FriendRequestRowViewFactoryCallback mCallback = null;

    public FriendRequestRowViewFactory(List<Profile> users, FriendRequestRowViewFactoryCallback callback) {
        super(users, false);

        mRowViewRes = R.layout.row_friend_request;

        mCallback = callback;
    }

    @Override
    public View getView(Object object) {
        View rowView = super.getView(object);

        final Profile profile = (Profile)object;


        if (mCallback != null) {
            rowView.findViewById(R.id.img_accept).setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    mCallback.onClickResponse(profile, true);
                }
            });
            rowView.findViewById(R.id.img_reject).setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    mCallback.onClickResponse(profile, false);
                }
            });
            rowView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    mCallback.onClick(profile);
                }
            });
        }

        return rowView;
    }
}
